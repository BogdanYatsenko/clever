<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*landing*/
Route::get('/','CustomController@HomePage')->name('home');

Route::get('/home', 'CustomController@HomePage')->name('home');

Route::post('/useridentify', [
    'as' => 'user.identify',
    'uses' => 'CustomController@identify'
]);
/* end landing */

/* contacts */
Route::get('/home/contacts', 'CustomController@ContactPage')->name('contacts');
/*end contacts */

// docs
Route::get('/home/offer', 'CustomController@OfferPage')->name('offer');

// chat
Route::get('/chat', [
    'as'=>'chat',
    'uses' => 'ChatController@index',
    'middleware' => 'auth'
]);
Route::get('autocomplete', [
    'as'=>'autocomplete',
    'uses'=>'ChatController@autocomplete'
]);

Route::group(['prefix' => 'ws'], function() {
    Route::get('check-auth', function() {
        return response()->json([
            'auth' => \Auth::check(),
            'id' => \Auth::id(),
            'first' => \Auth::user()->first,
            'second' => \Auth::user()->second
        ]);
    });
});

Route::group(['prefix' => 'messages'], function () {
    // Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    // Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    // Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);

    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    // Route::post('/', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::post('/update', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
    Route::post('/check', ['as' => 'messages.check', 'uses' => 'MessagesController@check']);
    Route::post('/drawthread', ['as' => 'thread.draw', 'uses' => 'MessagesController@drawthread']);
    Route::post('/unread', ['as' => 'thread.unread', 'uses' => 'MessagesController@unread']);
    Route::post('/remove', ['as' => 'thread.remove', 'uses' => 'MessagesController@remove']);

    //Route::get('/test', ['as' => 'test_rtc', 'uses' => 'MessagesController@test_rtc']);

});

// people search
Route::get('/people','PeopleController@index')->name('people')->middleware('auth');
Route::get('/people_{query}', [
    'as'=>'search',
    'uses' => 'PeopleController@search',
    'middleware' => 'auth'
]);
// Route::post('/livesearch' , [
//     'as' => 'people.search',
//     'uses' => 'PeopleController@livesearch'
// ]);

Route::get('/faq/page_id_{page_id}', 'FaqController@index');
Route::get('/support/send_question', 'SupportController@send_question');

Route::group(array('prefix' => 'catalog'), function() {
    Route::get('/', 'CourseCatalog@index')->name('catalog');
    Route::match(['get', 'post'], '/object_type_{object_type}/source_{source}/{searchword}/from_{from}/to_{to}/order_{order}/page_{page}', 'CourseCatalog@search');

});

/* login-register*/
Route::get('/login','CustomController@LoginPage')->name('login');
Route::get('/redirect/{service}', 'Auth\SocialNetAuthController@redirect');
Route::get('/callback/{service}', 'Auth\SocialNetAuthController@callback');

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'RegistrationController@confirm'
]);
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');

Auth::routes();
/* Auth::route description
    // Authentication Routes...
    Route::get('login', [
        'as' => 'login',
        'uses' => 'Auth\LoginController@showLoginForm'
    ]);
    Route::post('login', [
        'as' => '',
        'uses' => 'Auth\LoginController@login'
    ]);
    Route::post('logout', [
        'as' => 'logout',
        'uses' => 'Auth\LoginController@logout'
    ]);

    // Password Reset Routes...
    Route::post('password/email', [
        'as' => 'password.email',
        'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
    ]);
    Route::get('password/reset', [
        'as' => 'password.request',
        'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
    ]);
    Route::post('password/reset', [
        'as' => '',
        'uses' => 'Auth\ResetPasswordController@reset'
    ]);
    Route::get('password/reset/{token}', [
        'as' => 'password.reset',
        'uses' => 'Auth\ResetPasswordController@showResetForm'
    ]);

    // Registration Routes...
    Route::get('register', [
        'as' => 'register',
        'uses' => 'Auth\RegisterController@showRegistrationForm'
    ]);
    Route::post('register', [
        'as' => '',
        'uses' => 'Auth\RegisterController@register'
    ]);
*/

/* end login-register*/



/* user profile*/
Route::group(array('prefix' => '/'), function() {
    Route::get('/id{user_id}', 'UserController@profile');
    Route::get('/id{user_id}/config', 'UserController@config');
   // Route::get('/{user_name}', 'UserController@profile_name');

    Route::get('/id{user_id}/setting','UserController@setting')->name('user_setting')->middleware('auth');
    Route::post('/id{user_id}/setting','UserController@save_setting')->middleware('auth');
    Route::post('/id{user_id}/setting/region','UserController@get_region')->middleware('auth');
    Route::post('/id{user_id}/setting/city','UserController@get_city')->middleware('auth');

    Route::post('/id{user_id}', 'UserController@update_avatar')->middleware('auth');
    Route::post('/id{user_id}/follow', 'UserController@follow')->middleware('auth');
    Route::post('/id{user_id}/unfollow', 'UserController@unfollow')->middleware('auth');
    Route::post('/id{user_id}/getconfirm', 'UserController@getconfirm')->middleware('auth');
    Route::post('/id{user_id}/applyconfirm', 'UserController@applyconfirm')->middleware('auth');
});

Route::get('/smssender_{key}',  'SmsSenderController@index')->name('smscenter');
/* end user profile*/

Route::group(array('prefix' => 'tickets'), function() {
    Route::get('/moderation/id_{ticket_id}', 'EventsController@getModeratedTicket')->middleware('auth');
 //   Route::get('/all', 'EventsController@index')->middleware('auth');
 //   Route::get('/admin', 'EventsController@admin')->middleware('auth');
});

/* create book */
Route::group(array('prefix' => 'book', 'middleware' => 'auth'), function() {
    Route::get('/preview/id{book_id}', 'BookController@preview_book')->name('book_preview');
    Route::get('/preview/cid_{course_id}/id{book_id}', 'BookController@preview_cid_book')->name('book_cid_preview');
    Route::post('/preview/id{book_id}/getlesson', 'BookController@get_lesson');

    Route::get('/create/cid_{course_id}', 'BookController@create_book')->name('create_book');
    Route::get('/create', 'BookController@add_book')->name('add_book');

    Route::get('/edit/cid_{course_id}/id{book_id}', 'BookController@edit_book')->name('edit_book');
    Route::post('/edit/id{book_id}/addlesson', 'BookController@add_lesson');
    Route::post('/edit/id{book_id}/addsublesson', 'BookController@add_sublesson');
    Route::post('/edit/id{book_id}/dellesson', 'BookController@del_lesson');
    Route::post('/edit/id{book_id}/delsublesson', 'BookController@del_sublesson');
    Route::post('/edit/id{book_id}/getlesson', 'BookController@get_lesson');
    Route::post('/edit/id{book_id}/getalllesson', 'BookController@get_all_lesson');
    Route::post('/edit/id{book_id}/setlesson', 'BookController@set_lesson');
    Route::get('/edit/cid_{course_id}/id{book_id}/delbook', 'BookController@del_book');
    Route::post('/edit/id{book_id}/rename', 'BookController@rename_book');

});
/* end create book */

/* create course  */
Route::group(['prefix' => 'constructor', 'middleware' => 'auth'], function(){
    $c = 'ConstructorController@';

    Route::get('/id{course_id}', $c.'index')->name('constructor');

    Route::get('/config/id{course_id}', $c.'config_course')->name('config_course');
    Route::post('/config/id{course_id}', $c.'apply_config_course');
    Route::post('/config/id{course_id}/vector', $c.'get_vector');
    Route::post('/create/vector', $c.'get_vector');
    Route::post('/save_priority', $c.'save_priority');

    Route::get('/create_course', $c.'create_course');
    Route::get('/delete_course/id{course_id}', $c.'delete_course');

    Route::get('/add_material', $c.'add_material');
    Route::get('/get_material/id{material_id}', $c.'get_material');
    Route::get('/rename_material', $c.'rename_material');
    Route::get('/remove_material/cid_{course_id}/id{material_id}', $c.'delete_material');

    Route::get('/id{course_id}/add_text', $c.'add_text');
    Route::get('/id{course_id}/add_video', $c.'add_video');
    Route::get('/id{course_id}/add_lesson_id{lesson_id}', $c.'add_lesson');
    Route::get('/id{course_id}/add_quiz_id{quiz_id}', $c.'add_quiz');
    Route::get('/id{course_id}/add_task_id{task_id}', $c.'add_task');

    Route::get('/get_exist_books', $c.'get_exist_books');
    Route::get('/get_exist_lessons', $c.'get_exist_lessons');
    Route::get('/get_exist_video', $c.'get_exist_video');
    Route::get('/get_exists_files', $c.'get_exists_files');
    Route::get('/get_exist_pptx', $c.'get_exists_pptx');
});

Route::group(['prefix' => 'quiz', 'middleware' => 'auth'], function(){
    $c = 'QuizController@';

    Route::get('/create/cid_{course_id}', $c.'create')->name('new_quiz');
    Route::get('/edit/cid_{course_id}/id{quiz_id}', $c.'edit');
    Route::get('/save_user_answer/quiz_id{quiz_id}/question_id{question_id}/variant_id{variant_id}', $c.'save_user_answer');

    Route::get('/my_quiz_points/cid_id{course_id}/id{material_id}', $c.'my_quiz_points');
    Route::post('/edit/cid_{course_id}/id{quiz_id}/save', $c.'save');

    Route::post('/refresh', $c.'refresh_quiz');

});

Route::group(['prefix' => 'task', 'middleware' => 'auth'], function(){
    $c = 'TaskController@';

    Route::get('/create/cid_{course_id}', $c.'create')->name('new_task');
    Route::get('/edit/cid_{course_id}/id{task_id}', $c.'edit');

    Route::post('/edit/cid_{course_id}/id{task_id}/save', $c.'save');
});

Route::group(array('prefix' => 'course'), function() {
    Route::get('/card/id{course_id}', 'CourseController@index')->name('course_card');

    Route::get('/id{course_id}/materials', 'CourseController@material_reader')->name('course_materials')->middleware('auth');
    Route::get('/id{course_id}/my_results', 'CourseController@my_results')->name('course_materials')->middleware('auth');
    Route::get('/id{course_id}/my_students', 'CourseController@my_students')->name('course_materials')->middleware('auth');
    Route::get('/id{course_id}/delete', 'CourseController@delete_course_from_userpage')->name('course_materials')->middleware('auth');

    Route::post('id{course_id}/get_student_results', 'CourseController@get_student_results')->middleware('auth');
    Route::post('/set_mark_to_student', 'CourseController@set_mark_to_student')->middleware('auth');



    Route::get('/buy/id{course_id}', 'CourseController@buy_course')->name('buy_course');

    Route::get('/edit/id{course_id}/moderate', 'CourseController@send_to_moderate')->middleware('auth');
    Route::get('/edit_offline/id{course_id}/moderate', 'CourseController@send_to_moderate_offline')->middleware('auth');

    Route::get('/create', 'CourseController@create_course')->name('create_course')->middleware('auth');
    Route::post('/create', 'CourseController@add_course')->middleware('auth');

    Route::post('/edit/id{course_id}', 'CourseController@upload_avatar')->middleware('auth');
    Route::post('/edit/id{course_id}/delcourse', 'CourseController@del_course')->middleware('auth');

    Route::get('/id{course_id}/quiz{quiz_id}/start', 'CourseController@start')->middleware('auth');
    Route::get('/id{course_id}/quiz{quiz_id}/result', 'CourseController@result')->middleware('auth');
    Route::get('/id{course_id}/quiz{quiz_id}/uid{user_id}', 'CourseController@result_user')->middleware('auth');
    Route::post('/id{course_id}/quiz{quiz_id}/result', 'CourseController@save_result')->middleware('auth');

    Route::get('/id{course_id}/quiz{quiz_id}/getresult', 'CourseController@get_result')->middleware('auth');

    Route::post('/id{course_id}/task{task_id}/complete', 'CourseController@complete_task')->middleware('auth');

    //Route::get('/config/id{course_id}', 'CourseController@config_course')->name('config_course')->middleware('auth');
    //Route::post('/config/id{course_id}', 'CourseController@apply_config_course')->middleware('auth');

});


Route::group(array('prefix' => 'other_course'), function() {
    Route::get('/card/id{other_course_id}', 'CourseController@index_other')->name('other_course_card');
});
/*Route::get('other_course/card/id{other_course_id}', 'CourseController@index_other')->name('other_course_card');*/
/* end create course  */

/* create shop */
Route::group(array('prefix' => 'shop'), function() {
    Route::get('/manager/', 'ShopController@manage')->name('shop_manage')->middleware('auth');
    Route::post('/sendxhr/', 'ShopController@send_xhr')->name('send_xhr')->middleware('auth');
    Route::get('/sell/id{course_id}', 'ShopController@sell')->middleware('auth');
    Route::post('/sell/id{course_id}', 'ShopController@sell_course')->middleware('auth');
});
/* end create shop */

/* upload file */
Route::group(array('prefix' => 'files'), function() {
    Route::post('/upload', 'UploadController@file_upload')->middleware('auth');
    Route::post('/getall', 'UploadController@file_get_all')->middleware('auth');
});
/* end cupload file */


/*statistics*/
Route::group(['middleware' => ['auth', 'admin']], function(){
    Route::get('/stat','StatController@index');
});


Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function() {
    Route::get('/', 'AdminController@index');

    Route::get('/accept/id{event_id}', 'CourseController@accept_moderation');
    Route::get('/reject/id{event_id}', 'CourseController@reject_moderation');

    Route::get('/accept_teacher/id{teacher_id}', 'TeacherController@accept_moderation');
    Route::get('/reject_teacher/id{teacher_id}', 'TeacherController@reject_moderation');

    Route::get('/accept_company_admin/id{company_admin_id}', 'CompanyAdminController@accept_moderation');
    Route::get('/reject_company_admin/id{company_admin_id}', 'CompanyAdminController@reject_moderation');

    Route::get('/accept_offline/id{offline_id}', 'CourseController@accept_moderation_offline');
    Route::get('/reject_offline/id{offline_id}', 'CourseController@reject_moderation_offline');

    Route::get('/accept_xhr/id{xhr_id}_sid{student_id}_cid{course_id}', 'AdminController@accept_xhr');
    Route::get('/reject_xhr/id{xhr_id}_sid{student_id}_cid{course_id}', 'AdminController@reject_xhr');

    Route::get('/accept_meeting/id{meeting_id}', 'MeetingController@accept_moderation');
    Route::get('/reject_meeting/id{meeting_id}', 'MeetingController@reject_moderation');

    Route::get('/support/answer', 'SupportController@send_answer');
});



/* graph search section */

Route::group(['prefix' => 'search', 'middleware' => ['auth', 'admin']], function() {
    Route::get('/se_{id}/page_{page}', 'GraphSearchController@index');
});

/* end graph search section */

/* teachers section */
Route::group(['prefix' => 'teacher', 'middleware' => ['auth']], function (){
    Route::get('/became_{id}', 'TeacherController@becameteacher');
    Route::post('/became', 'TeacherController@createteacher');
    Route::get('/id_{id}', 'TeacherController@profile');

    Route::get('/online_teacher_accept_{id_course}', 'TeacherController@online_accept');
    Route::get('/online_teacher_reject_{id_course}', 'TeacherController@online_reject');

    Route::get('/offline_teacher_accept_{id_course}', 'TeacherController@offline_accept');
    Route::get('/offline_teacher_reject_{id_course}', 'TeacherController@offline_reject');

    Route::get('/meeting_teacher_accept_{id_meeting}', 'TeacherController@meeting_accept');
    Route::get('/meeting_teacher_reject_{id_meeting}', 'TeacherController@meeting_reject');

});

/* end teachers section*/


/* meeting section*/

Route::group(['prefix' => 'meeting'], function() {
    Route::get('/create', 'MeetingController@create');
    Route::post('/create', 'MeetingController@add_meeting');
    Route::get('/edit/id{meeting_id}/moderate', 'MeetingController@send_to_moderation');
    Route::get('/card/id{meeting_id}', 'MeetingController@index');
    Route::get('/edit{meeting_id}', 'MeetingController@edit_meeting');
    Route::post('/update/id{meeting_id}', 'MeetingController@update_meeting');
    Route::get('/edit{meeting_id}/delete_teacher{teacher_id}', 'MeetingController@delete_teacher');
    Route::get('/delete_meeting/id{id_meeting}', 'MeetingController@delete_meeting');
    Route::get('/buy/id{id_meeting}', 'MeetingController@buy_meeting');
    Route::get('/id{id_meeting}/delete', 'MeetingController@delete_from_userpage');
});

/* end meeting section*/

/* company admin section*/
Route::group(['prefix' => 'company_admin', 'middleware' => ['auth']], function() {
    Route::get('/became{user_id}', 'CompanyAdminController@became');
    Route::post('/became', 'CompanyAdminController@add_company_admin');
    Route::post('/create_teacher', 'CompanyAdminController@add_teacher');
});
/* end company admin section*/


/* webinars section*/

Route::group(['prefix' => 'webinar', 'middleware' => ['auth']], function() {
    Route::get('/{room}/viewer', 'MeetingController@go_to_webinar_room');
});
/*end webinars section*/


Route::group(['prefix' => 'offline_course', 'middleware' => 'auth'], function () {
    $c = 'CourseController@';
    Route::get('/id{meeting_id}', $c . 'offline_index');
    Route::post('/update/id{meeting_id}', $c . 'offline_course_update')->middleware('auth');
    Route::post('/edit/id{meeting_id}', $c . 'upload_avatar')->middleware('auth');
    Route::get('/card/id{meeting_id}', $c . 'offline_card_index');
    Route::get('/delete_course/id{meeting_id}', $c . 'delete_offline_course');
    Route::get('/card/rating/id{meeting_id}_{rating}', $c.'update_rating');
    Route::get('/buy/id{course_id}', $c.'buy_offline_course');
    Route::get('/id{meeting_id}/delete', $c.'delete_offline_from_userpage');
});

Route::get('test_webinar', 'WebinarController@index');