var token = $("input[name='_token']").val();

$(function() {

    // view messages
    $(document).on('click', '.conversation-wrap .conversation', function(e) {
        e.preventDefault();

        $('div.conversation-wrap div.active:first').removeClass('active');
        $(this).addClass('active');

        if ($(this).attr('multy-thread') == 'true') {
            alert(JSON.stringify($(this).data('contacts')));
            $('.right-topmenu span').empty().append(
                '<a class="title">'+
                $(this).children('.msg-body').children('h5').text()+'</a>'
            );
        } else {
            $('.right-topmenu span').empty().append(
                '<a class="title" href="/id'+$(this).data('contacts')+'">'+
                $(this).children('.msg-body').children('h5').text()+'</a>'
            );
        }

        queryShow($(this).data('thread_id'));
    });

    // send messages
    $(document).on('click', '.send-btn .btn', function() {
        // var thread_id = $('.conversation-wrap .active').data('thread_id');
        var thread_id = $(this).parents('.send-wrap').data('thread_id');
        var message = $('.send-wrap .input-box div').html();
        var id = $('.send-wrap').data('part_id');

        $("#input-div").empty().focus();
        $.ajax({
            method: 'POST',
            url: '/messages/update',
            data: {
                _token: token,
                participant_id: id,
                thread_id: thread_id,
                message: message
            }
        }).done(function(data) {
            if (thread_id == 0)
                $('.send-wrap').data('thread_id', data['thread_id']);

            socket.emit('message', {
                thread_id: $('.send-wrap').data('thread_id'),
                message: data['message']
            });
        });
    });

    $(document).bind('keypress', 'div.input-box div', function(event) {

        if ((event.keyCode == 13) && event.shiftKey) {

            event.preventDefault();
            $('#input-div').append('<div><br></div>');
            placeCaretAtEnd( document.getElementById("input-div") );

        } else if (event.which === 13)  {

            event.stopPropagation();
            event.preventDefault();

            var selection = window.getSelection(),
                range = selection.getRangeAt(0),
                newline = document.createTextNode('\n');

                range.deleteContents();
                range.insertNode(newline);
                range.setStartAfter(newline);
                range.setEndAfter(newline);
                range.collapse(false);
                selection.removeAllRanges();
                selection.addRange(range);

            if ($('#input-div').text().trim().length == 0)
                $('#input-div').empty();
            else
                $('.send-btn .btn').click();

        }
    });

    $(document).on('click', '.input-box', function() {
        $('#input-div').focus();
    });

    $(document).on('paste', 'div[contenteditable="true"]', function(e) {
        e.preventDefault();
        var paste = (e.originalEvent || e).clipboardData.getData('text/plain');
        window.document.execCommand('insertText', false, paste);
    });

    $(document).mousemove('#page-inside-wrap', function(event) {

        var thread =  $('.conversation-wrap .active');
        var marker = $('.conversation-wrap .active .number');

        if (marker.length) {

            console.log('Marker removing...');
            marker.parent().remove();

            // conversation decrement
            var number = parseInt($('#messages span.number').text());
            if (number > 1) {
                number--;
                $('#messages span.number').text(number);
            } else $('#messages span.tcount').remove();

            socket.emit('viewed', {
                thread_id: thread.data('thread_id'),
                user_id: thread.data('contacts')
            });
        }
    });

    $(document).on('click', 'span.thread-remove', function(e) {
        e.stopPropagation();
        e.preventDefault();

        var thread = $(this).parents('.conversation');
        var thread_id = thread.data('thread_id');

        $.post('/messages/remove', {
            _token: token,
            thread_id: thread_id
        }).done(function() {
            if (thread.hasClass('active')) {
                $('div.right-topmenu span').empty();
                $('div.message-wrap .msg-wrap').empty();
                $('div.send-wrap').remove();
            }
            thread.remove();
        });
    });

    $(document).on('click', 'div.verbal', function () {
        $('div.message-wrap').append('<div class="communication-form"><div class="stream"></div></div>')
        $('div.msg-wrap').hide();
        $('div.send-wrap').hide();

    });
});

// counting theads with unread messages
$.post('/messages/unread', {
    _token: token
}).done(function(tcount) {

    if ( tcount > 0 ) {
        $('#messages span.icon').append('<span class="tcount">'+ tcount +'<span>');
    }

});

function queryShow(thread_id) {
    $.get('/messages/'+thread_id, function(data) {
        $('div.message-wrap .msg-wrap').empty().html(data.messages);
        // Подумать над отрисовкой форм
        $('div.send-wrap').remove();

        $('div.message-wrap .msg-wrap')
        .append('<div class="user-behavior"></div>');

        $('div.message-wrap').append(data.send_form);

        $('.msg-wrap').scrollTop($('.msg-wrap')[0].scrollHeight);
        $('ul.pagination').hide();
        $("#input-div").empty().focus();
    });
}

function placeCaretAtEnd(element) {
    element.focus();
    if ("undefined" !== typeof window.getSelection() && typeof document.createRange() !== "undefined") {
        var range = document.createRange();
            range.selectNodeContents(element);
            range.collapse(false);

            var selection = window.getSelection();
                selection.removeAllRanges();
                selection.addRange(range);
    } else if ("undefined" !== typeof document.body.createTextRange) {
        var textRange = document.body.createTextRange();
            textRange.moveToElementText(element);
            textRange.collapse(false);
            textRange.select();
    }
    $('#input-div').scrollTop($('#input-div')[0].scrollHeight);
}

// function placeCaretAtEnd(el) {
//     el.focus();
//     if (typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {
//         var range = document.createRange();
//             range.selectNodeContents(el);
//             range.collapse(false);
//             var sel = window.getSelection();
//             sel.removeAllRanges();
//             sel.addRange(range);
//         } else if (typeof document.body.createTextRange != "undefined") {
//             var textRange = document.body.createTextRange();
//                 textRange.moveToElementText(el);
//                 textRange.collapse(false);
//                 textRange.select();
//         }
//
//         $('#input-div').scrollTop($('#input-div')[0].scrollHeight);
//     }
