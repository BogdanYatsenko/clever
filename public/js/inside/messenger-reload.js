$.typeahead({
    input: '.js-typeahead-user_v1',
    minLength: 1,
    order: "asc",
    dynamic: true,
    delay: 500,
    backdrop: {
        "background-color": "#fff"
    },
    template: function(query, item) {
        var color = "#777";
        if (item.status === "owner") {
            color = "#ff1493";
        }
        var avatar = item.type != undefined ?
                    '/storage/avatars/' + item.id + '.' +item.type :
                    '/img/no_avatar.png';

        return '<span class="row">' +
                '<span class="avatar">' +
                '<img class="user-logo" src="'+avatar+'">' +
                '</span>' +
                '<span class="username"> ' + item.first + ' ' + item.second + '</span>' +
                '<span class="id"></span>' +
                 '</span>'

    },
    emptyTemplate: "Ничего не найдено для {{query}}",
    source: {
        user: {
            display: ['first', 'second'],
            ajax: function(query) {
                return {
                    type: "GET",
                    url: "autocomplete",
                    data: {
                        q: "{{query}}"
                    },
                    callback: {
                        done: function(data) {
                            return data;
                        }
                    }
                }
            }
        }
    },
    callback: {
        onClick: function(node, a, item, event) {

            // $('.right-topmenu span').empty().append(
            //     '<a class="title" href="/id' + item.id + '">' + item.first + ' ' + item.second + '</a>'
            // );

            $.post('/messages/check', {
                _token: token,
                user_id: item.id
            }).done(function(res) {
                /*
                . response: 0 if not exist dialog between users
                . if response not zero, then make get query
                . where res is id of thread
                */
                // --------temp----------------------------------
                if (res['thread_id'] >= 0 )
                    $('.right-topmenu span').empty().append(
                        '<a class="title" href="/id' + item.id + '">' + item.first + ' ' + item.second + '</a>'
                    );
                // ----------------------------------------------
                if (res['thread_id'] == 0) {
                    $('div.infinite-scroll').empty();
                    $('div.send-wrap').remove();
                    $('.message-wrap').append(
                        '<div class="send-wrap" data-part_id="' + item.id + '" data-thread_id="0">' +
                        '<div class="general-form">' +
                        '<div class="input-box col-md-10">' +
                        '<div id="input-div" contenteditable="true" placeholder="Написать собщение..."></div></div>' +
                        '<div class="send-btn col-md-2">' +
                        '<button type="button" class="btn btn-default">Отправить</button>' +
                        '</div></div></div>'
                    );
                } else if (res['thread_id'] > 0) {
                    $('.conversation-wrap .active').removeClass('active');
                    $('div.conversation[data-thread_id="' + res['thread_id'] + '"]').remove();
                    $('.conversation-wrap').prepend(res['thread_form']);
                    $('div.conversation:first').addClass('active');
                    queryShow(res['thread_id']);
                } else alert('Такой вид общения ещё не готов!')
                $('p.text-tips').remove();
            });
        },
        onSendRequest: function(node, query) {
            console.log('request is sent')
        },
        onReceiveRequest: function(node, query) {
            console.log('request is received')
        }
    },
    debug: true
});



$('.infinite-scroll').scroll(function() {
    if ($(this).scrollTop() == 0) {
        var self = $(this);
        var link = $('div.msg-wrap .pagination li.active + li a').attr('href');
        if (link != undefined) {
            $('div.msg-wrap .pagination:last').remove();
            $.get(link, function(res) {
                before_h = self[0].scrollHeight;
                $('.infinite-scroll').prepend(res);
                after_h = self[0].scrollHeight;
                self.scrollTop(after_h-before_h);
            });
        }
    }
});

$('.conversation-wrap').scroll(function() {

    if ($(this).scrollTop() == $(this)[0].scrollHeight - $(this).height()) {
        link = $('div.conversation-wrap .pagination li.active + li a').attr('href');

        if (link != undefined) {

            $('div.conversation-wrap .pagination:last').remove();

            $.get(link, function(res) {
                $('div.conversation-wrap').append(res);
            });

        }
    }
});
