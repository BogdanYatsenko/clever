/* state of category and vector select */

$('#select_cat').on('change', function (e) {
    var vektor_block_id = '#vektor-list-'+$(this).find("option:selected").data('id');
    cat_id = $(this).find("option:selected").data('id');
    vektor_id = 0;
    $('.vectors-list').hide();
    $(vektor_block_id).show();
    if ($('#search_button').length != 0)
        $('#search_button').click();

});

$('.select_vektor').on('change', function (e) {
    vektor_id = $(this).find("option:selected").data('id');
    if ($('#search_button').length != 0)
    $('#search_button').click();
});


/* end state of category and vector select */

