CKEDITOR.plugins.add('slide', {
    init : function(editor) {

        editor.addCommand( 'slide', {

            exec: function( editor ) {
                var text = CKEDITOR.instances.mytextarea.getData();
                var target = "#slide";
                var slideCount = 0;
                var pos = -1;
                while ((pos = text.indexOf(target, pos + 1)) != -1) {
                    slideCount++;
                }
                slideCount = slideCount+1;
                editor.insertHtml( '<p>#slide<a id="'+ slideCount+'" name="'+slideCount+'"></a></p>' );
            }
        });

        editor.ui.addButton('Slide', {
            label : 'Создать Слайд',
            command : 'slide',
            icon : this.path + 'icons/add_slide.png',
            toolbar: 'others,0'
        });

        CKEDITOR.dialog.add('slide', this.path + 'dialogs/slide.js');
    }
});