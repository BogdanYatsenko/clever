CKEDITOR.plugins.add('courselink', {
    init : function(editor) {

        editor.addCommand( 'courselink',  new CKEDITOR.dialogCommand( 'courselink',
            {  } ) );

        editor.ui.addButton('courselink', {
            label : 'Внутренняя ссылка',
            command : 'courselink',
            icon : this.path + 'icons/courselink.png',
            toolbar: 'others,101'
        });

        CKEDITOR.dialog.add('courselink', this.path + 'dialogs/courselink.js');
    }
});