
var model = require('../messenger/DB.js');

exports.getUserThreadAll = function(thread_id, callback) {
    var sql = 'SELECT user_id as id FROM participants WHERE thread_id ='+thread_id;
    connection.query(sql, function(err, res) {
        if (err) {
            callback(err);
        } else {
            callback(res);
        }
    });
}

exports.setUserLastRead = function(thread_id, user_id, callback) {
    var sql = 'UPDATE participants SET last_read = now() '+
              'WHERE thread_id = '+thread_id+' AND user_id = '+user_id;
    connection.query(sql, function(err, res) {
        if (err) throw err;
    });
}
