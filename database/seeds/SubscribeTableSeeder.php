<?php

use Illuminate\Database\Seeder;

class SubscribeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 2 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 3 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 4 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 5 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 6 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 7 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 8 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 9 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 10 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 11 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 12 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 13 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 14 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 15 ]);
        DB::table('subscribe')->insert(['user_id' => 1,  'sub_id' => 16 ]);
        DB::table('subscribe')->insert(['user_id' => 2,  'sub_id' => 1 ]);
        DB::table('subscribe')->insert(['user_id' => 3,  'sub_id' => 1 ]);
        DB::table('subscribe')->insert(['user_id' => 4,  'sub_id' => 1 ]);
        DB::table('subscribe')->insert(['user_id' => 5,  'sub_id' => 1 ]);
    }
}
