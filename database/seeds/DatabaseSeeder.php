<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoryTableSeeder::class);
        $this->call(VectorTableSeeder::class);
        $this->call(DifficultTableSeeder::class);

        $this->call(PhotoTableSeeder::class);

        $this->call(UserTableSeeder::class);
        $this->call(SubscribeTableSeeder::class);

        $this->call(CoursesTableSeeder::class);

        $this->call(CountryTableSeeder::class);
        $this->call(RegionTableSeeder::class);
        $this->call(CityTableSeeder::class);

        $this->call(FaqTableSeeder::class);
        $this->call(CreateLessons::class);
    }
}
