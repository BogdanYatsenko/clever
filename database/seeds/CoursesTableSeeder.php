<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statement = "ALTER TABLE courses AUTO_INCREMENT = 173;";
        DB::unprepared($statement);
    }
}
