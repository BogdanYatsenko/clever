<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';

    public function user()
    {
        return $this->belongsTo('App\User')->select('id', 'first', 'second');
    }

//    public function category()
//    {
//        return $this->belongsTo('App\Category');
//    }
//
//    public function vector()
//    {
//        return $this->belongsTo('App\Vector');
//    }

    public function difficult()
    {
        return $this->belongsTo('App\Difficult');
    }

    public function lessons()
    {
        return $this->hasMany('App\Lessons');
    }

    public function photo()
    {
        return $this->belongsTo('App\Photo');
    }

    public function materials()
    {
        return $this->belongsTo('App\Materials');
    }
    public function students()
    {
        return $this->hasMany('App\Student');
    }

    public function categories(){
        return $this->belongsToMany('App\Search_categories', 'clever_course_categories', 'id_course', 'id_category');
    }

    public function teacher(){
        return $this->hasMany('App\Course_Teacher', 'id_course', 'id');
    }

}
