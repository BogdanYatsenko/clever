<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Cmgmyr\Messenger\Traits\Messagable;

class User extends Authenticatable {
    use Notifiable;
    use Messagable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'first', 'second', 'password','confirmation_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function photo()
    {
        return $this->belongsTo('App\Photo');
    }

    public function subscribe_to()
    {
        return $this->hasMany('App\Subscribe', 'user_id')->select('sub_id');
    }

    public function subscribe()
    {
        return $this->hasMany('App\Subscribe', 'sub_id')->select('user_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function company_admin()
    {
        return $this->belongsTo('App\CompanyAdmin', 'id', 'user_id');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Teacher', 'id', 'id_user');
    }

}
