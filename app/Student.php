<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function user()
    {
        return $this->belongsTo('App\User')->select('id', 'first', 'second');
    }

    public function mark()
    {
        return $this->hasMany('App\Mark', 'student_id', 'user_id');
    }


}
