<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.11.2018
 * Time: 11:48
 */

namespace App;



use Illuminate\Database\Eloquent\Model;

class Students_offline extends Model
{
    protected $table = 'students_offline';

    public function offline_course(){
        return $this->belongsTo('App\OfflineCourse', 'id_offlinecourse', 'id');
    }
}