<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'region';
    public $timestamps = false;

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function city()
    {
        return $this->hasMany('App\City');
    }
}
