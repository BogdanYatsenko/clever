<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Book extends Model
{
    protected $table = 'book';

    public function user()
    {
        return $this->belongsTo('App\User')->select('id', 'first', 'second');
    }

    public function lessons()
    {
        return $this->hasMany('App\Lessons');
    }

}
