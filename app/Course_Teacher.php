<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.10.2018
 * Time: 15:13
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Course_Teacher extends Model
{
    protected $table = 'courses_teachers';

    public function teacher(){
        return $this->belongsTo('App\Teacher', 'id_teacher', 'id_teacher');
    }

    public function course(){
        return $this->belongsTo('App\Course', 'id_course', 'id');
    }
}