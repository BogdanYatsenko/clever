<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.11.2018
 * Time: 14:59
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Students_meetings extends Model
{
    protected $table = 'students_meetings';

    public function meetings(){
        return $this->belongsTo('App\Meeting', 'id_meeting', 'id_meeting');
    }
}