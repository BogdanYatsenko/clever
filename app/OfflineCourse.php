<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 25.09.2018
 * Time: 10:50
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class OfflineCourse extends Model
{
    protected $table = 'offlinecourses';


//    public function teacher()
//    {
//        return $this->belongsTo('App\Teacher', 'teacher_id','id_teacher');
//    }

    public function teacher(){
        return $this->hasMany('App\OfflineCourse_Teacher', 'id_offlinecourse', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'id_user_creator', 'id');
    }

    public function photo(){
        return $this->belongsTo('App\Photo', 'photo_id', 'id');
    }
    public function source(){
        return $this->belongsTo('App\Sources', 'id_source', 'id');
    }

}