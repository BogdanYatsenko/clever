<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.11.2018
 * Time: 15:00
 */

namespace App;

use Illuminate\Database\Eloquent\Model;


class Meeting_types extends Model
{
    protected $table = "meetings_types";
    public $timestamps = false;

    public function type(){
        return $this->belongsTo('App\Meeting', 'id', 'id_type');
    }

}