<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 26.09.2018
 * Time: 15:20
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $table = 'teachers';

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user', 'id');
    }

    public function offline()
    {
        return $this->belongsTo('App\OfflineCourse', 'id_teacher', 'teacher_id');
    }



}