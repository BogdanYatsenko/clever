<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model
{
    protected $table = 'subscribe';
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }


}
