<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.10.2018
 * Time: 16:46
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Meeting_Teacher extends Model
{
    protected $table = "meetings_teachers";

    public function teacher(){
        return $this->belongsTo('App\Teacher', 'id_teacher', 'id_teacher');
    }

    public function meeting(){
        return $this->belongsTo('App\Meeting', 'id_meeting', 'id_meeting');
    }
}