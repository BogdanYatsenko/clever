<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tasks;
use Auth;

class TaskController extends Controller
{
    public function edit($course_id, $task_id)
    {
        $task = Tasks::whereid($task_id)->first();

        if ($task->user_id != Auth::id()) return  redirect('/id'.Auth::id());

        $view = view('inside.courses.tasks.edit')->with([
            'task_id' => $task_id,
            'task' => $task,
            'course_id' => $course_id
        ]);

        if (request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title']
            ]);
        }

        return $view;
    }


    public function save(Request $request, $course_id, $task_id)
    {

        $task = Tasks::whereid($task_id)->first();
        if ($task->user_id != Auth::id()) return 0;


        $questions_count = -1;
        $variant_count = 0;

        $parsed['name'] = 'Новое задание';
        $parsed['is_rating'] = 0;
        $parsed['is_date_of'] = 0;
        $parsed['is_material_look'] = 0;

        foreach ($request->task as $elem) {
            \DebugBar::info($elem['value'] == 'on');
            if ($elem['name'] == 'task_name') {
                $parsed['name'] = $elem['value'];
            }

            if ($elem['name'] == 'task_text') {
                $parsed['task_text'] = $elem['value'];
            }

            if ($elem['name'] == 'task_time') {
                $parsed['date_of'] = $elem['value'];
            }

            if ($elem['name'] == 'is_rating') {
                if ($elem['value'] == 'on')
                    $parsed['is_rating'] = 1;
            }

            if ($elem['name'] == 'task_off_time') {
                if ($elem['value'] == 'on')
                    $parsed['is_date_of'] = 1;
            }

            if ($elem['name'] == 'task_access') {
                if ($elem['value'] == 'on')
                    $parsed['is_material_look'] = 1;
            }

            if ($elem['name'] == 'task_comment') {
                $parsed['comment'] = $elem['value'];
            }


        }

        $task = Tasks::whereid($task_id)->update(['json' => json_encode($parsed, JSON_UNESCAPED_UNICODE),
                                                    'name' => $parsed['name'],
                                                    'task_text' => $parsed['task_text'],
                                                    //'date_of' => $parsed['task_time'],
                                                    'is_rating' => $parsed['is_rating'],
                                                    'is_date_of' => $parsed['is_date_of'],
                                                    'is_material_look' => $parsed['is_material_look'],
                                                    'comment' => $parsed['comment']]);

        return 1;
    }


    public function create($course_id)
    {
        $task = new Tasks;
        $task->name = 'Новое задание';
        $task->json = '{"name":"Новое задание","is_rating":0,"is_date_of":0,"is_material_look":0,"task_text":"","date_of":"","comment":""}';
        $task->user_id = Auth::id();
        $task->task_text = '';
        $task->date_of = null;
        $task->is_rating = 0;
        $task->is_date_of = 0;
        $task->is_material_look = 0;
        $task->comment = '';
        $task->save();

        if ($task->user_id != Auth::id()) return  redirect('/id'.Auth::id());

        $view = view('inside.courses.tasks.edit')->with([
            'task_id' => $task->id,
            'task' => $task,
            'course_id' => $course_id
        ]);

        if (request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
                'id' =>  $task->id
            ]);
        }

        return $view;
    }
}
