<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Files;


class UploadController extends Controller
{
    public function file_upload(Request $request){

        if($request->hasFile('fileload')) {
            \DebugBar::info($request->file('fileload'));
            $file = $request->file('fileload');
            $file_name = $file->getClientOriginalName();
            $file_ext = $file->getClientOriginalExtension();
            $file_size = $file->getSize();
            if($file_size > 20000000){
                return 0;
            }
            if (!is_dir(storage_path().'/app/public/user_' . Auth::id())) {
                Storage::makeDirectory('/public/user_' . Auth::id());
            }
            if (!is_dir(storage_path().'/app/public/user_' . Auth::id().'/'.$file_ext)) {
                Storage::makeDirectory('/public/user_' . Auth::id().'/'.$file_ext);
            }
            $file_hash =  md5_file($file);
            $file_one = Files::where('hash', $file_hash)->first();
            if($file_one == null) {

                $file_save = new Files;
                $file_save->user_id = Auth::id();
                $file_save->name = $file_name;
                $file_save->type = $file_ext;
                $file_save->hash = $file_hash;
                $file_save->save();

                $file->storeAs('/public/user_' . Auth::id() . '/' . $file_ext, $file_hash . '.' . $file_ext);

                $file_array = collect();
                $file_array->push(['name'=>$file_name, 'type'=>$file_ext,'hash'=>$file_hash]);

                return $file_array;
            }
            return 1;
        }
        return 0;

    }

    public function file_get_all(){

        $files = Storage::allFiles('public/user_'.Auth::id());

        $files_db = Files::where('user_id', Auth::id())
            ->orderBy('id', 'desc')
            ->select('name', 'hash')->get();

        $file_array = collect();

        foreach ($files_db as $file_db){
            foreach ($files as $file)  {
                if (pathinfo($file, PATHINFO_FILENAME) == $file_db->hash) {
                    $file_array->push(['name'=>$file_db->name, 'type'=>pathinfo($file, PATHINFO_EXTENSION),'hash'=>$file_db->hash]);
                }
            }
        }

        return $file_array;

    }
}
