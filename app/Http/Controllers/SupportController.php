<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Support;
use Illuminate\Support\Facades\Auth;

class SupportController extends Controller
{
    public function send_question(Request $request){
        $question = new Support;
        $question->name = $request->name;
        $question->text = $request->text;
        $question->question_status = 2;
        $question->first_msg_id = 0;
        $question->md5 = md5($request->email.$request->text.rand(1, 10000));

        $user_id = Auth::id();

        if ($user_id){
            $question->user_id = $user_id;
            $question->email = 'login';
        } else {
            $question->email = $request->email;
            $question->user_id = 0;
        }

        $question->save();

        return 1;
    }

    public function send_answer($id, $admin_id, $answer){
        Support::where('id', $id)
            ->update(['answer' => $answer, 'admin_id' => $admin_id]);
    }
}
