<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\faq;
class FaqController extends Controller
{
    public function index($page_id){
        $questions = faq::all()->where('page_id', $page_id);
        return $questions;
    }

}
