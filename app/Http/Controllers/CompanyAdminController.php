<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.10.2018
 * Time: 10:28
 */

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use App\CompanyAdmin;
use Auth;
use App\Teacher;

class CompanyAdminController extends Controller
{
    public function became($user_id){
        return view('inside.company_admin.became');
    }

    public function add_company_admin(Request $request){
        $company_admin = new CompanyAdmin();

        $company_admin->user_id = Auth::id();
        $company_admin->contact_telephone = $request->telephone;
        $company_admin->is_moderated = 0;
        $company_admin->save();

        $view = view('inside.company_admin.create_success');

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'title' => $sections['title'],
            ]);
        }

        return $view;
    }


    public function accept_moderation($id){
        CompanyAdmin::where('id', $id)
            ->update(['is_moderated' => 1]);

    }

    public function reject_moderation($id){
        CompanyAdmin::where('id', $id)
            ->delete();
    }

    public function add_teacher(Request $request){
        $user = new User();
        $user->first = $request->name;
        $user->second = $request->second_name;
        $user->third = $request->third_name;
        $user->login = $request->email;

        $password = hash('sha256', $request->password);
        $user->password = $password;

        $user->teacher()->is_moderated = 1;
        $user->save();

        $teacher = new Teacher();
        $teacher->id_user = $user->id;
        $teacher->is_moderated = 0;
        $teacher->save();


        return response()->json([
            'first' => $user->first,
            'second' => $user->second,
            'teacher_id' => $teacher->id,
        ]);
    }
}