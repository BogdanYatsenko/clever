<?php

namespace App\Http\Controllers;

use App\CompanyAdmin;
use App\Students_offline;
use App\Teacher;
use Illuminate\Http\Request;
use App\Difficult;
use App\Lessons;
use App\EventsModerated;
use DebugBar\DebugBar;
use App\User;
use App\Photo;
use App\Subscribe;
use App\Course;
use App\Category;
use App\Student;
use App\XhrCourse;
use App\Tasks;
use App\UserTasks;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Image;
use Auth;
use App\Materials;
use App\UserAnswer;
use App\Quiz;
use App\Log;
use App\Mark;
use App\OtherCourses;
use App\OfflineCourse;
use App\OfflineCourse_Teacher;
use App\Course_Teacher;

use Illuminate\Support\Facades\DB;

class CourseController extends Controller
{
    public function index($course_id){

        $course = Course::with(['user'])
            ->where('id', $course_id)
            ->first();
        $materials = Materials::where('course_id', $course_id)
            ->select('id', 'name')->orderBy('priority')->get();

        $view = view('inside.courses.card')->with([
            'course'=>$course,
            'materials' => $materials
        ]);

        //dd($course);

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
                'materials' => $sections['content']
            ]);
        }

        return $view;
    }

    public function index_other($course_id){

        $course = OtherCourses::with(['photo'])->
        where('id', $course_id)
            ->first();


        $view = view('inside.courses.other_card')->with([
            'course'=>$course
        ]);

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
                'materials' => $sections['content']
            ]);
        }

        return $view;
    }


    public function material_reader($course_id){
        $course = Course::with(['user', 'category', 'vector'])
            ->where('id', $course_id)
            ->first();

        $is_student = Student::where('user_id', Auth::id())->where('course_id', $course_id)->first();
        $is_author = Course::whereid($course_id)->where('user_id', Auth::id())->first();
        $is_admin = User::whereid(Auth::id())->where('is_admin', 1)->first();

        $materials = Materials::where('course_id', $course_id)
            ->select('id', 'type', 'name', 'json')->orderBy('priority')->get();

        $i = 0;
        $huge_mas = array();
        $renderedTest = '';
        foreach ($materials as $material){
            $json = $material->json;
            $array = explode('#slide', $json);
            $huge_mas[$i] = $array;
            $i = $i + 1;

//            if($i == 1){
//                break;
//            }
//            else{
//                $i = $i +1;
//            }
        }
//        echo implode($huge_mas[]);

        $user_answer = UserAnswer::where('user_id', Auth::id())->select('material_id')->get();
        $user_tasks = UserTasks::where('user_id', Auth::id())->select('material_id','json')->get();

        if (!($is_student || $is_author || $is_admin)) return redirect('/course/card/id'.$course_id);

        $view = view('inside.courses.material_reader')->with([
            'course'=>$course,
            'materials' => $materials,
            'user_answer' => $user_answer,
            'user_tasks' => $user_tasks,
            'slides' => $huge_mas
        ]);


        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
                'materials' => $sections['content'],
                'course' =>$sections['content']

            ]);
        }

        return $view;
    }

    public function send_to_moderate($course_id)
    {
        $course = Course::where('id', $course_id)->first();
        $is_moderated = $course->is_moderated;
        $author_id = $course->user_id;


        if (($author_id == Auth::id()) && (!$is_moderated)){
            EventsModerated::insert(
                ['user_id' => $author_id, 'course_id' => $course_id]
            );

            Course::where('id', $course_id)
                ->update(['is_moderated' => 2]);

            return 0;
        } else return $is_moderated;
    }

    public function send_to_moderate_offline($course_id){
        OfflineCourse::where('id', $course_id)
            ->update(['is_moderated' => '1']);

    }


    public function accept_moderation($event_id)
    {
        $course_id = EventsModerated::where('id', $event_id)->pluck('course_id')->first();

        EventsModerated::where('id', $event_id)
            ->update(['accept' => 1]);

        Course::where('id', $course_id)
            ->update(['is_moderated' => 1]);

        return 1;
    }

    public function reject_moderation($event_id)
    {
        $comment = '';
        $course_id = EventsModerated::where('id', $event_id)->pluck('course_id')->first();

        EventsModerated::where('id', $event_id)
            ->update(['accept' => 2, 'comment' => $comment]);

        Course::where('id', $course_id)
            ->update(['is_moderated' => 0]);

        return 1;
    }

    public function accept_moderation_offline($id){

        OfflineCourse::where('id', $id)
            ->update(['is_moderated'=>2]);
        return 1;
    }

    public function reject_moderation_offline($id)
    {
        OfflineCourse::where('id', $id)
            ->update(['is_moderated'=>0]);
        return 1;
    }



    public function get_course_card($course_id){
        $course = Course::with(['user', 'category', 'vector'])
            ->where('id', $course_id)
            ->get();
        return $course;
    }



    public function create_course()
    {
        $teachers = Teacher::with('user')->get();

        $company_admin = CompanyAdmin::with('user')
            ->where('user_id', Auth::id())
            ->get();

        //dd($teachers);

        $view = view('inside.courses.create')->with([
            'teachers' => $teachers,
            'company_admin' => $company_admin,
        ]);

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
            ]);
        }

        return $view;

    }

    public function edit_course($course_id)
    {
        $course = Course::with(['user', 'lessons'])
            ->where('id', $course_id)
            ->first();
        if($course->user_id == $user_id = Auth::id()) {

            $view = view('inside.courses.edit')->with([
                'course' => $course
            ]);

            if (request()->ajax()) {
                $sections = $view->renderSections();
                return response()->json([
                    'content' => $sections['content'],
                    'modal' => $sections['modal'],
                    'title' => $sections['title'],
                ]);
            }

            return $view;
        }
        return redirect('/id'.Auth::id());

    }

    public function del_course(Request $request){
        $course = Course::where('id', $request->course_id)
            ->select('id', 'user_id')
            ->first();
        if ($course->user_id == $user_id = Auth::id()) {
            $course->delete();

            $user = User::with(['subscribe', 'subscribe_to', 'photo'])->get();
            $course = Course::with(['photo'])->whereuser_id(Auth::id())->get();
            $user_id = $user->pluck('id')->search($user_id);


            $view = view('inside.userpage')->with([
                'user' => $user,
                'course' =>$course,
                'user_id' => $user_id

            ]);

            if(request()->ajax()) {
                $sections = $view->renderSections();
                return response()->json([
                    'content' => $sections['content'],
                    'modal' => $sections['modal'],
                    'title' => $sections['title'],
                ]);
            }

            return $view;
        }
        return redirect('/id'.Auth::id());
    }

    public function add_course(Request $request)
    {

        if($request->type == 0){
            $course = new Course();
            $course->name = $request->name;
            $course->price = $request->price;
            $course->user_id = Auth::id();

            if (isset($request->discription)){
                $course->description = $request->discription;
            }
            if (isset($request->full_description)){
                $course->full_description = $request->full_description;
            }
            if (isset($request->material_link)){
                $course->material_link = $request->material_link;
            }
            $course->save();

            for($i=0; $i<count($request->teacher_id); $i++)
            {

                $connection = new Course_Teacher();
                $connection->id_teacher = $request->teacher_id[$i];
                $connection->id_course = $course->id;
                $connection->is_moderated_by_teacher = 0;
                $connection->save();
            }

            $course = $course->id;
            $view = view('inside.courses.create_success')->with([
                'course' => $course
            ]);
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                //'modal' => $sections['modal'],
                //'title' => $sections['title'],
            ]);
        }
        else{
            $offlineCourse = new OfflineCourse();

            $offlineCourse->name = $request->name;
            $offlineCourse->description = $request->discription;
            $offlineCourse->full_description = $request->full_description;
            $offlineCourse->course_length_hours = $request->course_length_hours;
            $offlineCourse->lessons_count = $request->lessons_count;
            if($request->start_date != null){
                $start = date('Y-m-d H:m', strtotime($request->start_date));
                $offlineCourse->start_date = $start;
            }
            $offlineCourse->max_number_students = $request->max_number_students;
            $offlineCourse->id_user_creator = Auth::id();
            $offlineCourse->author = $request->author;
            $offlineCourse->author_link = $request->author_link;
            $offlineCourse->num_views = 0;
            $offlineCourse->rating = 0;
            $offlineCourse->count_listeners = 0;
            $offlineCourse->difficult_id = $request->difficulty;
            $offlineCourse->price = $request->price;
            $offlineCourse->audience = $request->audience;
            $offlineCourse->knowledge_requires = $request->knowledge_requires;
            $offlineCourse->skills = $request->skills;
            $offlineCourse->address = $request->address;
            $offlineCourse->registration_count = 0;
            $offlineCourse->is_moderated = 0;

            $offlineCourse->save();
            //dd($request->teacher_ids[0]);

            for($i=0; $i<count($request->teacher_ids); $i++)
            {
                $connection = new OfflineCourse_Teacher();
                $connection->id_teacher = $request->teacher_ids[$i];
                $connection->id_offlinecourse = $offlineCourse->id;
                $connection->is_moderated_by_teacher = 0;
                $connection->save();
            }

            $view = view('inside.courses.offline_create_success')->with([
            ]);
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
            ]);
        }

    }

    public function config_course($course_id)
    {
        $course = Course::with(['user', 'lessons', 'difficult'])
            ->where('id', $course_id)
            ->first();

        $difficult = Difficult::all();
        if($course->user_id == Auth::id()) {

            $view = view('inside.courses.config')->with([
                'course' => $course,
                'v_id' => 0,
                'c_id' => 0,
                'difficult' => $difficult
            ]);

            if (request()->ajax()) {
                $sections = $view->renderSections();
                return response()->json([
                    'content' => $sections['content'],
                    'modal' => $sections['modal'],
                    'title' => $sections['title'],
                ]);
            }
            return $view;
        }
        return redirect('/id'.Auth::id());
    }

    public function apply_config_course(Request $request){

        $course_id = $request->course_id;
        $course = Course::whereid($course_id)->first();
        if($course->user_id == Auth::id()) {
            $course->name = $request->name;
            $course->price = $request->price;
            if (isset($request->discri)) {
                $course->vector_id = $request->vector;
            }
            if (isset($request->difficult)) {
                $course->difficult_id = $request->difficult;
            }
            if (isset($request->discription)) {
                $course->discription = $request->discription;
            }
            if (isset($request->full_description)) {
                $course->full_description = $request->full_description;
            }

            $course->save();

            $view = view('inside.courses.config_apply')->with([
                'course' => $course
            ]);

            if (request()->ajax()) {
                $sections = $view->renderSections();
                return response()->json([
                    'content' => $sections['content'],
                    'modal' => $sections['modal'],
                    'title' => $sections['title'],
                ]);
            }
            return $view;
        }
        return redirect('/id'.Auth::id());
    }

    public function buy_course($course_id){
        $user = Auth::id();
        $is_buy = Student::where('user_id',$user)->where('course_id',$course_id)->count();
        if($is_buy == 0){
            $course = Course::where('id', $course_id)
                ->select('id','user_id', 'is_moderated', 'material_link')
                ->first();
            if($course->is_moderated == 1 && $course->user_id != $user){
                $student = new Student;
                $student->user_id =  $user;
                $student->course_id =  $course->id;
                if($course->material_link != null){
                    $student->xhr = 1;
                }
                if($course->price == 0)
                    $student->pay = 1;
                else
                    $student->pay = 0;

                $student->save();
                return redirect('/course/id'.$course_id.'/materials');
            }
            return redirect('/course/id'.$course_id.'/materials');
        }
        return redirect('/course/id'.$course_id.'/materials');
    }


    public function buy_offline_course($course_id){
        $user = Auth::id();
        $price = OfflineCourse::where('id', $course_id)->select('price')->first();
        if ($price->price == 0)
        {
            $is_buy = Students_offline::where('user_id', $user)->where('id_offlinecourse', $course_id)->count();
            if ($is_buy == 0){
                $stud_off = new Students_offline();
                $stud_off->id_offlinecourse = $course_id;
                $stud_off->user_id = $user;
                $stud_off->pay = 1;
                $stud_off->save();
                return $user;
            }
            else{
                return 1;
            }
        }
        else{
            return 2;
        }

    }


    public function upload_avatar(Request $request){
        // Handle the user upload of avatar
        $course = Course::where('id', $request->course_id)
            ->select('user_id')
            ->first();
        if ($course->user_id == $user_id = Auth::id()) {
            if ($request->hasFile('avatar')) {
                $course_id = $request->course_id;
                $avatar = $request->file('avatar');
                $filename = $course_id . '.' . $avatar->getClientOriginalExtension();
                $img = Image::make($avatar);
                if ($img->height() >= $img->width()) {
                    $img->resize(null, 190, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $img->resize(295, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save(storage_path() . '/app/public/course_avatars/' . $filename);
//            $img->save( public_path('/uploads/course_avatars/' . $filename ) );

                if ($img->height() >= $img->width()) {
                    $img->resize(null, 140, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $img->resize(140, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save(storage_path() . '/app/public/course_avatars/small/' . $filename);
//            $img->save( public_path('/uploads/course_avatars/small/' . $filename ) );

                $course = Course::whereid($course_id)->first();

                if ($avatar->getClientOriginalExtension() == "png") {
                    $course->photo_id = 1;
                }
                if ($avatar->getClientOriginalExtension() == "jpg") {
                    $course->photo_id = 2;
                }
                if ($avatar->getClientOriginalExtension() == "gif") {
                    $course->photo_id = 3;
                }

                $course->save();
            }

            return $filename;
        }
        return redirect('/id'.Auth::id());
    }

    public function start($course_id,$material_id)
    {
        $quiz = Materials::whereid($material_id)->first();

        $new_log = new Log;

        $new_log->user_id = Auth::id();
        $new_log->material_id = $material_id;
        $new_log->result = 0;

        $new_log->save();


        $view = view('inside.courses.quiz.quiz_start')->with([
            'quiz_id' => $material_id,
            'quiz' => json_decode($quiz->json,false,JSON_UNESCAPED_UNICODE),
            'course_id' => $course_id
        ]);

        if (request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title']
            ]);
        }

        return $view;
    }

    public function result($course_id, $quiz_id)
    {

        $quiz = Materials::whereid($quiz_id)->first();
        $quiz = json_decode($quiz->json,false,JSON_UNESCAPED_UNICODE);

//        $points = 0;

//        foreach ($quiz->questions as $question){
//            foreach ($answers as $answer){
//                if ($answer->question_id == $question->id){
//                    foreach($question->variant as $variant){
//                        if (isset($variant->is_true)){
//                            if ($answer->variant_id == $variant->id){
//                                $points++;
//                            }
//                        }
//                    }
//                }
//            }
//        }

        $user_answer = UserAnswer::where('user_id', Auth::id())
            ->where('material_id', $quiz_id)
            ->get();


        if($user_answer == null || $user_answer =="[]")
            return redirect('/course/id'.$course_id.'/quiz'.$quiz_id.'/start');

        //$quiz = Quiz::whereid($quiz_id)->select('name')->first();

        $view = view('inside.courses.quiz.result')->with([
            'quiz_id' => $quiz_id,
            'quiz' => $quiz,
            'user_answers' => $user_answer,
            'course_id' => $course_id

        ]);

        if (request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title']
            ]);
        }

        return $view;
    }

    public function save_result(Request $request, $course_id, $quiz_id){
        $last = Log::where('user_id', Auth::id())
            ->where('material_id', $quiz_id)
            ->orderBy('id', 'desc')
            ->first();

        $last->result = $request->result;
        $last->save();
    }

    public function complete_task(Request $request, $course_id, $task_id)
    {
        $user_tasks = UserTasks::where('user_id', Auth::id())
            ->where('material_id', $task_id)
            ->count();

        if($user_tasks == 0) {

            $parsed['task_text'] = $request->task_text;
            $user_tasks = new UserTasks;
            $user_tasks->user_id = Auth::id();
            $user_tasks->material_id = $request->material_id;
            $user_tasks->json = json_encode($parsed, JSON_UNESCAPED_UNICODE);
            $user_tasks->save();

            return $user_tasks;
        }
        return $user_tasks;

    }


    public function my_results($course_id){

        $results = Log::groupBy('material_id')->get([DB::raw('MAX(id) as id')]);
        $results = Log::whereIn('id', $results)->get();

        $course = Course::with(['user'])
            ->where('id', $course_id)
            ->first();


        $is_student = Student::where('user_id', Auth::id())->where('course_id', $course_id)->first();

        $materials = Materials::with('mark')->where('course_id', $course_id)
            ->select('id', 'type', 'name', 'json')->orderBy('priority')->get();

        if (!($is_student)) return redirect('/course/card/id'.$course_id);

        $view = view('inside.user.my_results')->with([
            'course'=>$course,
            'materials' => $materials,
            'results' => $results
        ]);

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
                'materials' => $sections['content'],
                'course' =>$sections['content'],
                'results' => $sections['content']
            ]);
        }

        return $view;
    }

    public function result_user($course_id, $quiz_id, $user_id){

        $is_author = Course::whereid($course_id)->where('user_id', Auth::id())->first();
        if (!($is_author)) return redirect('/course/card/id'.$course_id);

        $is_student = Student::where('user_id', $user_id)->where('course_id', $course_id)->first();

        if (!($is_student)) return redirect('/course/card/id'.$course_id);

        $quiz = Materials::whereid($quiz_id)->first();
        $quiz = json_decode($quiz->json,false,JSON_UNESCAPED_UNICODE);

        $user_answer = UserAnswer::where('user_id', $user_id)
            ->where('material_id', $quiz_id)
            ->get();

        if($user_answer == null || $user_answer =="[]")
            return redirect('/course/id'.$course_id.'/my_students');


        $view = view('inside.courses.quiz.result')->with([
            'quiz_id' => $quiz_id,
            'quiz' => $quiz,
            'user_answers' => $user_answer,
            'course_id' => $course_id

        ]);

        if (request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title']
            ]);
        }

        return $view;

        $view = view('inside.user.my_results')->with([
            'course'=>$course,
            'materials' => $materials,
            'results' => $results
        ]);

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
                'materials' => $sections['content'],
                'course' =>$sections['content'],
                'results' => $sections['content']
            ]);
        }

        return $view;
    }

    public function my_students($course_id){

        $is_author = Course::whereid($course_id)->where('user_id', Auth::id())->first();
        $students = Student::with('user')->with('mark')->where('course_id', $course_id)->get();

        $course = Course::with(['user'])
            ->where('id', $course_id)
            ->first();

        $materials = Materials::where('course_id', $course_id)
            ->select('id', 'type', 'name', 'json')->orderBy('priority')->get();

        if (!($is_author)) return redirect('/course/card/id'.$course_id);

        $view = view('inside.user.my_students')->with([
            'course'=>$course,
            'materials' => $materials,
            'students' => $students
        ]);

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
                'materials' => $sections['content'],
                'course' =>$sections['content'],
                'results' => $sections['content'],
                'students' => $sections['content']
            ]);
        }

        return $view;
    }

    public function get_student_results(Request $request, $course_id){

        $is_author = Course::whereid($course_id)->where('user_id', Auth::id())->first();

        if (!($is_author)) return redirect('/course/card/id'.$course_id);

        $material_id = $request->material_id;
        $student_id = $request->student_id;

        $data = Log::where('user_id', $student_id)->where('material_id', $material_id)->get();
        return $data;
    }

    public function set_mark_to_student(Request $request){

        //$course_id = Materials::where('id', $request->material_id)->select('course_id')->first();

        //$is_author = Course::whereid($course_id)->where('user_id', Auth::id())->first();
       // if (!($is_author)) return 0;
        $student_id = $request->student_id;
        $material_id = $request->material_id;
        $mark = $request->mark;

        $markOld = Mark::where('student_id', $student_id)->where('material_id', $material_id)->first();

        if (isset($markOld)) {
            $markOld->mark = $mark;
            $markOld->save();
            return 2;
        } else {
            $markNew = new Mark;
            $markNew->student_id = $student_id;
            $markNew->material_id = $material_id;
            $markNew->mark = $mark;
            $markNew->save();
            return 1;
        }
    }





    /*Bogdan*/


    public function offline_index($offline_course_id)
    {

        $meeting = OfflineCourse::where('id', $offline_course_id)->get();
        $first_meetings = $meeting -> first();

        //dd($offline_course_id);

        $view = view('inside.courses.offline_course')->with([
            'meetings' => $meeting,
            'first_meetings' => $first_meetings,
        ]);



        if (request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title']
            ]);
        }

        return $view;
    }

    public function offline_course_update(Request $request)
    {

        OfflineCourse::where(['id' => $request -> id])
            ->update(['name' => $request->name, 'description' => $request->discription, 'price' => $request->price, 'full_description' => $request->full_description,
                'course_length_hours' => $request->course_length_hours, 'max_number_students' => $request->max_number_students,
                'start_date' => $request->start_date, 'author' => $request->author, 'author_link' => $request->author_link,
                'address' => $request->address, 'knowledge_requires' => $request->knowledge_requires, 'skills' => $request->skills,
                'difficult_id' => $request->difficulty, 'lessons_count' => $request->lessons_count, 'audience' => $request->audience ]);
        $view = view('inside.courses.create_offline_success')->with([

        ]);



        if (request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'title' => $sections['title']
            ]);
        }

        return $view;
    }


    public function delete_offline_course($meeting_id)
    {
        OfflineCourse::where(['id' => $meeting_id])
            ->delete();
        OfflineCourse_Teacher::where('id_offlinecourse', $meeting_id )->delete();
        return redirect('/id'.Auth::id());
    }


    public function offline_card_index($offline_course_id)
    {

        $meeting = OfflineCourse::where('id', $offline_course_id)->first();
        $teachers = OfflineCourse_Teacher::where('id_offlinecourse', $offline_course_id)->get();

        $view = view('inside.courses.offline_card')->with([
            'course' => $meeting,
            'teachers' => $teachers
        ]);

        if (request()->ajax()) {
            $sections = $view->renderSections();

            return response()->json([
                'content' => $sections['content'],
                'title' => $sections['title']
            ]);
        }
        return $view;
    }

    public function update_rating($meeting_id, $rating)
    {

        $prev_rating = OfflineCourse::where('id', $meeting_id)->select('rating')->first();
        $prev_num_voiters = OfflineCourse::where('id', $meeting_id)->select('num_voiters')->first();
        $num_voiters = $prev_num_voiters->num_voiters + 1;
        $rating =  $prev_rating->rating + $rating;
        OfflineCourse::where('id', $meeting_id)->update([
            'rating' => $rating,
            'num_voiters' => $num_voiters,
        ]);
    }


    public function delete_course_from_userpage($course_id)
    {

        Student::where('course_id', $course_id)
            ->where('user_id', Auth::id())
            ->delete();
        return redirect('/id'.Auth::id());
    }

    public function delete_offline_from_userpage($course_id){
        Students_offline::where('id_offlinecourse', $course_id)
            ->where('user_id', Auth::id())
            ->delete();
        return 1;
    }

}
