<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Photo;

class ChatController extends Controller
{
    public function index() {
        return view('inside.chat');
    }

    public function autocomplete(Request $req) {

        // $searchq = $req->input('q');

        // $searchq = preg_replace("#[^0-9a-zа-я]#i", "", $searchq);
        // $keywords = explode(' ', $searchq);
        // \DebugBar::info($searchq);
        // \DebugBar::info(count($keywords));
        // if (count($keywords) > 1) {
            // $users =DB::table('users')
            // ->join('photo', 'users.photo_id', '=', 'photo.id')
            // ->select('users.id', 'users.first', 'users.second', 'photo.type')
            // ->where('first', 'like', '%'.$keywords[0].'%')
            // ->orWhere('first', 'like', '%'.$keywords[1].'%')
            // ->orWhere('second', 'like', '%'.$keywords[0].'%')
            // ->orWhere('second', 'like', '%'.$keywords[1].'%')
            // ->select('users.id', 'users.first', 'users.second', 'users.login', 'users.photo_id')
            // ->get();
            // \DebugBar::info($keywords[0]." ".$keywords[1]);
        // } else {
        //     $users =DB::table('users')
        //     ->join('photo', 'users.photo_id', '=', 'photo.id')
        //     ->select('users.id', 'users.first', 'users.second', 'photo.type')
        //     ->where('first', 'like', '%'.$keywords[0].'%')
        //     ->orWhere('second', 'like', '%'.$keywords[0].'%')
        //     ->select('users.id', 'users.first', 'users.second', 'users.login', 'users.photo_id')
        //     ->get();
        // }
        //
        //
        // \DebugBar::info($users);


        $query = $req->input('q');

        // $users = User::where('first', 'like', '%'.$query.'%')
        // ->orWhere('second', 'like', '%'.$query.'%')
        // ->select('id', 'first', 'second', 'login', 'photo_id')
        // ->get();

        $users = User::leftjoin('photo', 'users.photo_id', '=', 'photo.id')
        ->where('users.first', 'like', '%'.$query.'%')
        ->orWhere('users.second', 'like', '%'.$query.'%')
        ->select('users.id', 'users.first', 'users.second', 'users.login', 'photo_id', 'photo.type')
        ->get();

        return  $users;
   }
}
