<?php

namespace App\Http\Controllers;

use App\Meeting;
use App\OfflineCourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Student;
use App\EventsModerated;
use App\User;
use App\Support;
use App\XhrCourse;
use Carbon\Carbon;
use App\Teacher;
use App\CompanyAdmin;


use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Message;


class AdminController extends Controller
{
    public function index()
    {
        $moderation = EventsModerated::with('user', 'course')
            ->where('accept', 0)
            ->get();

        $questions = Support::where('user_id', '>', 0)
            ->with('user')
            ->where('question_status', 2)
            ->get();

        $questions_guests = Support::where('user_id', 0)
            ->where('question_status', 2)
            ->get();

        $xhr_check = XhrCourse::with('user', 'course.user', 'course.user')->where('is_check', 0)->get();

        $moderation_teacher = Teacher::with('user')
            ->where('is_moderated', 0)
            ->get();

        $moderation_offline = OfflineCourse::with('teacher')
            ->where('is_moderated', '=', 0)
            ->get();


        $moderation_company_admins = CompanyAdmin::with('user')
            ->where('is_moderated', 0)
            ->get();

        $meetings = Meeting::with('user')
            ->where('is_moderated', 0)
            ->get();
            //dd($meetings);

        //dd($moderation_offline);


       return view('inside.admin.index')->with([
            'moderation' => $moderation,
            'questions' => $questions,
            'questions_guests'=>$questions_guests,
            'xhr_check'=>$xhr_check,
            'moderation_teacher' => $moderation_teacher,
            'moderation_offline' => $moderation_offline,
            'moderation_company_admins' => $moderation_company_admins,
            'meetings' => $meetings,
       ]);
    }

    public function accept_xhr($xhr_id, $student_id, $course_id){
        $xhr = XhrCourse::with('course')->where('id', $xhr_id)->first();
        $xhr->is_check = 1;
        $xhr->save();

        $student = Student::where('user_id',$student_id)->where('course_id',$course_id)->first();
        $student->xhr_check = 1;
        $student->save();


        // send message to student
        $admin_id = $xhr->course->user_id;

        $target = DB::table(DB::raw('(select *from  participants
        where user_id = '.$student_id.') as tb1'))
        ->join(DB::raw('(select *from  participants
        where user_id = '.$admin_id.') as tb2'), 'tb2.thread_id', '=', 'tb1.thread_id')
        ->join('threads', 'tb1.thread_id', '=', 'threads.id')
        ->whereNull('threads.subject')
        ->first();


        if (empty($target)) {
            $thread = Thread::create();

            Participant::create([
                'thread_id' => $thread->id,
                'user_id' => $admin_id,
                'last_read' => new Carbon
            ]);

            $thread->addParticipant($student_id);
        }  else $thread = $target;

        $message = 'Курс ' . $xhr->course->name .
                   '<br> Логин: ' . $xhr->login .
                   '<br> Пароль: ' . $xhr->password .
                   '<br> Инструкция: ' . $xhr->comment;

        Message::create([
          'thread_id' => $thread->id,
          'user_id' => $admin_id,
          'body' => $message
        ]);

        return $xhr->id;
    }

    public function reject_xhr($xhr_id, $student_id, $course_id){
        $xhr = XhrCourse::find($xhr_id);
        $xhr->is_check = 2;
        $xhr->save();

        $student = Student::where('user_id',$student_id)->where('course_id',$course_id)->first();
        $student->xhr_check = 2;
        $student->save();
        return $xhr->id;

    }
}
