<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use Auth;
use App\Materials;
use App\UserAnswer;

class QuizController extends Controller
{

    public function edit($course_id, $quiz_id)
    {
        $quiz = Quiz::whereid($quiz_id)->first();

        if ($quiz->user_id != Auth::id()) return  redirect('/id'.Auth::id());

        $view = view('inside.courses.quiz.quiz_editor')->with([
            'quiz_id' => $quiz_id,
            'quiz' => json_decode($quiz->json,false,JSON_UNESCAPED_UNICODE),
            'course_id' => $course_id
        ]);

        if (request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title']
            ]);
        }

        return $view;
    }


    public function save(Request $request, $course_id, $quiz_id)
    {

        $quiz = Quiz::whereid($quiz_id)->first();
        if ($quiz->user_id != Auth::id()) return 0;


        $questions_count = -1;
        $variant_count = 0;

        $parsed['name'] = 'Новый тест';

        foreach ($request->quiz as $elem){
            if ($elem['name'] == 'test_name') {
                $parsed['name']  = $elem['value'];
            }

            if ($elem['name'] == 'question_id') {
                $questions_count++;
                $variant_count = -1;
                $parsed['questions'][$questions_count]['id'] = $elem['value'];
            }

            if ($elem['name'] == 'question_name') {
                $parsed['questions'][$questions_count]['name'] = $elem['value'];
            }

            if ($elem['name'] == 'variant_id'){
                $variant_count++;
                $parsed['questions'][$questions_count]['variant'][$variant_count]['id'] = $elem['value'];
            }

            if ($elem['name'] == 'variant_name'){
                $parsed['questions'][$questions_count]['variant'][$variant_count]['name'] = $elem['value'];
            }

            if ($elem['name'] == 'is_true'){
                $parsed['questions'][$questions_count]['variant'][$variant_count]['is_true'] = $elem['value'];
            }

        }

        $quiz = Quiz::whereid($quiz_id)->update(['json' => json_encode($parsed, JSON_UNESCAPED_UNICODE), 'name' => $parsed['name']]);

        return 1;
    }


    public function create($course_id)
    {
        $quiz = new Quiz;
        $quiz->name = 'Новый тест';
        $quiz->json = '{"name":"Новый тест"}';
        $quiz->comment = '';
        $quiz->user_id = Auth::id();
        $quiz->attemps = 0;
        $quiz->difficulty = 0;
        $quiz->max_questions = 0;
        $quiz->randomize = 0;
        $quiz->timer = 0;
        $quiz->save();


        if ($quiz->user_id != Auth::id()) return  redirect('/id'.Auth::id());

        $view = view('inside.courses.quiz.quiz_editor')->with([
            'quiz_id' => $quiz->id,
            'quiz' => json_decode($quiz->json,false,JSON_UNESCAPED_UNICODE),
            'course_id' => $course_id
        ]);

        if (request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
                'id' => $quiz->id
            ]);
        }

        return $view;
    }

    public function refresh_quiz(Request $request){
        $material_id = $request->material_id;
        UserAnswer::where('user_id', Auth::id())
            ->where('material_id', $material_id)
            ->delete();
    }


    public function save_user_answer($material_id, $question_id, $variant_id)
    {
        $answer = new UserAnswer;
        $answer->user_id = Auth::id();
        $answer->material_id = $material_id;
        $answer->question_id = $question_id;
        $answer->variant_id = $variant_id;
        $answer->save();

        return 1;
    }

    public function my_quiz_points($course_id, $material_id)
    {


    }

}
