<?php

namespace App\Http\Controllers;


use App\Materials;
use App\Student;
use App\Vector;
use Illuminate\Http\Request;
use App\Course;
use App\Lessons;
use App\Book;
use App\Difficult;
use App\Category;
use App\User;
use App\Photo;
use App\Quiz;
use App\Tasks;
use Auth;
use Image;
use PhpParser\PrettyPrinter\Standard;

class ConstructorController extends Controller
{
    public function index($course_id)
    {
        $course = Course::where('id', $course_id)->first();

        $quiz = Quiz::where('user_id',Auth::id())->select('id', 'name')->get();

        $tasks = Tasks::where('user_id',Auth::id())->select('id', 'name')->get();

        $materials = Materials::where('course_id', $course_id)->orderBy('priority')->get();

        $books = Book::with('lessons')
            ->where('user_id', Auth::id())
            ->get();

        $view = view('inside.courses.constructor')->with([
            'course' => $course,
            'books' => $books,
            'materials' => $materials,
            'quizes'=> $quiz,
            'tasks'=> $tasks
        ]);


        if (request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title']
            ]);
        }

        return $view;
    }


    public function add_text($course_id, Request $request)
    {

        $user_id = Course::whereid($course_id)
            ->pluck('user_id')->first();

        if (Auth::id() == $user_id) {
            $material = new Materials;
            $material->name = $request->name;
            $material->type = 1;
            $material->priority = $request->priority;
            $material->course_id = $course_id;
            $material->json = $request->text;
            $material->save();

            $answer['id'] = $material->id;
            $answer['name'] = $request->name;
            $answer['content'] = $request->text;

            return $answer;
        } else return 0;

    }

    public function add_video($course_id, Request $request)
    {

        $user_id = Course::whereid($course_id)
            ->pluck('user_id')->first();

        if (Auth::id() == $user_id) {
            $material = new Materials;
            $material->name = $request->name;
            $material->type = 2;
            $material->priority = $request->priority;
            $material->course_id = $course_id;
            $material->json = $request->text;
            $material->save();

            $answer['id'] = $material->id;
            $answer['name'] = $request->name;
            $answer['content'] = $request->text;

            return $answer;
        } else return 0;

    }


    public function add_lesson($course_id, $lesson_id)
    {

        $user_id = Course::whereid($course_id)
            ->pluck('user_id')->first();

        $lesson = Lessons::whereid($lesson_id)->first();

        if (Auth::id() == $user_id) {
            $material = new Materials;
            $material->name = $lesson['name'];
            $material->type = 3;
            $material->priority = 1;
            $material->course_id = $course_id;
            $material->json = $lesson['text'];
            $material->save();

            $answer['id'] = $material->id;
            $answer['name'] = $lesson['name'];
            $answer['content'] = $lesson['text'];

            return $answer;
        } else return 0;

    }


    public function add_quiz($course_id, $quiz_id)
    {

        $user_id = Course::whereid($course_id)
            ->pluck('user_id')->first();

        $quiz = Quiz::whereid($quiz_id)->where('user_id', $user_id)->first();

        if (Auth::id() == $user_id) {
            $material = new Materials;
            $material->name = $quiz['name'];
            $material->type = 4;
            $material->priority = 1;
            $material->course_id = $course_id;
            $material->json = $quiz['json'];
            $material->save();

            $answer['id'] = $material->id;
            $answer['name'] = $quiz['name'];
            $answer['content'] = $quiz['json'];

            return $answer;
        } else return 0;

    }

    public function add_task($course_id, $task_id)
    {

        $user_id = Course::whereid($course_id)
            ->pluck('user_id')->first();

        $task = Tasks::whereid($task_id)->where('user_id', $user_id)->first();

        if (Auth::id() == $user_id) {
            $material = new Materials;
            $material->name = $task['name'];
            $material->type = 5;
            $material->priority = 1;
            $material->course_id = $course_id;
            $material->json = $task['json'];
            $material->save();

            $answer['id'] = $material->id;
            $answer['name'] = $task['name'];
            $answer['content'] = $task['json'];

            return $answer;
        } else return 0;

    }

    public function get_material($material_id)
    {
            $material = Materials::whereid($material_id)->first();
            return $material;
    }


    public function delete_material($course_id, $material_id)
    {
        $user_id = Course::whereid($course_id)
            ->pluck('user_id')->first();

        if (Auth::id() == $user_id) {

            Materials::whereid($material_id)->first()->delete();

            return 1;
        } else return 0;

    }

    public function rename_material(Request $request)
    {
        $user_id = Course::whereid($request->course_id)
            ->pluck('user_id')->first();

        if (Auth::id() == $user_id) {

            Materials::whereid($request->material_id)
                ->update(['name' => $request->new_name]);

            return 1;
        } else return 0;

    }

    public function config_course($course_id)
    {
        $course = Course::with(['user'])
            ->where('id', $course_id)
            ->first();

        $category = Category::get();
        $vector = Vector::where('category_id', $course->category_id)->get();
        $difficult = Difficult::all();
        if($course->user_id == Auth::id()) {

            $view = view('inside.courses.config')->with([
                'course' => $course,
                'categories'=>$category,
                'vectors'=>$vector,
                'difficult' => $difficult
            ]);

            if (request()->ajax()) {
                $sections = $view->renderSections();
                return response()->json([
                    'content' => $sections['content'],
                    'modal' => $sections['modal'],
                    'title' => $sections['title'],
                ]);
            }
            return $view;
        }
        return redirect('/id'.Auth::id());
    }

    public function get_vector(Request $request){

        $vector = Vector::where('category_id', $request->category_id)->get();
        return $vector;
    }

    public function apply_config_course(Request $request){

        $course_id = $request->course_id;
        $course = Course::whereid($course_id)->first();
        if($course->user_id == Auth::id()) {
            $course->name = $request->name;
            $course->price = $request->price;
            if (isset($request->category)) {
                $course->category_id = $request->category;
            }
            if (isset($request->vector)) {
                $course->vector_id = $request->vector;
            }
            if (isset($request->difficult)) {
                $course->difficult_id = $request->difficult;
            }
            if (isset($request->discription)) {
                $course->discription = $request->discription;
            }
            if (isset($request->full_description)) {
                $course->full_description = $request->full_description;
            }

            $course->save();

            $view = view('inside.courses.config_apply')->with([
                'course' => $course
            ]);

            if (request()->ajax()) {
                $sections = $view->renderSections();
                return response()->json([
                    'content' => $sections['content'],
                    'modal' => $sections['modal'],
                    'title' => $sections['title'],
                ]);
            }
            return $view;
        }
        return redirect('/id'.Auth::id());


    }

    public function delete_course($course_id){
        $course = Course::where('id', $course_id)
            ->select('id', 'price', 'user_id')
            ->first();

        if ($course->user_id == Auth::id()) {
            $student = Student::where('course_id', $course->id)->count();
            if($course->price == 0) {
                $deleted = Student::where('course_id', $course->id)->delete();
                $course->delete();

            }
            if($course->price > 0 && $student == 0)
                $course->delete();

            return redirect('/id'.Auth::id());
        }
        return redirect('/id'.Auth::id());
    }

    public function save_priority(Request $request){

        foreach($request->material_id as $index => $material_id){
            $update_material = Materials::where('id',$material_id)->update(['priority' => $index]);
        }

        return 0;
    }

    public function upload_avatar(Request $request){
        // Handle the user upload of avatar
        $course = Course::where('id', $request->course_id)
            ->select('user_id')
            ->first();
        if ($course->user_id == $user_id = Auth::id()) {
            if ($request->hasFile('avatar')) {
                $course_id = $request->course_id;
                $avatar = $request->file('avatar');
                $filename = $course_id . '.' . $avatar->getClientOriginalExtension();
                $img = Image::make($avatar);
                if ($img->height() >= $img->width()) {
                    $img->resize(null, 190, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $img->resize(295, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save(storage_path() . '/app/public/course_avatars/' . $filename);
//            $img->save( public_path('/uploads/course_avatars/' . $filename ) );

                if ($img->height() >= $img->width()) {
                    $img->resize(null, 140, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $img->resize(140, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save(storage_path() . '/app/public/course_avatars/small/' . $filename);
//            $img->save( public_path('/uploads/course_avatars/small/' . $filename ) );

                $course = Course::whereid($course_id)->first();

                if ($avatar->getClientOriginalExtension() == "png") {
                    $course->photo_id = 1;
                }
                if ($avatar->getClientOriginalExtension() == "jpg") {
                    $course->photo_id = 2;
                }
                if ($avatar->getClientOriginalExtension() == "gif") {
                    $course->photo_id = 3;
                }

                $course->save();
            }

            return $filename;
        }
        return redirect('/id'.Auth::id());
    }
}
