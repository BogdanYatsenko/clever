<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 26.09.2018
 * Time: 14:12
 */

namespace App\Http\Controllers;

use App\Course_Teacher;
use App\User;
use App\Teacher;

use Illuminate\Support\Facades\Auth;
use App\OfflineCourse_Teacher;
use App\Meeting_Teacher;


class TeacherController extends Controller
{
    public function profile($id){

        $teacher = Teacher::with('offline')
            ->where('id_teacher', $id)
            ->get();

        //dd($teacher);
        return view('inside.teacher.profile');
    }


    public function becameteacher($id){
        $user = User::where('id', $id)
            ->select('first', 'second', 'third')
            ->first();

        return view('inside.teacher.became')->with([
            'user' => $user,
        ]);

    }

    public function createteacher(Request $request){
        $check_exist = Teacher::where('id_user', Auth::id())
            ->get();
        if(count($check_exist) == 1)
        {
            $check_exist = Teacher::where('id_user', Auth::id())
                ->update(
                [
                    'organization' => $request->organization,
                    'exp_desc' => $request->exp_desc,
                    'video_link' =>  $request->video_link,
                    'is_moderated' => 0
                ]
            );

            $view = view('inside.teacher.became_success')->with([
                'id' => Auth::id()
            ]);
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'title' => $sections['title'],
            ]);

        }
        else{
            $teacher = new Teacher();
            $teacher->id_user = Auth::id();
            $teacher->exp_desc = $request->exp_desc;
            $teacher->organization = $request->organization;
            $teacher->rating = 0;
            $teacher->video_link = $request->video_link;
            $teacher->is_moderated = 0;
            $teacher->save();

            $view = view('inside.teacher.became_success')->with([
                'id' => Auth::id()
            ]);
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'title' => $sections['title'],
            ]);
        }



    }

    public function accept_moderation($teacher_id){
        $teacher = Teacher::where('id_teacher', $teacher_id)
            ->update(['is_moderated' => 1]);
        return 1;
    }
    public function reject_moderation($teacher_id){
        $teacher = Teacher::where('id_teacher', $teacher_id)
            ->update(['is_moderated' => 2]);
        return 1;
    }

    public function online_accept($id_course){
        $teacher_id = Teacher::where('id_user', Auth::id())->first();
        Course_Teacher::where('id_course', $id_course)
            ->where('id_teacher', $teacher_id->id_teacher)
            ->update(['is_moderated_by_teacher' => 2]);
    }

    public function online_reject($id_course){
        $teacher_id = Teacher::where('id_user', Auth::id())->first();
        Course_Teacher::where('id_course', $id_course)
            ->where('id_teacher', $teacher_id->id_teacher)
            ->update(['is_moderated_by_teacher' => 1]);
    }

    public function offline_accept($id_course){
        $teacher_id = Teacher::where('id_user', Auth::id())->first();
        OfflineCourse_Teacher::where('id_offlinecourse', $id_course)
            ->where('id_teacher', $teacher_id->id_teacher)
            ->update(['is_moderated_by_teacher' => 2]);
    }
    public function offline_reject($id_course){
        $teacher_id = Teacher::where('id_user', Auth::id())->first();
        OfflineCourse_Teacher::where('id_offlinecourse', $id_course)
            ->where('id_teacher', $teacher_id->id_teacher)
            ->update(['is_moderated_by_teacher' => 1]);
    }

    public function meeting_accept($id_meeting){
        $teacher_id = Teacher::where('id_user', Auth::id())->first();
        Meeting_Teacher::where('id_meeting', $id_meeting)
            ->where('id_teacher', $teacher_id->id_teacher)
            ->update(['is_moderated_by_teacher' => 2]);
    }

    public function meeting_reject($id_meeting){
        $teacher_id = Teacher::where('id_user', Auth::id())->first();
        Meeting_Teacher::where('id_meeting', $id_meeting)
            ->where('id_teacher', $teacher_id->id_teacher)
            ->update(['is_moderated_by_teacher' => 1]);
    }

}