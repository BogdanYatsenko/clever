<?php

namespace App\Http\Controllers;

use App\XhrCourse;
use Illuminate\Http\Request;
use App\Course;
use App\Student;
use Auth;

class ShopController extends Controller
{
    public function manage(){
        $course = Course::with(['students', 'students.user'])->where('user_id',Auth::id())->wherein('is_moderated',[1,2])->select('id','name','price','is_moderated', 'material_link')->get();
        //$xhr_student = Student::where('xhr', 1)->where('pay',1)->get();
        //\DebugBar::info($course->students);

        $view = view('inside.shop.manage')->with([
            'courses'=> $course
        ]);

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
            ]);
        }
        return $view;

    }

    public function send_xhr(Request $request){
        $user_id =  $request->user_id;
        $course_id =  $request->course_id;
        $login =  $request->login;
        $password =  $request->password;
        $comment =  $request->comment;

        $course_admin = Course::where('id',$course_id)->select('user_id')->first();

        if($course_admin->user_id == Auth::id()){
            $xhr = XhrCourse::where('user_id',$user_id)->where('course_id',$course_id)->first();
            $student = Student::where('user_id',$user_id)->where('course_id',$course_id)->first();

            if(is_null($xhr)) {
                $xhr = new XhrCourse;
                $xhr->course_id = $course_id;
                $xhr->user_id = $user_id;
                $xhr->login = $login;
                $xhr->password = $password;
                $xhr->comment = $comment;
                $xhr->save();
                $student->xhr_check = 3;
                $student->save();
                return $xhr->id;
            } else if($xhr->is_check == 2) {
                $xhr->course_id = $course_id;
                $xhr->user_id = $user_id;
                $xhr->login = $login;
                $xhr->password = $password;
                $xhr->comment = $comment;
                $xhr->is_check = 0;
                $xhr->save();
                $student->xhr_check = 3;
                $student->save();
                return $xhr->id;
            }
            return 0;
        }
        return 0;
    }

    public function sell($book_id){
        $book = Book::with(['user'])
            ->where('id', $book_id)
            ->first();

        $view = view('inside.shop.sell')->with([
            'course' => $book
        ]);

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'title' => $sections['title'],
            ]);
        }
        return $view;
    }
}
