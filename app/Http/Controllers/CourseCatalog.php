<?php

namespace App\Http\Controllers;

use App\Meeting;
use App\Meeting_types;
use App\OfflineCourse;
use App\Search_categories;
use Illuminate\Http\Request;
use App\Course;
use App\OtherCourses;
use Illuminate\Database\Eloquent\Collection;

class CourseCatalog extends Controller
{

    public function index()
    {

        $unique_sources = OtherCourses::select('source')->distinct()->get();

        //dd($unique_sources);

        $max_price_courses = Course::where('is_moderated', 1)->max('price');
        $min_price_courses = Course::where('is_moderated', 1)->min('price');

        $max_price_othercourses = OtherCourses::max('price');
        $min_price_othercourses = OtherCourses::min('price');

        $max_price_offline = OfflineCourse::where('is_moderated', 2)->max('price');
        $min_price_offline = OfflineCourse::where('is_moderated', 2)->min('price');

        $max_price_meetings = Meeting::where('is_moderated', 2)->max('price');
        $min_price_meetings = Meeting::where('is_moderated', 2)->min('price');



        $max_price = max($max_price_courses, $max_price_othercourses, $max_price_offline,$max_price_meetings);
        $min_price = min($min_price_courses, $min_price_othercourses, $min_price_offline,$min_price_meetings);
//        if($max_price_courses >= $max_price_othercourses)
//            $max_price = $max_price_courses;
//        else
//            $max_price = $max_price_othercourses;
//
//        if($min_price_courses >= $min_price_othercourses)
//            $min_price = $min_price_othercourses;
//        else
//            $min_price = $min_price_courses;


        $courses = Course::with(['user', 'photo'])
            ->where('is_moderated', '=', 1)
            ->orderBy('price')
            ->take(5)
            //->get()
            ->select('id', 'photo_id', 'name', 'description', 'user_id', 'price')->get();



        $coursesother = OtherCourses::with('photo')
            ->take(5)
//            ->where('price', '>', 0)
            ->orderBy('price')
            ->select('id', 'name', 'description', 'price', 'link', 'source', 'photo_id')->get();


        $offline_courses = OfflineCourse::with('photo', 'source')
            ->where('is_moderated', 2)
            ->take(5)
            ->orderBy('price')
            ->select('id', 'name', 'description', 'price', 'source_id')
            ->get();

        $meetings = Meeting::with('photo', 'mit_type', 'source')
            ->where('is_moderated', 2)
            ->take(5)
            ->orderBy('price')
            ->select('id_meeting', 'name', 'description', 'price', 'source_id', 'id_type')
            ->get();

        $objects = $courses ->merge($coursesother)->toBase()->merge($offline_courses)->toBase()->merge($meetings)->toBase();
        $sorted_objects = $objects->sortBy('price');


        $courses_count_othercourses = OtherCourses::count('id');
        $courses_count = Course::where('is_moderated', 1)->count('id');

        $courses_var_count = $courses_count;
        $courses_count = $courses_count + $courses_count_othercourses;

        $objects_count = $courses_count+ OfflineCourse::where('is_moderated', 2)->count('id') + Meeting::where('is_moderated', 2)->count('id_meeting');

        $courses_var_count = $courses_count_othercourses + $courses_var_count;

        $view = view('inside.courses.catalog_courses', [
            'price_from' => 0,
            'price_to' => 'max',
            'courses'=>$sorted_objects,
            'min_price' => $min_price,
            'max_price' => $max_price,
            'objects_count' => $objects_count,
            'page' => 1,
            'courses_var_count' => $courses_var_count,
            'order_by' => 0,
            'sources' => $unique_sources,
            'selected_source' => 0,
            'objects' => 0,

        ]);

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
                'materials' => $sections['content']
            ]);
        }

        return $view;
    }

    public function search($object_type, $source, $searchword, $from, $to, $order_by, $page){

        $unique_sources = OtherCourses::select('source')->distinct()->get();
        $query_our = Course::with(['user', 'photo'])
            ->where('price', '>=', $from)
            ->where('is_moderated', '=', 1);

        $query_other = OtherCourses::with('categories')
            ->where('price', '>=', $from);

        $query_offline = OfflineCourse::with('photo', 'source')
            ->where('price', '>=', $from)
            ->where('is_moderated', '=', 2);

        $query_meeting = Meeting::with('photo', 'source', 'mit_type')
            ->where('price', '>=', $from)
            ->where('is_moderated', '=', 2);

        if ($searchword != '0'){
            $query_our = $query_our->where('name', 'like', '%'.$searchword.'%');
            $query_other = $query_other->where('name', 'like', '%'.$searchword.'%');
            $query_offline = $query_offline->where('name', 'like', '%'.$searchword.'%');
            $query_meeting = $query_meeting->where('name', 'like', '%'.$searchword.'%');
        }

        if ($to != 'max'){
            $query_our = $query_our->where('price', '<=', $to);
            $query_other = $query_other->where('price', '<=', $to);
            $query_offline = $query_offline->where('price', '<=', $to);
            $query_meeting = $query_meeting->where('price', '<=', $to);
        }

        if ($order_by == 0){
            $query_our = $query_our->orderBy('price');
            $query_other = $query_other->orderBy('price');
            $query_offline = $query_offline->orderBy('price');
            $query_meeting = $query_meeting->orderBy('price');
        }

        if ($order_by == 1){
            $query_our = $query_our->orderBy('price', 'desc');
            $query_other = $query_other->orderBy('price', 'desc');
            $query_offline = $query_offline->orderBy('price', 'desc');
            $query_meeting = $query_meeting->orderBy('price', 'desc');
        }

        if ($order_by == 2){
            $query_our = $query_our->orderBy('created_at');
            $query_other = $query_other->orderBy('created_at');
            $query_offline = $query_offline->orderBy('created_at');
            $query_meeting = $query_meeting->orderBy('created_at');
        }

        if ($order_by == 3){
            $query_our = $query_our->orderBy('created_at', 'desc');
            $query_other = $query_other->orderBy('created_at', 'desc');
            $query_offline = $query_offline->orderBy('created_at', 'desc');
            $query_meeting = $query_meeting->orderBy('created_at', 'desc');
        }

        if ($source != 0) {
                if ($source == 1){
                    $unique_sources = OtherCourses::select('source')->distinct()->get();
                    $query_buf_our = $query_our;
                    $query_buf_offline = $query_offline;
                    $query_buf_meeting = $query_meeting;


                    $coures_var_count_our = $query_buf_our->count();
                    $offline_var_count = $query_buf_offline->count();
                    $meeting_var_count = $query_buf_meeting->count();

                    $objects_var_count = $coures_var_count_our + $offline_var_count + $meeting_var_count;
                    //dd($objects_var_count);

                    $query_our = $query_our->take(5*$page);
                    $query_offline = $query_offline->take(5*$page);
                    $query_meeting = $query_meeting->take(5*$page);

                    $courses_our = $query_our->select('id', 'photo_id', 'name', 'description', 'user_id', 'price')->get();
                    $offline = $query_offline->select('id', 'photo_id', 'name', 'description', 'id_user_creator', 'price')->get();
                    $meeting = $query_meeting->select('id_meeting', 'photo_id', 'name', 'description', 'id_user_creator', 'price', 'id_type')->get();

                    $objects = $courses_our->toBase()->merge($offline)->toBase()->merge($meeting)->toBase();

                    if ($order_by == 0) {
                        $objects = $objects->sortBy('price');
                    }
                    if ($order_by == 1) {
                        $objects = $objects->sortByDesc('price');
                    }


                    $max_price_courses = Course::where('is_moderated', 1)->max('price');
                    $min_price_courses = Course::where('is_moderated', 1)->min('price');

                    $max_price_offline = OfflineCourse::where('is_moderated', 2)->max('price');
                    $min_price_offline = OfflineCourse::where('is_moderated', 2)->min('price');

                    $max_price_meetings = Meeting::where('is_moderated', 2)->max('price');
                    $min_price_meetings = Meeting::where('is_moderated', 2)->min('price');

                    $max_price = max($max_price_courses, $max_price_offline,$max_price_meetings);
                    $min_price = min($min_price_courses, $min_price_offline,$min_price_meetings);

                    $courses_count_our = Course::where('is_moderated', '=', 1)->count('id');
                    $offline_count = OfflineCourse::where('is_moderated', '=', 2)->count('id');
                    $meeting_count = Meeting::where('is_moderated', '=', 2)->count('id_meeting');

                    $objects_count = $courses_count_our + $offline_count + $meeting_count;
                    //dd($objects_count);



                    $content = view('inside.courses.catalog_courses', [
                        'courses' => $objects,
                        'searchword' => $searchword,
                        'price_from' => $from,
                        'price_to' => $to,
                        'max_price' => $max_price,
                        'min_price' => $min_price,
                        'order_by' => $order_by,
                        'objects_count' => $objects_count,
                        'page' => $page,
                        'courses_var_count' => $objects_var_count,
                        'sources' => $unique_sources,
                        'selected_source' => 1,
                        'objects' => $object_type,
                    ]);

                    if(request()->ajax()) {

                        $sections = $content->renderSections();
                        return response()->json([
                            'content' => $sections['content'],
                            'content_courses' => $sections['content_courses'],
                            'modal' => $sections['modal'],
                            'title' => $sections['title'],
                        ]);

                        //$content = $content->renderSections(); // returns an associative array of 'content', 'head' and 'footer'

                    }
                    return $content;
                }
                else{
                    $query_buf_other = $query_other;
                    $query_buf_offline = $query_offline->where('source_id', '=', $source-1);
                    $query_buf_meeting = $query_meeting->where('source_id', '=', $source-1);


                    $coures_var_count_other = $query_buf_other->count();
                    $offline_var_count = $query_buf_offline->count();
                    $meeting_var_count = $query_buf_meeting->count();

                    $objects_var_count = $coures_var_count_other + $offline_var_count + $meeting_var_count;

                    $query_other = $query_other->take(5*$page);
                    $query_offline = $query_offline->take(5*$page);
                    $query_meeting = $query_meeting->take(5*$page);

                    $courses_other = $query_other->select('id', 'name', 'description','price', 'source', 'photo_id')->get();
                    $offline = $query_offline->select('id', 'name', 'description','price', 'source_id', 'photo_id')->get();
                    $meeting = $query_meeting->select('id_meeting', 'name', 'description','price', 'source_id', 'photo_id', 'id_type')->get();

                    $objects = $courses_other->toBase()->merge($offline)->toBase()->merge($meeting)->toBase();


                    $max_price_othercourses = OtherCourses::max('price');
                    $min_price_othercourses = OtherCourses::min('price');

                    $max_price_offline = OfflineCourse::where('is_moderated', 2)->where('source_id', $source-1)->max('price');
                    $min_price_offline = OfflineCourse::where('is_moderated', 2)->where('source_id', $source-1)->min('price');

                    $max_price_meetings = Meeting::where('is_moderated', 2)->where('source_id', $source-1)->max('price');
                    $min_price_meetings = Meeting::where('is_moderated', 2)->where('source_id', $source-1)->min('price');

                    $max_price = max($max_price_othercourses, $max_price_offline,$max_price_meetings);
                    $min_price = min($min_price_othercourses, $min_price_offline,$min_price_meetings);

                    $content = view('inside.courses.catalog_courses', [
                        'courses'=>$objects,
                        'searchword' => $searchword,
                        'price_from' => $from,
                        'price_to' => $to,
                        'max_price' => $max_price,
                        'min_price' => $min_price,
                        'order_by' => $order_by,
                        'objects_count' => $objects_var_count,
                        'page' => $page,
                        'courses_var_count' => $objects_var_count,
                        'sources' => $unique_sources,
                        'selected_source' => $source,
                        'objects' => $object_type
                    ]);

                    if(request()->ajax()) {

                        $sections = $content->renderSections();
                        return response()->json([
                            'content' => $sections['content'],
                            'content_courses' => $sections['content_courses'],
                            'modal' => $sections['modal'],
                            'title' => $sections['title'],
                        ]);

                        //$content = $content->renderSections(); // returns an associative array of 'content', 'head' and 'footer'

                    }
                    return $content;
                }
        }
        else{
            $unique_sources = OtherCourses::select('source')->distinct()->get();

            $query_buf_our = $query_our;
            $query_buf_other = $query_other;
            $query_buf_offline = $query_offline;
            $query_buf_meeting = $query_meeting;

            $coures_var_count_our = $query_buf_our->count();
            $coures_var_count_other = $query_buf_other->count();
            $offline_var_count = $query_buf_offline->count();
            $meeting_var_count = $query_buf_meeting->count();




            $query_our = $query_our->take(5*$page);
            $query_other = $query_other->take(4*$page);
            $query_offline = $query_offline->take(3*$page);
            $query_meeting = $query_meeting->take(3*$page);

            $courses_our = $query_our->select('id', 'photo_id', 'name', 'description', 'user_id', 'price')->get();
            $courses_other = $query_other->select('id', 'name', 'description','price', 'source', 'photo_id')->get();
            $offline = $query_offline->with('source')->select('id', 'name', 'description','price', 'source_id', 'photo_id')->get();
            $meeting = $query_meeting->with('source', 'mit_type')->select('id_meeting', 'name', 'description','price', 'source_id', 'photo_id', 'id_type')->get();


            $max_price_courses = Course::where('is_moderated', 1)->max('price');
            $min_price_courses = Course::where('is_moderated', 1)->min('price');

            $max_price_othercourses = OtherCourses::max('price');
            $min_price_othercourses = OtherCourses::min('price');

            $max_price_offline = OfflineCourse::where('is_moderated', 2)->max('price');
            $min_price_offline = OfflineCourse::where('is_moderated', 2)->min('price');

            $max_price_meetings = Meeting::where('is_moderated', 2)->max('price');
            $min_price_meetings = Meeting::where('is_moderated', 2)->min('price');

            $max_price = max($max_price_courses, $max_price_othercourses, $max_price_offline,$max_price_meetings);
            $min_price = min($min_price_courses, $min_price_othercourses, $min_price_offline,$min_price_meetings);


            $courses_count_our = Course::where('is_moderated', '=', 1)->count('id');
            $courses_count_other = OtherCourses::count('id');
            $offline_count = OfflineCourse::where('is_moderated', 2)->count('id');
            $meeting_count = Meeting::where('is_moderated', 2)->count('id_meeting');




            if($object_type == 0){
                $objects = $courses_our->toBase()->merge($courses_other)->toBase()->merge($offline)->toBase()->merge($meeting)->toBase();
                $objects_count = $courses_count_our + $courses_count_other + $offline_count + $meeting_count;
                $objects_var_count = $coures_var_count_our + $coures_var_count_other + $offline_var_count + $meeting_var_count;
            }

            if($object_type == 1){
                $objects = $courses_our->toBase()->merge($courses_other)->toBase();
                $objects_count = $courses_count_our + $courses_count_other;
                $objects_var_count = $coures_var_count_our + $coures_var_count_other;
            }
            if($object_type == 2){
                $objects = $offline;
                $objects_count = $offline_count;
                $objects_var_count = $offline_var_count;
            }
            if($object_type == 3){
                $objects = $meeting;
                $objects_count = $meeting_count;
                $objects_var_count = $meeting_var_count;
            }

            //dd($objects);
            if ($order_by == 0) $objects_sorted = $objects->sortBy('price');
            if ($order_by == 1) $objects_sorted = $objects->sortByDesc('price');



            $content = view('inside.courses.catalog_courses', [
                'courses'=>$objects_sorted,
                'searchword' => $searchword,
                'price_from' => $from,
                'price_to' => $to,
                'max_price' => $max_price,
                'min_price' => $min_price,
                'order_by' => $order_by,
                'objects_count' => $objects_count,
                'page' => $page,
                'courses_var_count' => $objects_var_count,
                'sources' => $unique_sources,
                'selected_source' => 0,
                'objects' => $object_type,
            ]);

            if(request()->ajax()) {

                $sections = $content->renderSections();
                return response()->json([
                    'content' => $sections['content'],
                    'content_courses' => $sections['content_courses'],
                    'modal' => $sections['modal'],
                    'title' => $sections['title'],
                ]);

            }
            return $content;
        }




    }
}
