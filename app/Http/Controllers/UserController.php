<?php

namespace App\Http\Controllers;

use App\Course;
use App\Course_Teacher;
use App\Meeting;
use App\Meeting_Teacher;
use App\OfflineCourse_Teacher;
use App\Students_meetings;
use App\Students_offline;
use App\Subscribe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Photo;
use App\Country;
use App\Region;
use App\City;
use App\Student;
use App\SmsSender;
use Auth;
use Image;
use Carbon\Carbon;
use App\Teacher;
use App\OfflineCourse;
use App\CompanyAdmin;

class UserController extends Controller
{
    public function profile($user_id)
    {
        $user = User::select('country_id','region_id','city_id')->find($user_id);
        if (!is_null($user->city_id))
            $user = User::with(['city'])->find($user_id);
        else if (!is_null($user->region_id))
            $user = User::with(['region'])->find($user_id);
        else if (!is_null($user->country_id))
            $user = User::with(['country'])->find($user_id);
        else
            $user = User::find($user_id);

        $is_teacher = Teacher::with('user')
            ->where('id_user', $user_id)
            ->get();

        //dd($is_teacher);
        if(count($is_teacher) > 0){

            $offline_teach = OfflineCourse_Teacher::with('offline')
                ->where('id_teacher', $is_teacher[0]->id_teacher)
                ->where('is_moderated_by_teacher', 2)
                ->get();
            //dd($offline_teach);

            $online_teach = Course_Teacher::with('course', 'teacher')
                ->where('id_teacher', $is_teacher[0]->id_teacher)
                ->where('is_moderated_by_teacher', 2)
                ->get();
            //dd($online_teach);

            $meetings_teach = Meeting_Teacher::with('meeting')
                ->where('id_teacher', $is_teacher[0]->id_teacher)
                ->where('is_moderated_by_teacher', 2)
                ->get();
            //dd($meetings_teach);
        }
        else{
            $offline_teach = null;
            $online_teach = null;
            $meetings_teach = null;
        }

        $offline_creator = OfflineCourse::where('id_user_creator', $user_id)->get();
        $online_creator = Course::where('user_id', $user_id)->get();
        $meeting_creator = Meeting::where('id_user_Creator', $user_id)->get();
        //dd($online_creator);

        $company_admin = CompanyAdmin::where('user_id', $user_id)
            ->get();

        $company_admin_offline = OfflineCourse::with('teacher')->where('id_user_creator', $user_id)->get();
        $company_admin_online = Course::with('teacher')->whereuser_id($user_id)->get();
        $company_admin_meeting = Meeting::with('teacher')->where('id_user_creator', $user_id)->get();

        //dd($company_admin);

        $courses = Course::whereuser_id($user_id)->get();
        $students = Student::with(['course'])->whereuser_id($user_id)->get();
        $students_offline = Students_offline::with('offline_course')->where('user_id', $user_id)->get();
        $students_meetings = Students_meetings::with('meetings')->where('user_id', $user_id)->get();
        $subscribe_to = User::inRandomOrder()->limit(6)->find($user->subscribe_to);
        $subscribe = User::inRandomOrder()->limit(6)->find($user->subscribe);
        $photo = Photo::get();

        //dd($students_offline);

        $notification_from_offline = DB::select('select offlinecourses.id as o_id, offlinecourses.name, offlinecourses.start_date, users.first, users.second, users.id as u_id
                                from offlinecourses 
                                JOIN offlinecourses_teachers on offlinecourses.id = offlinecourses_teachers.id_offlinecourse
                                JOIN users on offlinecourses.id_user_creator = users.id
                                where offlinecourses_teachers.is_moderated_by_teacher = 0 
                                and offlinecourses_teachers.id_teacher = (select id_teacher from teachers where id_user = ?) ', [$user_id]);

        $notification_from_online = DB::select('select courses.id as c_id, courses.name, users.first, users.second, users.id as u_id
                                from courses
                                JOIN courses_teachers on courses.id = courses_teachers.id_course
                                JOIN users on courses.user_id = users.id
                                where courses_teachers.is_moderated_by_teacher = 0 
                                and courses_teachers.id_teacher = (select id_teacher from teachers where id_user = ? and teachers.id_user = ?) 
                                ', [$user_id, $user_id]);

        $notification_from_meetings = DB::select('select meetings.id_meeting as m_id, meetings.name, meetings.start_date, users.first, users.second, users.id as u_id, 
                                meetings_types.name as t_name from meetings 
                                JOIN meetings_teachers on meetings.id_meeting = meetings_teachers.id_meeting
                                JOIN users on meetings.id_user_creator = users.id
                                JOIN meetings_types on meetings.id_type = meetings_types.id
                                where meetings_teachers.is_moderated_by_teacher = 0 
                                and meetings_teachers.id_teacher = (select id_teacher from teachers where id_user = ?) ', [$user_id]);



        //dd($offline_teach);

        $count_notification = count($notification_from_offline) + count($notification_from_online) + count($notification_from_meetings);
        //dd($count_notification);
        $view = view('inside.user.userpage')->with([
            'user' => $user,
            'courses' => $courses,
            'photos' => $photo,
            'subscribe_tos' => $subscribe_to,
            'subscribes' => $subscribe,
            'students' =>$students,
            'students_offline' => $students_offline,
            'students_meetings' => $students_meetings,
            'is_teacher' => $is_teacher,
            'offline_courses' => $offline_teach,
            'online_teach' => $online_teach,
            'meeting_teach' => $meetings_teach,
            'company_admin' => $company_admin,
            'company_admin_offline'=>$company_admin_offline,
            'company_admin_online'=>$company_admin_online,
            'company_admin_meeting'=>$company_admin_meeting,
            'count_notification'=>$count_notification,
            'notification_from_offline'=>$notification_from_offline,
            'notification_from_online'=>$notification_from_online,
            'notification_from_meetings'=>$notification_from_meetings,
            'offline_creator' => $offline_creator,
            'online_creator' => $online_creator,
            'meeting_creator' => $meeting_creator,
        ]);

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
            ]);
        }

        return $view;
    }

    public function profile_name($user_name)
    {
        $user = User::wherelogin($user_name)->first();

        $view = view('inside.user.userpage')->with([
            'user' => $user,
        ]);

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
            ]);
        }

        return $view;
    }

    public function setting($user_id)
    {
        if($user_id == Auth::id()) {
            $user = User::whereid($user_id)->first();
            $country = Country::select('id','name')->get();
            $region = Region::where('country_id', $user->country_id)->select('id','name')->get();
            $city = City::where('region_id', $user->region_id)->select('id','name')->get();

            $view = view('inside.user.setting')->with([
                'user' => $user,
                'user_id' => $user_id,
                'countries' => $country,
                'regions' =>$region,
                'cities'=>$city

            ]);

            if (request()->ajax()) {
                $sections = $view->renderSections();
                return response()->json([
                    'content' => $sections['content'],
                    'modal' => $sections['modal'],
                    'title' => $sections['title'],
                ]);
            }

            return $view;
        }
        return redirect('/id'.Auth::id());
    }

    public function save_setting(Request $request){

        if($request->user_id == Auth::id()) {
            $user = User::find(Auth::id());

            $user->third = $request->third;
            $user->sex = $request->sex;
            $user->birthday = $request->birthday;
            $user->country_id = $request->country;
            $user->region_id = $request->region;
            $user->city_id = $request->city;
            $user->work_place_id = $request->work;
            $user->job_id = $request->job;
            $user->phone = $request->phone;
            $user->skype = $request->skype;
            $user->vk = $request->vk;
            $user->fb = $request->fb;
            $user->site = $request->site;

            $user->save();

            return $user->id;
        }
        return redirect('/id'.Auth::id());
    }

    public function get_region(Request $request){

        $region = Region::where('country_id', $request->country_id)->select('id','name')->get();
        return $region;
    }

    public function get_city(Request $request){

        $city = City::where('region_id', $request->region_id)->select('id','name')->get();
        return $city;
    }

    public function follow(Request $request){

        if (Auth::id() != $request->user_id) {
            $subscribe = new Subscribe();
            $subscribe->user_id = Auth::id();
            $subscribe->sub_id = $request->user_id;
            $subscribe->save();
            return $subscribe->id;
        }
        return redirect('/id'.Auth::id());
    }

    public function unfollow(Request $request){

        if (Auth::id() != $request->user_id) {
            $subscribe = Subscribe::where('user_id', Auth::id())->where('sub_id', $request->user_id)->delete();

            return $subscribe;
        }
        return redirect('/id'.Auth::id());
    }

    public function update_avatar(Request $request){

        // Handle the user upload of avatar
        \DebugBar::info($request->file('avatar'));
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');

            $filename = Auth::id() . '.' . $avatar->getClientOriginalExtension();
            $img= Image::make($avatar);
            if($img->height() >= $img->width()){
                $img->resize(null, 180, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }else{
                $img->resize(230, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            $img->save( storage_path().'/app/public/avatars/' . $filename);
            //$img->save( public_path('/uploads/avatars/' . $filename ) );

            if($img->height() >= $img->width()){
                $img->resize(null, 52, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }else{
                $img->resize(52, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save( storage_path().'/app/public/avatars/small/' . $filename  );

            if($img->height() >= $img->width()){
                $img->resize(null, 42, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }else{
                $img->resize(42, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save( storage_path().'/app/public/avatars/icon/' . $filename );
            //$img->save( public_path('/uploads/avatars/icon/' . $filename ) );

            $user = Auth::user();

            if($avatar->getClientOriginalExtension() == "png"){
                $user->photo_id = 1;
            }
            if($avatar->getClientOriginalExtension() == "jpg"){
                $user->photo_id = 2;
            }
            if($avatar->getClientOriginalExtension() == "gif"){
                $user->photo_id = 3;
            }

            $user->save();
        }

        return $filename;

    }

    public function randomPassword() {
        $alphabet = "0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 4; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function send_sms($email, $password){
        $passPhar= [
            0 => "Horoshego dnya",
            1 => "S uvazheniem",
            2 => "Vsego nailuchego",
            3 => "Vash Clever",
        ];
        $passInt =  rand ( 0, 3);

        $newSms = new SmsSender();
        $newSms->recipient = '+7' . $email;
        $newSms->message = 'Kod podtverzdeniya Clever-e.com: '. $password . " ". $passPhar[$passInt];
        $newSms->save();
    }

    public function getconfirm(Request $request){
        $email = $request->get('email');
        if($email == null || $email == ""){
            return response()->json(['errors' => 'Invalid phone']);
        }

        if(substr($email , 0, 1) == '+' && strlen ($email) == 12)
            $email = substr($email, 1, 12);

        if (is_numeric($email)) {
            if (substr($email, 0, 1) == 7 && strlen($email) == 11)
                $email = substr($email, 1, 11);
            if (substr($email, 0, 1) == 8 && strlen($email) == 11)
                $email = substr($email, 1, 11);

            $hasUser = User::where('id', Auth::id())->first();
            \DebugBar::info($email);
            if ($hasUser != null) {
                $now = Carbon::now();
                $update =  Carbon::parse($hasUser->updated_at);
                if ($now > $update->addSeconds(60)) {
                    $password = $this->randomPassword();
                    $hasUser->phone = $email;
                    $hasUser->phone_confirm = 0;
                    $hasUser->phone_confirm_code = $password;
                    $hasUser->save();
                    $this->send_sms($email, $password);
                }
                return response()->json(['success' => 'success']);
            } else {
                return 0;
            }
        }
    }
    public function applyconfirm(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        if ($email == null || $email == "") {
            return response()->json(['errors' => 'Invalid phone']);
        }

        if (substr($email, 0, 1) == '+' && strlen($email) == 12)
            $email = substr($email, 1, 12);

        if (is_numeric($email)) {
            if (substr($email, 0, 1) == 7 && strlen($email) == 11)
                $email = substr($email, 1, 11);
            if (substr($email, 0, 1) == 8 && strlen($email) == 11)
                $email = substr($email, 1, 11);

            $hasUser = User::where('id', Auth::id())->first();

            if ($hasUser != null && $password == $hasUser->phone_confirm_code && $email == $hasUser->phone) {
                $password = $this->randomPassword();
                $hasUser->phone_confirm = 1;
                $hasUser->phone_confirm_code = $password;
                $hasUser->save();
                return response()->json(['success' => 'success']);
            } else {
                return 0;
            }
        }
    }



}
