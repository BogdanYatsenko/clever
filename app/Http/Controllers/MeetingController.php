<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.10.2018
 * Time: 11:41
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Meeting;
use Auth;
use App\Teacher;
use App\CompanyAdmin;
use App\Meeting_Teacher;
use App\Students_meetings;

class MeetingController
{
    public function create(){
        $teachers = Teacher::with('user')->get();

        $company_admin = CompanyAdmin::with('user')
            ->where('user_id', Auth::id())
            ->get();

        return view('inside.meeting.create')->with([
            'teachers' => $teachers,
            'company_admin' => $company_admin,
        ]);
    }

    public function add_meeting(Request $request){
        $meeting = new Meeting();

        //dd($request->type);
        $meeting->name = $request->name;
        $meeting->id_type = $request->type;
        $meeting->meeting_length_hours = $request->meeting_length_hours;
        $meeting->start_date = $request->start_date;
        $meeting->address = $request->address;
        $meeting->max_number_students = $request->max_number_students;
        $meeting->goal = $request->goal;
        $meeting->price = $request->price;
        $meeting->audience = $request->audience;
        $meeting->difficult_id = $request->difficulty;
        $meeting->description = $request->description;
        $meeting->full_description = $request->full_description;
        $meeting->skills = $request->skills;

        $meeting->is_moderated = 0;
        $meeting->id_user_creator = Auth::id();
        if($request->type == 1)
        {
            $meeting->source = $request->source;
        }
        $meeting->save();

        for($i=0; $i<count($request->teacher_ids); $i++)
        {
            $connection = new Meeting_Teacher();
            $connection->id_teacher = $request->teacher_ids[$i];
            $connection->id_meeting = $meeting->id;
            $connection->is_moderated_by_teacher = 0;
            $connection->save();
        }
        $view = view('inside.meeting.create_success');

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'title' => $sections['title'],
            ]);
        }

        return $view;
    }


    public function accept_moderation($meeting_id){

        Meeting::where('id_meeting', $meeting_id)
            ->update(['is_moderated' => 2]);
        return 1;
    }

    public function reject_moderation($meeting_id){
        Meeting::where('id_meeting', $meeting_id)
            ->update(['is_moderated' => 0]);
        return 1;
    }

    public function send_to_moderation($meeting_id){
        Meeting::where('id_meeting', $meeting_id)
            ->update(['is_moderated' => 1]);
        return 1;
    }


    public function index($meeting_id){
        $meeting = Meeting::where('id_meeting', $meeting_id)->first();
        //dd($meeting);

        $teachers = Meeting_Teacher::where('id_meeting', $meeting_id)->get();
        //dd($teachers);

        $view = view('inside.meeting.card')->with([
            'meeting' => $meeting,
            'teachers' => $teachers,
        ]);

        if (request()->ajax()) {
            $sections = $view->renderSections();

            return response()->json([
                'content' => $sections['content'],
                'title' => $sections['title']
            ]);
        }
        return $view;
    }


    public function edit_meeting($meeting_id){
        $meeting = Meeting::where('id_meeting', $meeting_id)->get();
        $first_meetings = $meeting -> first();

        $teachers = Meeting_Teacher::where('id_meeting', $meeting_id)->get();
        //dd($teachers->teacher->user->photo->type);

        $company_admin = CompanyAdmin::where('user_id', Auth::id())->first();

        //dd($meeting);

        $view = view('inside.meeting.edit_meeting')->with([
            'meetings' => $meeting,
            'first_meetings' => $first_meetings,
            'company_admin' => $company_admin,
            'teachers' => $teachers
        ]);



        if (request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title']
            ]);
        }

        return $view;
    }

    public function update_meeting(Request $request){
        Meeting::where(['id_meeting' => $request -> id])
            ->update(['name' => $request->name, 'description' => $request->discription, 'price' => $request->price, 'full_description' => $request->full_description,
                'meeting_length_hours' => $request->course_length_hours, 'max_number_students' => $request->max_number_students,
                'start_date' => $request->start_date,
                'address' => $request->address, 'skills' => $request->skills,
                'difficult_id' => $request->difficulty,'audience' => $request->audience ]);
        $view = view('inside.meeting.update_success')->with([

        ]);

        if (request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title']
            ]);
        }
        return $view;
    }

    public function delete_teacher($meeting_id, $teacher_id){
        Meeting_Teacher::where('id_meeting', $meeting_id)
            ->where('id_teacher', $teacher_id)
            ->delete();
        return 1;
    }

    public function delete_meeting($id_meeting){
        Meeting::where(['id' => $id_meeting])
            ->delete();
        Meeting_Teacher::where('$id_meeting', $id_meeting)->delete();
        return redirect('/id'.Auth::id());
    }


    public function buy_meeting($meeting_id){
        $user = Auth::id();
        $price = Meeting::where('id_meeting', $meeting_id)->select('price')->first();

        if ($price->price == 0)
        {
            $is_buy = Students_meetings::where('user_id', $user)->where('id_meeting', $meeting_id)->count();
            if ($is_buy == 0){
                $stud_off = new Students_meetings();
                $stud_off->id_meeting = $meeting_id;
                $stud_off->user_id = $user;
                $stud_off->pay = 1;
                $stud_off->save();
                return $user;
            }
            else{
                return 1;
            }
        }
        else{
            return 2;
        }
    }

    public function delete_from_userpage($meeting_id){
        Students_meetings::where('id_meeting', $meeting_id)
            ->where('user_id', Auth::id())
            ->delete();
        return 1;
    }

    public function go_to_webinar_room($room){
        $webinar_id = base64_decode($room);
        dd($webinar_id);
    }



}