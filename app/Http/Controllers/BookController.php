<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Difficult;
use App\Lessons;
use App\EventsModerated;
use DebugBar\DebugBar;
use App\User;
use App\Photo;
use App\Subscribe;
use App\Book;
use App\Category;
use App\Course;
use App\Materials;
use Image;
use Auth;


class BookController extends Controller
{

    public function edit_book($course_id, $book_id)
    {
        $book = Book::with(['user', 'lessons'])
            ->where('id', $book_id)
            ->first();
        if ($book->user_id == Auth::id()) {

            $view = view('inside.courses.books.edit')->with([
                'book' => $book,
                'course_id' => $course_id
            ]);

            if (request()->ajax()) {
                $sections = $view->renderSections();
                return response()->json([
                    'content' => $sections['content'],
                    'modal' => $sections['modal'],
                    'title' => $sections['title'],
                ]);
            }

            return $view;
        }
        return redirect('/id' . Auth::id());

    }

    public function preview_cid_book($course_id, $book_id)
    {
        $book = Book::with(['user', 'lessons'])
            ->where('id', $book_id)
            ->first();
        if ($book->user_id == Auth::id()) {

            $view = view('inside.courses.books.preview')->with([
                'book' => $book,
                'course_id' => $course_id
            ]);

            if (request()->ajax()) {
                $sections = $view->renderSections();
                return response()->json([
                    'content' => $sections['content'],
                    'modal' => $sections['modal'],
                    'title' => $sections['title'],
                ]);
            }

            return $view;
        }
        return redirect('/id' . Auth::id());

    }

    public function create_book($course_id)
    {
        $book = new Book;
        $book->name = 'Новый текстовый материал';
        $book->user_id = Auth::id();
        $book->save();

        $view = view('inside.courses.books.edit')->with([
            'book' => $book,
            'course_id' => $course_id
        ]);

        if (request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
                'id' => $book->id,
            ]);
        }
        return $view;
    }

    public function del_book($course_id, $book_id)
    {
        $book = Book::with(['user', 'lessons'])
            ->where('id', $book_id)
            ->first();
        //DebugBar::info(Auth::id());
        if ($book->user_id == Auth::id()) {
            $book->delete();

            $course = Course::where('id', $course_id)->first();

            $materials = Materials::where('course_id', $course_id)->get();

            $books = Book::with('lessons')
                ->where('user_id', Auth::id())
                ->get();

            $view = view('inside.courses.constructor')->with([
                'course' => $course,
                'books' => $books,
                'materials' => $materials
            ]);


            if (request()->ajax()) {
                $sections = $view->renderSections();
                return response()->json([
                    'content' => $sections['content'],
                    'modal' => $sections['modal'],
                    'title' => $sections['title']
                ]);
            }

            return $view;
        }
        return redirect('/id' . Auth::id());

    }

    public function add_lesson(Request $request)
    {
        $book = Book::where('id', $request->book_id)
            ->select('user_id')
            ->first();
        if ($book->user_id == Auth::id()) {
            $lesson = new Lessons;
            $lesson->name = $request->name;
            $lesson->book_id = $request->book_id;
            $lesson->parent_id = 0;
            $lesson->priority = $request->priority;
            $lesson->text = "";

            $lesson->save();

            return $lesson->id;
        }
        return redirect('/id' . Auth::id());
    }

    public function add_sublesson(Request $request)
    {
        $book = Book::where('id', $request->book_id)
            ->select('user_id')
            ->first();
        if ($book->user_id == Auth::id()) {
            $lesson = new Lessons;
            $lesson->name = $request->name;
            $lesson->book_id = $request->book_id;
            $lesson->parent_id = $request->parent_id;
            $lesson->priority = $request->priority;
            $lesson->text = "";

            $lesson->save();

            return $lesson->id;
        }
        return redirect('/id' . Auth::id());

    }

    public function del_lesson(Request $request)
    {
        $book = Book::where('id', $request->book_id)
            ->select('user_id')
            ->first();
        if ($book->user_id == Auth::id()) {
            $lesson = Lessons::find($request->lesson_id);
            $deletedRows = Lessons::where('parent_id', $lesson->id)->delete();

            $lesson->delete();

            return $request->lesson_id;
        }
        return redirect('/id' . Auth::id());
    }

    public function del_sublesson(Request $request)
    {
        $book = Book::where('id', $request->book_id)
            ->select('user_id')
            ->first();
        if ($book->user_id == Auth::id()) {
            $lesson = Lessons::where('id', $request->lesson_id)->delete();

            return $request->lesson_id;
        }
        return redirect('/id' . Auth::id());

    }

    public function get_lesson(Request $request)
    {
        $lesson = Lessons::find($request->lesson_id);

        return $lesson;
    }

    public function get_all_lesson($book_id, Request $request)
    {
        $lesson = Lessons::where('book_id', $book_id)->get();
        return $lesson;
    }


    public function set_lesson(Request $request)
    {
        $book = Book::where('id', $request->book_id)
            ->select('user_id')
            ->first();
        if ($book->user_id == Auth::id()) {
            $lesson = Lessons::find($request->lesson_id);
            $lesson->name = $request->name;
            $lesson->text = $request->text;

            $lesson->save();

            return $lesson->id;
        }
        return redirect('/id' . Auth::id());

    }

    public function rename_book(Request $request)
    {
        $book = Book::where('id', $request->book_id)
            ->first();
        if ($book->user_id == Auth::id()) {
            $book->name = $request->name;
            $book->save();
            return $book->id;
        }
        return redirect('/id' . Auth::id());

    }
}

