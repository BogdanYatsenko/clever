<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.11.2018
 * Time: 18:09
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Sources extends Model
{
    protected $table = 'sources';
    public $timestamps = false;

}