<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.10.2018
 * Time: 11:39
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{

    protected $table = 'meetings';

    public function teacher(){
        return $this->hasMany('App\Meeting_Teacher', 'id_meeting', 'id_meeting');
    }

    public function user(){
        return $this->belongsTo('App\User', 'id_user_creator', 'id');
    }

    public function photo(){
        return $this->belongsTo('App\Photo', 'photo_id', 'id');
    }

    public function mit_type(){
        return $this->belongsTo('App\Meeting_types', 'id_type', 'id');
    }

    public function source(){
        return $this->belongsTo('App\Sources', 'id_source', 'id');
    }

}