@extends('inside.index')

@section('title', 'Контакты')

@section('modal')
@endsection


@section('content')

    <link rel="stylesheet" href="/css/inside/people.css">

    <div class="container has-people-api">

        <div class="row">
            <div id="search_wrap" class="col-lg-12">
                <div class="input-group">
                    <input type="search" class="user-search-input search-input form-control" placeholder="Поиск контактов">
                    <span class="input-group-btn">
                        <button class="user-search-btn btn btn-default" type="submit">
                            Найти
                        </button>
                    </span>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="users endless-pagination col-lg-9" data-next-page="{{ $users->nextPageUrl() }}">
                @foreach ($users as $user)
                    <div class="profile col-lg-3 col-sm-6 text-center">
                        <a href="/id{{ $user->id }}">
                            @if (!is_null($user->photo_id))
                                @foreach($photos as $photo)@if($photo->id == $user->photo_id)
                                <img class="user-avatar" src="{{Storage::url('avatars/')}}{{ $user->id }}.{{$photo->type}}">
                                @endif @endforeach
                            @else
                                <img class="img-circle img-responsive img-center" src="/img/no_avatar.png">
                            @endif
                        </a>
                        <h5 class="h5">
                            {{ $user->first }} {{ $user->second }}
                            {{-- <small>Job Title</small> --}}
                        </h5>
                    </div>
                @endforeach

            </div>

            <div class="col-lg-3 form-options">
                <div class="col-lg-12 first">

                </div>
            </div>

        </div>

    </div>

    <script type="text/javascript">
        var token = '{{ Session::token() }}';
    </script>

@stop
