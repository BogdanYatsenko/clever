
 @section ('messages')

    @foreach ($messages->sortBy('id') as $message)
        @include('inside.messenger.partials.message', [
            'message' => $message,
            'users' => $users
        ])
    @endforeach

    {{ $messages->links() }}

@endsection

@section ('send_form')

    <div class="send-wrap" data-thread_id={{ $threadId }}>
        <div class="general-form">
            <div class="input-box col-md-10">
                <div id="input-div" contenteditable="true" placeholder="Написать собщение..."></div>
            </div>
            <div class="send-btn col-md-2">
                <button type="button" class="btn btn-default">Отправить</button>
            </div>
        </div>
    </div>

@endsection
