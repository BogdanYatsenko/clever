
@php
    $users = $thread->participantsString(Auth::id());
    $mcount = $thread->userUnreadMessagesCount(Auth::id());
@endphp

@if (count($users) > 0)
        @if (count($users) < 2)

            @php
                $single = $users[0];
            @endphp
            <div class="conversation" multy-thread=false data-contacts="{{ $single->id }}" data-thread_id="{{ $thread->id }}">
                <div class="msg-photo col-md-2">
                    @if (is_null($single->photo_id))
                        <img class="media-object" alt="{{ $single->first }} {{ $single->second }}" src="/img/no_avatar.png">
                    @else
                        <img class="media-object" alt="{{ $single->first }} {{ $single->second }}" src="{{ Storage::url('avatars/') }}{{ $single->id }}.{{ $single->type }}">
                    @endif
                </div>

                @php
                    $LastMessage = $thread->getLatestMessageAttribute();
                    $thisParticipant = $thread->getParticipantFromUser(Auth::id())
                @endphp

                <div class="msg-body col-md-10">
                    {{-- <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> --}}
                    <h5>{{ $single->first }} {{ $single->second }}</h5>

                    @if (count($LastMessage) > 0)
                        @if  ($thisParticipant->created_at > $LastMessage->created_at )
                            <small class="text-grey">Новая беседа</small>
                        @else
                        <small>{!! $LastMessage->body !!}</small>
                        <span class="msg-time">
                            {{ $LastMessage->created_at->diffForHumans() }}
                        </span>
                        @endif
                    @else
                        <small class="text-grey">Новая беседа</small>
                    @endif

                    <span class="thread-remove">
                        Удалить
                    </span>
                </div>
                @if ( $mcount > 0)
                    <div class="msg-count">
                        <span class="number">{{ $mcount }}</span>
                    </div>
                @endif
            </div>
        @else
            <div class="media conversation" multy-thread=true data-contacts="{{ json_encode($users) }}" data-thread_id="{{ $thread->id }}">

                <div class="pull-left img-group">
                    @foreach ($users->slice(0, 3) as $user)
                        @if (is_null($user->photo_id))
                            <a class="pull-left" href="#">
                                <img class="media-object" alt="{{ $user->first }} {{ $user->second }}" src="/img/no_avatar.png">
                            </a>
                        @else
                            <a class="pull-left" href="#">
                                <img class="media-object" alt="{{ $user->first }} {{ $user->second }}" src="{{ Storage::url('avatars/') }}{{ $user->id }}.{{ $user->type }}">
                            </a>
                        @endif
                    @endforeach
                </div>

                <div class="media-body">
                    <h5>{{ $thread->subject }}</h5>
                    <small>{{ $thread->getLatestMessageAttribute()->body }}</small>
                </div>

            </div>
        @endif
@endif
