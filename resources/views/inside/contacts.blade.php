@extends('inside.index')

@section('title', 'Контакты')

@section('modal')
@endsection


@section('content')
<link rel="stylesheet" href="{{ asset('css/home/body_inside.css') }}">
<div class="main container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-header">
                        <h1>Контакты</h1>
                    </div>
                    <div class="contacts col-md-10 col-md-offset-1 col-sm-12col-xs-12">
                        <div class="col-md-6 col-sm-4 col-offset-2 col-xs-12 text-right-md">Телефон:</div>
                        <div class="col-md-6 col-sm-6 col-xs-12">+7(4852) 79-77-26</div>
                        <div class="col-md-6 col-sm-4 col-offset-2 col-xs-12 text-right-md">Email:</div>
                        <div class="col-md-6 col-sm-6 col-xs-12">info@clever-e.com</div>
                        <div class="col-md-6 col-sm-4 col-offset-2 col-xs-12 text-right-md">Юридический адрес:</div>
                        <div class="col-md-6 col-sm-6 col-xs-12">г. Ярославль, ул. Полушкина Роща 16, стр. 67а, офис 3</div>
                        <div class="col-md-6 col-sm-4 col-offset-2 col-xs-12 text-right-md">ИНН:</div>
                        <div class="col-md-6 col-sm-6 col-xs-12">7606047112</div>
                        <div class="col-md-6 col-sm-4 col-offset-2 col-xs-12 text-right-md">КПП:</div>
                        <div class="col-md-6 col-sm-6 col-xs-12">760201001</div>
                        <div class="col-md-12 col-sm-12 col-xs-12 text-muted text-center">
                            Cайт принадлежит организации ООО "А-РЕАЛ КОНСАЛТИНГ"
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
