@extends('inside.index')

@section('title', 'Карточка курса '.$course->name)

@section('content')


    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12" style = "padding: 0;">
                    <header id="header">

                        <div class="slider">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">

                                        <div class="col-md-12  col-sm-12 col-xs-12" style = "min-height: 250px; background-color: rgb(0, 177, 166); padding-right: 15px;">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="col-xs-6 navbar-brand-course" style = "background-color: #fff;  margin-top:10px; margin-left:0px;">
                                                        @if (!is_null($course->photo_id))
                                                            <div class = "user-avatar-sm">
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><img class="course-avatar-pic" src="{{Storage::url('course_avatars/')}}{{ $course->id  }}.{{$course->photo->type}}"></div>
                                                            </div>
                                                        @else
                                                            <div class = "user-no-avatar-sm">
                                                                <img class="course-avatar-pic" src="/img/course/no_avatar.png">
                                                            </div>
                                                        @endif


                                                    </div>

                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="wrap">
                                                        <div class="border-wrap">
                                                            <div id="rating_3">
                                                                {{--<input name="val" value="{{$course -> rating/ $course-> num_voiters}}" type="hidden">--}}
                                                                <input name="votes" value="{{$course -> num_voiters}}" type="hidden">
                                                                <input name="vote-id" value="3" type="hidden">
                                                                <input name="cat_id" value="2" type="hidden">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <script>
                                                    function getCookie(name) {
                                                        var matches = document.cookie.match(new RegExp(
                                                                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                                                        ));
                                                        return matches ? decodeURIComponent(matches[1]) : undefined;
                                                    }


                                                    $(function() {
                                                        $('#rating_3').rating({

                                                            fx: 'float',
                                                            image: '../../img/course/stars.png',
                                                            loader: '../../img/course/ajax-loader.gif',
                                                            minimal: 1,
                                                            //url: '/offline_course/card/rating/id'+id,
                                                            //type: 'get',
                                                            click: function (object) {
                                                                var currentLocation = window.location.pathname;
                                                                var id = currentLocation.substr(23, currentLocation.length);
                                                                if(getCookie('clever-e'+id)){
                                                                    alert('Вы уже оценивали этот курс.');
                                                                }
                                                                else {

                                                                    $.get('/offline_course/card/rating/id'+ id+'_'+object).done( function () {
                                                                        alert('Ваш голос учтён!');
                                                                        setCookie('clever-e'+id, object, 3600*24*31);
                                                                        function setCookie(name, value, options) {
                                                                            options = options || {};

                                                                            var expires = options.expires;

                                                                            if (typeof expires == "number" && expires) {
                                                                                var d = new Date();
                                                                                d.setTime(d.getTime() + expires * 3600*24*31);
                                                                                expires = options.expires = d;
                                                                            }
                                                                            if (expires && expires.toUTCString) {
                                                                                options.expires = expires.toUTCString();
                                                                            }

                                                                            value = encodeURIComponent(value);

                                                                            var updatedCookie = name + "=" + value;

                                                                            for (var propName in options) {
                                                                                updatedCookie += "; " + propName;
                                                                                var propValue = options[propName];
                                                                                if (propValue !== true) {
                                                                                    updatedCookie += "=" + propValue;
                                                                                }
                                                                            }

                                                                            document.cookie = updatedCookie;
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        });
//                                                                callback: function (responce) {
//
//                                                                    this.vote_success.fadeOut(2000);
//                                                                    if (responce.msg) alert(responce.msg);
//                                                                }
                                                    });




                                                </script>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <p class="slider_txt" style="color: #fff; float: left;  -moz-hyphens: auto;   -webkit-hyphens: auto;   -ms-hyphens: auto; font-family: 'Open Sans Condensed';" >{{$course->name}}</p>
                                                    </div>
                                            </div>
                                            <div class="row" style="margin-left:0px">

                                                    @if ($course->author == "")

                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <p class = "site-name-cat"><b>Автор курса неизвестен</b> </p></div>
                                                    @else
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a class = "site-name-cat" href="/id{{ $course->author_link }}"><b>Автор оффлайн курса: {{$course->author}}</b> </a></div>
                                                    @endif

                                                <div class="col-lg-12 col-md-3 col-sm-3 col-xs-12"> <p style = "font-size: 12px; color: #fff; float: left; margin-left: -15px; font-family: 'Open Sans Condensed'; line-height: 22pt" >Количество просмотров: {{$course->num_views}}</p></div>
                                            </div>

                                        </div>


                                </div>


                            </div>
                        </div>
                        <nav class="navbar navbar-default" style = "margin-bottom: 0; border-radius: 0;">
                            <div class="navbar">


                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-3">  <p>Дата начала: {{$course->start_date}}</p> </div>
                                <div class="col-lg-4col-md-3 col-sm-3 col-xs-3">  <p>Осталось мест: {{$course->registration_count}}</p></div>
                                <div class="take_part_btn col-lg-4 col-md-3 col-sm-3 col-xs-3" style = "font-size: 16pt;">

                                    @if ($course->price > 0)

                                            <div class = "btn btn-success btn-right" style = "background-color: rgb(0, 177, 166);"><b> Принять участие {{$course->price}}</b> р. </div>
                                    @else
                                        @if ($course->id_user_creator != Auth::id())
                                            <div class = "btn btn-success buy-offline_course" style = "background-color: rgb(0, 177, 166); " >
                                                <a class="take_part_offline_btn" href="/offline_course/buy/id{{$course->id}}" style="text-decoration: none; color: white;" data-id="{{$course->id}}"><b>Принять участие </b></a>
                                            </div>

                                        @else
                                            <div class = "btn btn-success buy-offline_course" style = "background-color: rgb(0, 177, 166); " >
                                                <b>Вы преподаватель</b>
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </nav>
                    </header>



                    <nav class="navbar navbar-default" style = "margin-bottom: 10px; border-radius: 0; font-size: 1.2em; height: auto; min-height: 150px;">
                        <div class="navbar-header" style = "padding:20px;">
                            @if($course->full_description == "")
                                <p style="word-wrap: break-word;"> Полное описание: Нет информации </p><br>
                            @else
                                <p style="word-wrap: break-word; ">{!! $course->full_description !!}</p><br>
                            @endif
                        </div>
                    </nav>


                    @if(count($teachers) != 0)
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                Преподаватели мероприятия:
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    @foreach($teachers as $teacher)
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                @if(isset($teacher->teacher->user->photo->type))
                                                    <img src='{{Storage::url('avatars/')}}{{$teacher->teacher->user->id}}.{{$teacher->teacher->user->photo->type}}' style='width:35px'/> <b>&nbsp;&nbsp;<a href="/id{{$teacher->teacher->user->id}}">{{$teacher->teacher->user->first}} {{$teacher->teacher->user->second}}</a>&nbsp;</b>

                                                    {{--<input data-content="<img src='{{Storage::url('avatars/')}}{{$teacher->teacher->user->id}}.{{$teacher->teacher->user->photo->type}}' style='width:35px'/><b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->teacher->user->first}} {{$teacher->teacher->user->second}}</b> " data-id="{{$teacher->teacher->id_teacher}}" value="{{$teacher->teacher->user->first}} {{$teacher->teacher->user->second}}">--}}
                                                @else
                                                    <img src='/img/no_avatar.png' style='width:35px'/> <b>&nbsp;&nbsp;<a href="/id{{$teacher->teacher->user->id}}">&nbsp;&nbsp;&nbsp;{{$teacher->teacher->user->first}} {{$teacher->teacher->user->second}}</a></b>

                                                    {{--<input data-content="<img src='/img/no_avatar.png' style='width:35px'/><b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->teacher->user->first}} {{$teacher->teacher->user->second}}</b> " data-id="{{$teacher->teacher->id_teacher}}" value="{{$teacher->teacher->user->first}} {{$teacher->teacher->user->second}}">--}}
                                                @endif


                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                Преподаватели не указаны.
                            </div>
                        </div>
                    @endif




                    <div id="roll" class = "btn btn-success" style = "background-color: rgb(0, 177, 166); width: 100%"><b> Показать полностью </b> </div>
                    <br>
                    <script>
                        $("#roll").click(function () {
                            $("#offline").slideToggle("slow")
                        })
                    </script>
                    <div id = "offline" style="display: none">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12" style="margin-top: 10px;">
                                <div class="form-group">
                                @if($course->course_length_hours == "")
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;">
                                            <p style="padding: 10px; word-wrap: break-word;  height: auto"> Длительность курса в часах: Нет информации </p><br>

                                        </nav>
                                @else
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;">
                                            <p style="padding: 10px; word-wrap: break-word;  height: auto;"> Длительность курса в часах:  {!! $course->course_length_hours !!}</p><br>
                                        </nav>
                                @endif
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12" style="margin-top: 10px;">
                                <div class="form-group">
                                    @if($course->lessons_count == "")
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em; word-wrap: break-word;">
                                            <p style="padding: 10px; word-wrap: break-word;  height: auto;"> Количество занятий: Нет информации </p><br>
                                        </nav>
                                    @else
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em; word-wrap: break-word;;">
                                            <p style="padding: 10px; word-wrap: break-word; height: auto"> Количество занятий:  {!! $course->lessons_count !!}</p><br>
                                        </nav>
                                    @endif

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12" style="margin-top: 10px;">
                                <div class="form-group">
                                    @if($course->difficult_id == "")
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;word-wrap: break-word;">
                                            <p style="padding: 10px; word-wrap: break-word; height: auto"> Сложность: Нет информации </p><br>
                                        </nav>
                                    @else
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;word-wrap: break-word;">
                                            @if ($course->difficult_id == 1) <p style="padding: 10px; word-wrap: break-word;  height: auto"> Сложность:  лёгкая </p><br> @endif
                                            @if ($course->difficult_id == 2) <p style="padding: 10px; word-wrap: break-word;  height: auto"> Сложность:  средняя </p><br> @endif
                                            @if ($course->difficult_id == 3) <p style="padding: 10px; word-wrap: break-word;  height: auto"> Сложность:  высокая </p><br> @endif
                                        </nav>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12" style="margin-top: 10px;">
                                <div class="form-group">
                                    @if($course->knowlegde_requires == "")
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;word-wrap: break-word;">
                                            <p style="padding: 10px; word-wrap: break-word;  height: auto"> Необходимые знания: Нет информации </p><br>
                                        </nav>
                                    @else
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;word-wrap: break-word;">
                                            <p style="padding: 10px; word-wrap: break-word;  height: auto"> Необходимые знания:  {!! $course->knowlegde_requires !!}</p><br>
                                        </nav>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12" style="margin-top: 10px;">
                                <div class="form-group">
                                    @if($course->audience  == "")
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;word-wrap: break-word;">
                                            <p style="padding: 10px; word-wrap: break-word; height: auto"> Целевая аудитория курса:  Нет инфомарции </p><br>
                                        </nav>
                                    @else
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;word-wrap: break-word;">
                                            <p style="padding: 10px; word-wrap: break-word; height: auto"> Целевая аудитория курса:  {!! $course->audience !!}</p><br>
                                        </nav>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12" style="margin-top: 10px;">
                                <div class="form-group">
                                    @if($course->skills == "")
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;word-wrap: break-word;">
                                            <p style="padding: 10px; word-wrap: break-word;  height: auto"> Полученные знания:  Нет информации </p><br>
                                        </nav>
                                    @else
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;word-wrap: break-word;">
                                            <p style="padding: 10px; word-wrap: break-word;  height: auto"> Полученные знания:  {!! $course->skills !!}</p><br>
                                        </nav>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12" style="margin-top: 10px;">
                                <div class="form-group">
                                    @if($course->address == "")
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;word-wrap: break-word;">
                                            <p style="padding: 10px; word-wrap: break-word;  height: auto"> Место проведения:  Нет информации </p><br>
                                        </nav>
                                    @else
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;word-wrap: break-word;">
                                            <p style="padding: 10px; word-wrap: break-word; height: auto"> Место проведения:  {!! $course->address !!}</p><br>
                                        </nav>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12" style="margin-top: 10px;">
                                <div class="form-group">
                                    @if($course->max_number_students == "")
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;word-wrap: break-word;">
                                            <p style="padding: 10px; word-wrap: break-word;  height: auto"> Максимальное количество студентов:  Нет информации </p><br>
                                        </nav>
                                    @else
                                        <nav class="navbar navbar-default" style = "border-radius: 0; font-size: 1.2em;word-wrap: break-word;">
                                            <p style="padding: 10px; word-wrap: break-word;  height: auto"> Максимальное количество студентов:  {!! $course->max_number_students !!}</p><br>
                                        </nav>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="courses_content">

                    </div>

                </div>
            </div>
        </div>


    </div>
    <br>


    <link href="../../css/styles.css" rel="stylesheet" type="text/css" />
    <link href="../../css/jquery.rating.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

    <script type="text/javascript">
        window.jQuery || document.write('<script type="text/javascript" src="js/jquery-1.6.2.min.js"><\/script>');
    </script>

    <script type="text/javascript" src="{{ asset('js/jquery.rating-2.0.js') }}"> alert("SS");</script>


@endsection