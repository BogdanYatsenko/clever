@extends('inside.index')

@section('title', 'Настройки курса')

@section('modal')
@endsection


@section('content')
    <div class="container">
        <div class="row margin-row">
            <form id ="config-course-form" role="form" method="POST" action="/constructor/config/id{{$course->id}}">
                {{ csrf_field() }}
                <div class="col-md-12  col-sm-12 col-xs-12 ">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">
                            <button action="/constructor/id{{$course->id}}" type="button" class="btn-default btn btn-left goto_edit_course" ><b>Отмена</b></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Название курса:
                                <input id="course_name" class="form-control" placeholder="Введите название курса" name="course_name" required autofocus type="text" value="{{$course->name}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Категория:
                                <select id="category" class="selectpicker form-control" name="category_name" data-live-search="true" data-link="/constructor/config/id{{$course->id}}/vector">
                                    <option data-id="0"> Все категории</option>
                                    @foreach($categories as $category)
                                        <option data-id="{{$category->id}}"
                                                @if($category->id == $course->category_id)
                                                selected
                                                @endif
                                        >{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div id = "vektor-list" class="vectors-list">
                                <div  class="form-group @if (is_nan($course->category_id) or is_null($course->category_id)) hidden @endif">
                                    Направление:
                                    <select class="select_vektor selectpicker form-control" name="vector_name" title = "Укажите категорию"   data-live-search="true">
                                        <option data-id = '0'>Все направления</option>

                                        @if (!is_nan($course->vector_id))
                                            @foreach($vectors as $vector)
                                                <option data-id = "{{$vector->id}}"
                                                        @if($vector->id == $course->vector_id)
                                                        selected
                                                        @endif
                                                >{{$vector->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Сложность:
                                <select id="difficult_cat" class="selectpicker form-control" name="difficult">
                                    <option data-id="0">Сложность курса</option>
                                    @foreach($difficult as $difficult)
                                        <option data-id="{{$difficult->id}}"
                                                @if($difficult->id == $course->difficult_id)
                                                selected
                                                @endif
                                        >{{$difficult->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Краткое описание:
                                <textarea type="text" rows="3" class="form-control" id="discription" placeholder="Краткое описание" >{{$course->discription}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Полное описание:
                                <textarea type="text" rows="3" class="form-control" id="full_description" placeholder="Полное описание" >{{$course->full_description}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Цена курса:
                                <div class="input-group">
                                    @if ($course->price > 0)
                                    <input id="course_price" class="form-control" placeholder="Цена" name="price" type="number" min="0"  value="{{$course->price}}">
                                    <span class="input-group-addon">
                                        <input class="free" aria-label="..." name="free"  type="checkbox">
                                    Бесплатный
                                    </span>
                                        @else
                                        <input id="course_price" class="form-control" placeholder="Цена" name="price" type="number" min="0" >
                                        <span class="input-group-addon">
                                        <input class="free" aria-label="..." name="free"  type="checkbox" checked>
                                    Бесплатный
                                    </span>
                                    @endif
                                </div>
                                <div class="help-block hidden help-error">

                                </div>
                            </div>
                        </div>
                    </div>
                    @if (!is_null($course->material_link ))
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input class="xhr_course" aria-label="..." name="xhr_course"  type="checkbox" disabled checked>
                                    Материалы курса находятся на стороннем ресурсе
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Ссылка на материалы:
                                <input id="xhr_link" class="form-control" placeholder="Введите ссылку на материалы курса" name="xhr_link" type="text" value="{{$course->material_link}}" required>
                                <div class="help-block-xhr hidden help-error">

                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">
                            <button action="/constructor/id{{$course->id}}" type="button" class="btn-default btn btn-left goto_edit_course" ><b>Отмена</b></button>
                            <button id="apply_config_course" type="submit" class="btn btn-success btn-right "><b>Сохранить изменения</b></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>


        <script>
            $(document).ready(function() {
                $('.selectpicker').selectpicker({
                    size: 7
                });
                $('body').on('hide.bs.select', 'select[name="category_name"]', function (e) {
                    var category_id = $(this).find(':selected').data('id');
                    var link = $(this).data('link');
                    if (category_id > 0) {
                        get_vector(category_id, link);
                    }
                    else{
                        $('option', 'select[name="vector_name"]').not(':eq(0)').remove();
                        $('select[name="vector_name"]').selectpicker('refresh');
                        $('select[name="vector_name"]').parent().parent().addClass('hidden');
                    }
                });
            });

            function get_vector (category_id, link){
                var _token = $("input[name='_token']").val();
                $.post(link, {
                    _token: _token,
                    category_id: category_id
                }, function (response) {
                    $('option', 'select[name="vector_name"]').not(':eq(0)').remove();
                    $.each(response, function (key) {
                        $('select[name="vector_name"]')
                            .append($("<option></option>")
                                .attr("data-id", response[key].id)
                                .text(response[key].name));
                    });
                    $('select[name="vector_name"]').selectpicker('refresh');
                    $('select[name="vector_name"]').parent().parent().removeClass('hidden');
                });

            }
        </script>
    </div>



@endsection