@extends('inside.index')
@section('title', 'Результаты теста: '.$quiz->name)

@section('modal')
@endsection

@section('content')
    <script type="text/x-mathjax-config">

      MathJax.Hub.Config({

        tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}

      });

    </script>

    <style>
        .is_true, .question_ok {
            color: #0a7e07;
            font-weight: bold;

        }

        .is_false, .error{
            color: #9c3328 !important;
            font-weight: bold;
        }

        .question-block{
            border: 1px solid black;
            margin-top: 10px;
            padding: 15px;
        }

    </style>

    <script>
        $( document ).ready(function() {
            var _token = $("input[name='_token']").val();

            errors = $('.error').length;
            trues = $('.question_ok').length;

            all = errors+trues;

            percent = Math.floor(trues/all*100);
            $.post('/course/id{{$course_id}}/quiz{{$quiz_id}}/result', {
                _token: _token,
                result: percent
            }, function () {

            });

            $(".results_count").html("Кол-во правильных " + trues + " из " + all + " ("+percent+"%)");
        });
    </script>

    <div class="container" style = "padding: 0;">

        <div class="row">
            <div class="col-md-12 user-name">
                Результаты теста "{{$quiz->name}}"<br>
                <p class = "results_count">  </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6 add-marginb-30">
                <button action="action" onclick="window.history.go(-1); return false;"  type="button" class="btn-default btn btn-left goto_material" ><b><-- Вернуться </b></button>
            </div>

        </div>
        @if (isset($quiz->questions))
            @if (!is_null($quiz->questions) && !empty($quiz->questions))

            @foreach ($quiz->questions as $question)
                    <div class = "question-block"><b>{!! $question->name !!}</b>

                        @php $question_ok = true; $answers_count = 0; @endphp


                    @if (isset($question->variant))
                    @foreach ($question->variant as $variant)
                                <div class =
                       @foreach ($user_answers  as $answer)
                            @if (($answer->question_id == $question->id) && ($answer->variant_id == $variant->id ) && (isset($variant->is_true)))"is_true"
                                @php $answers_count++; @endphp
                            @elseif(($answer->question_id == $question->id) && ($answer->variant_id == $variant->id ) && (!isset($variant->is_true)))"is_false"
                                @php $question_ok = false; $answers_count++; @endphp
                            @endif
                       @endforeach
                                    >{!! $variant->name !!}</div>
                    @endforeach
                    @endif

            @if ((!$question_ok) && ($answers_count > 0)) <div class = 'error'>Ошибка в ответе</div>@endif
            @if (($question_ok) && ($answers_count > 0)) <div class = 'question_ok'>Ответ правильный</div> @endif
            @if ($answers_count == 0)<div class = 'error'>Пользователь не ответил на вопрос</div> @endif

            @php $answers_count = 0; @endphp
                </div>
            @endforeach

            @endif
        @endif

    </div>
@endsection
