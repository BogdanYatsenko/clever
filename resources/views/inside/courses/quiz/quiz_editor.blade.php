@extends('inside.index')

@section('title', 'Редактирование теста')

@section('modal')

@endsection

@section('content')

    <style>
        .question_name{
            width: 100%;
            padding: 5px;
            margin-bottom: 10px;
        }

        .variant_name{
            margin-bottom: 10px;
        }

        .checkbox{
            margin-top: 20px !important;
        }

        .add_variant{
         margin: 20px 0;
        }

        .question_block{
            border: 1px solid black;
            margin-top: 10px;
            padding: 10px;
        }
        .divtextarea{
            min-height: 50px;
            background-color: #fff;
            margin: 5px;
            border: 1px solid #00adea;
            padding: 5px;
        }
        .cke{
            margin: 3px;
            border: 1px solid #00adea;

        }

    </style>

    <div class = "container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6 add-marginb-30">
                <button action="/constructor/id{{$course_id}}" type="button" class="btn-default btn btn-left goto_edit_course" ><b><-- Вернуться в конструктор</b></button>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 add-marginb-30 delete-cont ">
                <div class = "save_quiz btn btn-success btn-right" data-link ="/quiz/edit/cid_{{$course_id}}/id{{$quiz_id}}/save">Сохранить изменения</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
        <form class = "quiz">
            <div class="row">
                <div class="col-md-2  col-sm-2 col-xs-12">
                    Название тест:
                </div>
                <div class="col-md-10  col-sm-10 col-xs-12">
                    <input name = "test_name" class="col-md-12" title="Название теста" value="{{$quiz->name}}"placeholder="Укажите название теста" type="text">
                </div>
            </div>
            @if (isset($quiz->questions))
            @if (!is_null($quiz->questions) && !empty($quiz->questions))
                @foreach ($quiz->questions as $question)
                        <div class = "question_block row col-md-12">
                            <div style = "margin: 10px 0 10px 10px;" class = "title-question">Вопрос № {{$question->id}}</div>
                            <div class = "col-md-2 btn btn-default del-question" onclick="" style = "position:absolute; right: 10px; top: 5px;">Удалить</div>
                            <input name = "question_id" class = "hidden" value = "{{$question->id}}" />
                            <textarea name = "question_name" class = "question_name hidden">{{$question->name}}</textarea>
                            <div class = "ckeditor divtextarea">{!! $question->name !!}</div>
                                @if (isset($question->variant))
                                @foreach ($question->variant as $variant)
                                <div class = "variant_block row">
                                <input name = "variant_id" class = "hidden variant_id" value = "{{$variant->id}}" />
                                <div class = "col-md-1">
                                <input  name = "is_true" type = "checkbox" class = " col-md-12 checkbox" @if(isset($variant->is_true))checked @endif/>
                                </div>
                                <div class = "col-md-8">
                                <textarea name = "variant_name" class = "col-md-12 variant_name hidden">{{$variant->name}}</textarea>
                                <div class = "col-md-12 ckeditor divtextarea">{!! $variant->name !!}</div>
                                </div>
                                    <div class = "col-md-2 btn btn-default del-variant">Удалить</div>
                                </div>
                                @endforeach
                                @endif
                            <div class = "col-md-12 col-xs-12 add_variant btn btn-success">Добавить вариант</div>
                            </div>
                @endforeach
            @endif
            @endif

                {{--@foreach ($quiz as $elem)--}}
                    {{----}}
                    {{----}}
                    {{--@if ($elem->question == 'question_id')--}}
                        {{--<script>--}}
                            {{--block = '<div class = "question_block row">'+--}}
                                {{--'<div style = "margin: 10px 0 10px 10px;">Вопрос №'+ {{$elem->value}}+'</div>'+--}}
                                {{--'<input name = "question_id" class = "hidden" value = "'+ {{$elem->value}}+'" />'+--}}
                                {{--'<textarea name = "question_name" class = "question_name hidden"></textarea>'+--}}
                                {{--'<div class = "ckeditor divtextarea question_name_div"></div>'+--}}
                                {{--'<div class = "col-md-12 col-xs-12 add_variant btn btn-success">Добавить вариант</div>'+--}}
                                {{--'</div>';--}}
                            {{--$('.quiz').append(block);--}}

                        {{--</script>--}}
                    {{--@endif--}}

                    {{--@if ($elem->name == 'question_name')--}}
                        {{--<script>--}}

                            {{--$('.question_name:last').html('{!! preg_replace('~[[:cntrl:]]~', '',$elem->value) !!}');--}}
                            {{--$('.question_name:last').next().html('{!! preg_replace('~[[:cntrl:]]~', '',$elem->value) !!}');--}}

                        {{--</script>--}}
                    {{--@endif--}}

                    {{--@if ($elem->name == 'variant_id')--}}
                        {{--<script>--}}
                            {{--block =--}}
                                {{--'<div class = "variant_block row">'+--}}
                                {{--'<input name = "variant_id" class = "hidden" value = "' + {{$elem->value}} + '" />'+--}}
                                {{--'<div class = "col-md-1">'+--}}
                                {{--'<input  name = "is_true" type = "checkbox" class = " col-md-1 checkbox"/>'+--}}
                                {{--'</div>'+--}}
                                {{--'<div class = "col-md-11">'+--}}
                                {{--'<textarea name = "variant_name" class = "col-md-12 variant_name hidden"></textarea>'+--}}
                                {{--'<div class = "col-md-12 ckeditor divtextarea"></div>'+--}}
                                {{--'</div>'+--}}
                                {{--'</div>';--}}
                            {{--$('.add_variant:last').before(block);--}}
                        {{--</script>--}}
                    {{--@endif--}}

                    {{--@if ($elem->name == 'is_true')--}}
                        {{--<script>--}}
                            {{--$('.variant_block:last .checkbox').prop('checked', true);--}}
                        {{--</script>--}}
                    {{--@endif--}}

                    {{--@if ($elem->name == 'variant_name')--}}
                        {{--<script>--}}
                            {{--$('.variant_name:last').html('{!! preg_replace('~[[:cntrl:]]~', '',$elem->value) !!}');--}}
                            {{--$('.variant_name:last').next().html('{!! preg_replace('~[[:cntrl:]]~', '',$elem->value) !!}');--}}

                        {{--</script>--}}
                    {{--@endif--}}

                {{--@endforeach--}}

            <div class = "col-md-12 col-xs-12 add_question btn btn-success" style = "margin-top: 15px;">Добавить вопрос</div>
        </form>
            </div>
        </div>

    </div>

    <script>

    </script>

@endsection