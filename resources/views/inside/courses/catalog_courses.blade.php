@extends('inside.courses.catalog')

@section('content_courses')
    <div class="container">
        @foreach($courses as $object)

            @if(get_class($object) == 'App\Course')
                @if(!is_null($object->source))
                    {{--Сторонний курс--}}
                    <div class="col-md-4 col-sm-6">
                        <div class="block span3 to_course" style="margin-bottom: 20px;">
                            <div class="product">
                                @if($object->photo_id == '')
                                    <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="/img/course/geekbrains.jpg">
                                @else
                                    <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="{{Storage::url('other_course_avatars/')}}{{ $object->id-1  }}.{{$object->photo->type}}">
                                @endif
                            </div>
                            <a href ="/other_course/card/id{{$object->id}}" class="to_card">
                                <div class="info" style="height: 290px;">
                                    <h4>Курс: {{$object->name}}</h4>
                                    <span class="description" style = "font-size: 10pt;">
                                    @php
                                        if(strlen($object->name) < 130) {
                                            if (strlen($object->description) > 120){
                                            echo mb_substr($object->description,0,120, 'UTF-8').'...';
                                            }
                                            else
                                                echo $object->description;
                                        }
                                        else echo '...';
                                    @endphp

                                </span>

                                </div>
                            </a>

                            <a href="http://www.{{ $object->source  }}" class = "source"  style="padding-left: 25px">
                                Площадка: {{ $object->source  }}
                            </a>


                            <div class="details">
                        <span class="time" style="font-size: 18pt;"><span class="price">
                            @if($object->price > 0)
                                    {{$object->price}} р.
                                    <a class="btn btn-info pull-right" href="/other_course/card/id{{$object->id}}"><i class="icon-shopping-cart" data-id="{{$object->id}}"></i>Подробно</a>
                                @else
                                    Бесплатно
                                    <a class="btn btn-info pull-right" href="/other_course/card/id{{$object->id}}"><i class="icon-shopping-cart" data-id="{{$object->id}}"></i>Подробно</a>
                                @endif
                            </span>
                            </div>
                        </div>
                    </div>
                @else
                    {{--Курсы Клевера--}}
                    <div class="col-md-4 col-sm-6">
                        <div class="block span3 to_course" style="margin-bottom: 20px;">
                            <div class="product">
                                @if (!is_null($object->photo_id))
                                    <div class = "user-no-avatar-sm" style = "height: 190px; object-fit: contain; background-color: #fff;">
                                        <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="{{Storage::url('course_avatars/')}}{{ $object->id  }}.{{$object->photo->type}}">
                                    </div>
                                @else
                                    <div class = "user-no-avatar-sm" style = "height: 190px; object-fit: contain; background-color: #fff;">
                                        <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="/img/course/no_avatar.png">
                                    </div>
                                @endif

                            </div>
                            <a href ="/course/card/id{{$object->id}}" class="to_card">
                                <div class="info" style="height: 290px;">
                                    <h4>Курс: {{$object->name}}</h4>
                                    <span class="description" style = "font-size: 10pt;">
                                    @php
                                        if(strlen($object->name) < 130) {
                                            if (strlen($object->description) > 120){
                                            echo mb_substr($object->description,0,120, 'UTF-8').'...';
                                            }
                                            else
                                                echo $object->description;
                                        }
                                        else echo '...';
                                    @endphp

                                    </span>
                                </div>
                            </a>
                            <a href="/" class = "source"  style="padding-left: 25px">
                                Площадка: Clever-e.com
                            </a>

                            <div class="details">
                        <span class="time" style="font-size: 18pt;"><span class="price">
                                @if ($object->price > 0)
                                    {{$object->price}} р.
                                    </span></span>
                                @if ($object->user_id != Auth::id())
                                    <span class="rating pull-right buy-course">
                                        <a class="btn btn-info pull-right" href="#"><i class="icon-shopping-cart" data-id="{{$object->id}}"></i>Купить</a>
                                    </span>
                                @endif

                                @else
                                    Бесплатно
                                    @if ($object->user_id != Auth::id())
                                        <span class="rating pull-right buy-course">
                                        <a class="btn btn-info pull-right" href="/course/buy/id{{$object->id}}" data-id="{{$object->id}}"><i class="icon-shopping-cart"></i>Изучать</a>
                                    </span>
                                        @endif
                                        @endif
                                        </span></span>
                            </div>
                        </div>
                    </div>
                @endif
            @elseif(get_class($object) == 'App\OtherCourses')
                <div class="col-md-4 col-sm-6">
                    <div class="block span3 to_course" style="margin-bottom: 20px;">
                        <div class="product">
                            @if(is_null($object->photo_id))
                                <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="/img/course/geekbrains.jpg">
                            @else
                                <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="{{Storage::url('other_course_avatars/')}}{{ $object->id-1  }}.{{$object->photo->type}}">
                            @endif
                        </div>
                        <a href ="/other_course/card/id{{$object->id}}" class="to_card">
                            <div class="info" style="height: 290px;">
                                <h4>Курс: {{$object->name}}</h4>
                                <span class="description" style = "font-size: 10pt;">
                                    @php
                                        if(strlen($object->name) < 130) {
                                            if (strlen($object->description) > 120){
                                            echo mb_substr($object->description,0,120, 'UTF-8').'...';
                                            }
                                            else
                                                echo $object->description;
                                        }
                                        else echo '...';
                                    @endphp

                                </span>

                            </div>
                        </a>

                        <a href="http://www.{{ $object->source  }}" class = "source"  style="padding-left: 25px">
                            Площадка: {{ $object->source  }}
                        </a>


                        <div class="details">
                        <span class="time" style="font-size: 18pt;"><span class="price">
                            @if($object->price > 0)
                                    {{$object->price}} р.
                                    <a class="btn btn-info pull-right" href="/other_course/card/id{{$object->id}}"><i class="icon-shopping-cart" data-id="{{$object->id}}"></i>Подробно</a>
                                @else
                                    Бесплатно
                                    <a class="btn btn-info pull-right" href="/other_course/card/id{{$object->id}}"><i class="icon-shopping-cart" data-id="{{$object->id}}"></i>Подробно</a>
                            @endif
                            </span>
                        </div>
                    </div>
                </div>
            @elseif(get_class($object) == 'App\OfflineCourse')
                {{--Оффлайн курсы--}}
                <div class="col-md-4 col-sm-6">
                    <div class="block span3 to_course" style="margin-bottom: 20px;">
                        <div class="product">
                            @if(is_null($object->photo_id))
                                <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="/img/course/no_avatar.png">
                            @else
                                <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="{{Storage::url('other_course_avatars/')}}{{ $object->id-1  }}.{{$object->photo->type}}">
                            @endif
                        </div>
                        <a href ="/offline_course/card/id{{$object->id}}" class="to_card">
                            <div class="info" style="height: 290px;">
                                <h4>Курс: {{$object->name}}</h4>
                                <span class="description" style = "font-size: 10pt;">
                                    @php
                                        if(strlen($object->name) < 130) {
                                            if (strlen($object->description) > 120){
                                            echo mb_substr($object->description,0,120, 'UTF-8').'...';
                                            }
                                            else
                                                echo $object->description;
                                        }
                                        else echo '...';
                                    @endphp

                                </span>

                            </div>
                        </a>
                        @if(is_null($object->source))
                            <a href="/" class = "source"  style="padding-left: 25px">
                                Площадка: Clever-e.com
                            </a>
                        @else
                            <a href="http://www.{{ $object->source->name  }}" class = "source"  style="padding-left: 25px">
                                Площадка: {{ $object->source->name  }}
                            </a>
                        @endif


                        <div class="details">
                        <span class="time" style="font-size: 18pt;"><span class="price">
                            @if($object->price > 0)
                                    {{$object->price}} р.
                                    <a class="btn btn-info pull-right" href="/offline_course/card/id{{$object->id}}"><i class="icon-shopping-cart" data-id="{{$object->id}}"></i>Подробно</a>
                                @else
                                    Бесплатно
                                    <a class="btn btn-info pull-right" href="/offline_course/card/id{{$object->id}}"><i class="icon-shopping-cart" data-id="{{$object->id}}"></i>Подробно</a>
                                @endif
                            </span>
                        </div>
                    </div>
                </div>


            @elseif(get_class($object) == 'App\Meeting')
                {{--Встречи--}}
                <div class="col-md-4 col-sm-6">
                    <div class="block span3 to_course" style="margin-bottom: 20px;">
                        <div class="product">
                            @if(is_null($object->photo_id))
                                <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="/img/course/no_avatar.png">
                            @else
                                <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="{{Storage::url('other_course_avatars/')}}{{ $object->id_meeting-1  }}.{{$object->photo->type}}">
                            @endif
                        </div>
                        <a href ="/meeting/card/id{{$object->id_meeting}}" class="to_card">
                            <div class="info" style="height: 290px;">
                                <h4>{{$object->mit_type->name}}: {{$object->name}}</h4>
                                <span class="description" style = "font-size: 10pt;">
                                    @php
                                        if(strlen($object->name) < 130) {
                                            if (strlen($object->description) > 120){
                                            echo mb_substr($object->description,0,120, 'UTF-8').'...';
                                            }
                                            else
                                                echo $object->description;
                                        }
                                        else echo '...';
                                    @endphp

                                </span>

                            </div>
                        </a>
                        @if(is_null($object->source))
                            <a href="/" class = "source"  style="padding-left: 25px">
                                Площадка: Clever-e.com
                            </a>
                        @else
                            <a href="http://www.{{ $object->source->name  }}" class = "source"  style="padding-left: 25px">
                                Площадка: {{ $object->source->name  }}
                            </a>
                        @endif


                        <div class="details">
                        <span class="time" style="font-size: 18pt;"><span class="price">
                            @if($object->price > 0)
                                    {{$object->price}} р.
                                    <a class="btn btn-info pull-right" href="/meeting/card/id{{$object->id_meeting}}"><i class="icon-shopping-cart" data-id="{{$object->id_meeting}}"></i>Подробно</a>
                                @else
                                    Бесплатно
                                    <a class="btn btn-info pull-right" href="/meeting/card/id{{$object->id_meeting}}"><i class="icon-shopping-cart" data-id="{{$object->id_meeting}}"></i>Подробно</a>
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
                {{--Конец встречи--}}
            @endif

        @endforeach


            @if ($courses_var_count <= 15)
                <script>
                    $('.show-more-courses').hide();
                </script>
            @endif

    </div>
@endsection
