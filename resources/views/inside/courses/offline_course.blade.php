@extends('inside.index')

@section('title', 'Редактирование курса')

@section('modal')

@endsection

@section('modal')
@endsection


@section('content')
    <div class = "container  has-editor-api">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 block course-head-container">
                        <div class="col-md-11 col-sm-11 col-xs-11 left-container">
                            <div class="row">
                                <div class="col-md-2 col-sm-3 col-xs-12 course-avatar-container">
                                    @foreach($meetings as $meeting)
                                    <form enctype="multipart/form-data" id="course-avatar-upload-form"
                                          action="/offline_course/edit/id{{$meeting->id}}" method="POST">
                                        <input type="file" name="avatar" id="upload-avatar">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="course_id" value="{{$meeting->id}}">
                                        <a class="course-avatar-upload" href="/offline_course/edit/id{{$meeting->id}}" type="file"
                                           name="avatar">
                                            <span class="helper"></span>
                                            @if (!is_null($meeting->photo_id))
                                                <img class="course-avatar"
                                                     src="{{Storage::url('course_avatars/small/')}}{{ $meeting->id }}.{{$meeting->photo->type}}" style = "max-width: 130px; height: 140px; object-fit: contain;">
                                            @else
                                                <img class="course-avatar" src="/img/course/no_avatar.png">
                                            @endif
                                            <div class="middle">
                                                <div class="text">Обновить</div>
                                            </div>
                                        </a>
                                    </form>
                                </div>
                                <div class="col-md-10 col-sm-9 col-xs-12 ">
                                    <div class="row">
                                        <div class="col-md-12  col-sm-12 col-xs-12 course-name">
                                            {{str_limit($meeting->name, 150, '...')}}
                                        </div>
                                        <div class="col-md-12  col-sm-12 col-xs-12 ">
                                            @if (is_null($meeting->description) or $meeting->description == "")
                                                Описание отсутсвует.
                                            @else
                                                {!!  str_limit($meeting->description, 250, '...')!!}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
                    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>


                    <div class="container has-create-api">
                        <div class="row margin-row">
                            <div class="col-md-12  col-sm-12 col-xs-12 ">
                                <div class="row">

                                </div>
                            </div>

                            <form id ="offline-course-form" role="form" method="POST" action="/offline_course/create/id{{$first_meetings->id}}">
                                {{ csrf_field() }}

                                <div class="col-md-12  col-sm-12 col-xs-12" style="margin-top: 20px">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                Название курса:
                                                @if ($meeting->name == "")
                                                        <input id="course_name" class="form-control" placeholder="Введите название курса" name="course_name" required autofocus type="text">
                                                @else
                                                        <input id="course_name" class="form-control" placeholder="{{$meeting->name}}" name="course_name" required autofocus type="text" value="{{$meeting->name}}">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                Краткое описание:
                                                @if ($meeting->description == "")
                                                    <textarea type="text" rows="3" class="form-control" id="discription" placeholder="Краткое описание" value=""></textarea>
                                                @else
                                                    <textarea type="text" rows="3" class="form-control" id="discription" placeholder="{{$meeting->description}}" value="{{$meeting->description}}"></textarea>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                Полное описание:
                                                @if ($meeting->full_description == "")
                                                        <textarea type="text" rows="3" class="form-control" id="full_description" placeholder="Полное описание" value=""></textarea>
                                                @else
                                                        <textarea type="text" rows="3" class="form-control" id="full_description" placeholder="{{$meeting->full_description}}" value="{{$meeting->full_description}}"></textarea>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                Минимальная цена при продаже курса:
                                                @if (is_null($meeting->price))
                                                    <div class="input-group">
                                                        <input id="course_price" class="form-control" placeholder="Цена" name="price" type="number" min="0">
                                                        <span class="input-group-addon">
                                                             <input class="free" aria-label="..." name="free"  type="checkbox">
                                                                Бесплатный
                                                        </span>
                                                    </div>
                                                @else
                                                    <div class="input-group">
                                                        <input id="course_price" class="form-control" placeholder="{{$meeting->price}}" name="price" type="number" min="0" value="{{$meeting->price}}">
                                                        <span class="input-group-addon">
                                                        <input class="free" aria-label="..." name="free"  type="checkbox">
                                                            Бесплатный
                                                         </span>
                                                    </div>
                                                @endif
                                                <div class="help-block hidden help-error">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="buttons">
                                        <div class="col-md-4 col-sm-4 col-xs-6 add-marginb-30">
                                            <button id="cancel_course" action="/id{{Auth::id()}}" type="button" class="btn-default btn btn-left" ><b>Отмена</b></button>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6 add-marginb-30">
                                            <div class="Delete_meeting_btn">

                                                <div class="modal-overlay closed" id="modal-overlay"></div>

                                                <div class="row">
                                                    <div class="modal closed" id="modal" aria-hidden="true" aria-labelledby="modalTitle" aria-describedby="modalDescription" role="dialog">
                                                        <div class="modal-guts" role="document">
                                                            <h2> Вы действительно хотите удалить курс и всё его содержание?</h2>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <a class="button" href=""> <button class="btn-default btn" type="button">Отмена</button> </a>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <button id="delete_ok" type="button"  class="btn btn-danger btn-right"><b>Удалить</b></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button id="open-button" type="button"  class="btn btn-danger btn-left"><b>Удалить</b></button>
                                            </div>


                                                <script>
                                                    $('body').on('click', '#open-button', function (e) {
                                                        var scrollHeight = Math.max(
                                                                document.body.scrollHeight, document.documentElement.scrollHeight,
                                                                document.body.offsetHeight, document.documentElement.offsetHeight,
                                                                document.body.clientHeight, document.documentElement.clientHeight
                                                        );

                                                        var elem = document.getElementById('modal-overlay');
                                                        elem.style.height = scrollHeight.toString()+'px';


                                                    });

                                                    var modal = document.querySelector("#modal"),
                                                            modalOverlay = document.querySelector("#modal-overlay"),
                                                            //closeButton = document.querySelector("#close-button"),
                                                            openButton = document.querySelector("#open-button");

//                                                    closeButton.addEventListener("click", function() {
//                                                        modal.classList.toggle("closed");
//                                                        modalOverlay.classList.toggle("closed");
//                                                    });

                                                    $(document).ready(function(){
                                                        $(".button-cancel").click(function(){
                                                            $(".modal-overlay").toggleClass("modal-overlay closed"); return false;
                                                        });
                                                    });

                                                    openButton.addEventListener("click", function() {
                                                        modal.classList.toggle("closed");
                                                        modalOverlay.classList.toggle("closed");
                                                    });

                                                </script>
                                                <style>

                                                    .modal {
                                                        /* Не обязательно  block */
                                                        display: block;


                                                        width: 600px;
                                                        max-width: 100%;

                                                        height: 300px;
                                                        max-height: 100%;

                                                        position: fixed;

                                                        z-index: 100;

                                                        left: 50%;
                                                        top: 50%;

                                                        /* Центруем */
                                                        transform: translate(-50%, -50%);



                                                        background: white;
                                                        box-shadow: 0 0 60px 10px rgba(0, 0, 0, 0.9);
                                                    }
                                                    .closed {
                                                        display: none;
                                                    }

                                                    .modal-overlay {
                                                        position: fixed;
                                                        top: 0;
                                                        left: 0;
                                                        width: 100%;
                                                        height: 100%;
                                                        z-index: 50;

                                                        background: rgba(0, 0, 0, 0.6);
                                                    }
                                                    .modal-guts {

                                                        top: 0;
                                                        left: 0;
                                                        width: 100%;

                                                        overflow: auto;
                                                        padding: 20px 50px 20px 20px;
                                                    }
                                                </style>

                                                    </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 add-marginb-30">
                                            <button id="update_meeting" type="submit" class="btn btn-success btn-right"><b>Обновить</b></button>
                                        </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row margin-row" style="margin-top: 5px">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    Длительность курса:
                                                    @if (is_null($meeting->course_length_hours))
                                                        <input id="course_length_hours" class="form-control" placeholder="Длительность в часах"   type="number">
                                                    @else
                                                        <input id="course_length_hours" class="form-control" placeholder="{{$meeting->course_length_hours}}"   type="number" value="{{$meeting->course_length_hours}}">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    Количество занятий:
                                                    @if (is_null($meeting->lessons_count))
                                                        <input id="lessons_count" class="form-control" placeholder="Введите количество занятий"   type="number">
                                                    @else
                                                        <input id="lessons_count" class="form-control" placeholder="{{$meeting->lessons_count}}"   type="number" value="{{$meeting->lessons_count}}">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-row" style="margin-top: 5px">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    Максимальное количество студентов:
                                                    @if (is_null($meeting->max_number_students))
                                                        <input id="max_number_students" class="form-control" placeholder="Максимальное число слушателей"    type="number">
                                                    @else

                                                        <input id="max_number_students" class="form-control" placeholder="{{$meeting->max_number_students}}"    type="number" value="{{$meeting->max_number_students}}">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    Дата начала курса:
                                                    @if ($meeting->start_date == "")
                                                        <div class="input-group" id="datetimepicker1">
                                                            <input type="text" class="form-control" data-format="YYYY-MM-DD HH:MM:SS" placeholder="Введите дату"/>
                                                                        <span class="input-group-addon">
                                                  <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                        </div>
                                                    @else
                                                        <div class="input-group" id="datetimepicker1">
                                                            <input type="text" class="form-control" data-format="YYYY-MM-DD HH:MM:SS" placeholder="{{$meeting->start_date}}" value="{{$meeting->start_date}}"/>
                                                                        <span class="input-group-addon">
                                                  <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-row" style="margin-top: 5px">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    Автор:
                                                    @if ($meeting->author == "")
                                                        <input id="author" class="form-control" placeholder="Введите имя или название организации"    type="text">
                                                    @else
                                                        <input id="author" class="form-control" placeholder="{{$meeting->author}}"    type="text" value="{{$meeting->author}}">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    Ссылка на автора:
                                                    @if ($meeting->author_link == "")
                                                        <input id="author_link" class="form-control" placeholder="Введите ссылку на автора"    type="text">
                                                    @else
                                                        <input id="author_link" class="form-control" placeholder="{{$meeting->author_link}}"    type="text" value="{{$meeting->author_link}}">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row margin-row" style="margin-top: 5px">
                                            @if (is_null($meeting->difficult_id) )
                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                    <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 0px">
                                                        Укажите сложность:
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input class="dif_check" aria-label="..." name="difficult_checkbox"  type="checkbox">
                                                        Не указывать
                                                    </div>
                                                    <div id="select_dif">
                                                        <select id="difficulty" class="selectpicker form-control" name="difficulty_name" placeholder="Сложность" title="Выберите сложность">
                                                            <option data-id="1"> Низкая </option>
                                                            <option data-id="2"> Средняя </option>
                                                            <option data-id="3"> Высокая </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                    <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 0px">
                                                        Укажите сложность:
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input class="dif_check" aria-label="..." name="difficult_checkbox"  type="checkbox">
                                                        Не указывать
                                                    </div>
                                                    <div id="select_dif">
                                                        <select id="difficulty" class="selectpicker form-control" name="difficulty_name">
                                                            <option data-id = "1" @if ($meeting->difficult_id == 1) selected  value="{{$meeting->difficult_id == 1}}" @endif> Низкая </option>
                                                            <option data-id = "2" @if ($meeting->difficult_id == 2) selected  value="{{$meeting->difficult_id == 2}}" @endif> Средняя </option>
                                                            <option data-id = "3" @if ($meeting->difficult_id == 3) selected  value="{{$meeting->difficult_id == 3}}" @endif> Высокая </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    Место проведения:
                                                    @if ($meeting->address == "")
                                                        <input id="address" class="form-control" placeholder="Введите адрес, где будут проходить занятия"    type="text">
                                                    @else
                                                        <input id="address" class="form-control" placeholder="{{$meeting->address}}"    type="text" value="{{$meeting->address}}">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row margin-row" style="margin-top: 5px">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    Необходимые знания:
                                                    @if ($meeting->knowledge_requires == "")
                                                        <textarea type="text" rows="3" class="form-control" id="knowledge_requires" placeholder="Введите компетенции, которыми должен обладать студент" value=""></textarea>
                                                    @else
                                                        <textarea type="text" rows="3" class="form-control" id="knowledge_requires" placeholder="{{$meeting->knowledge_requires}}" value="{{$meeting->knowledge_requires}}"></textarea>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    Полученные навыки:
                                                    @if ($meeting->skills == "")
                                                        <textarea type="text" rows="3" class="form-control" id="skills" placeholder="Укажите список навыков, полученных послу прохождения курса" value=""></textarea>
                                                    @else
                                                        <textarea type="text" rows="3" class="form-control" id="skills" placeholder="{{$meeting->skills}}" value="{{$meeting->skills}}"></textarea>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-row" style="margin-top: 5px">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    Целевая аудитория курса:
                                                    @if ($meeting->audience == "")
                                                        <textarea type="text" rows="3" class="form-control" id="audience" placeholder="Введите описание целевой аудитории" value=""></textarea>
                                                    @else
                                                        <textarea type="text" rows="3" class="form-control" id="audience" placeholder="{{$meeting->audience}}" value="{{$meeting->audience}}"></textarea>
                                                    @endif
                                                </div>
                                            </div>

                                    </div>
                                    <div >





                                            {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}

                                            {{--<div style="position: relative">--}}
                                            {{--<div class="form-group" style="height: 100%; text-align: center">--}}
                                            {{--<br/>--}}
                                            {{--Изменить фото--}}
                                            {{--<input type="file" name="avatar" id="upload-avatar">--}}
                                            {{--<input type="hidden" name="course_id">--}}
                                            {{--<a class="course-avatar-upload" type="file" name="avatar">--}}
                                            {{--<span class="helper"></span>--}}
                                            {{--<img class="course-avatar" src="/img/course/no_avatar.png">--}}
                                            {{--</a>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <script>





                        /* render alert xhr */
                        $(document).ready(function () {
                            jQuery(function(){
                                jQuery('#datetimepicker1').datetimepicker(
                                    {
                                        format: 'YYYY-MM-DD HH:mm'
                                    }
                                );
                            });
                            $("select[name=type_name]").change(function(){
                                var el = $("#offline");
                                var xhr = $("#xhr_course");
                                if($(this).find("option:selected").data('id') == 0)
                                {

                                    el.css('visibility', 'hidden');
                                    el.slideUp();

                                    xhr.css('visibility', 'visible');
                                    xhr.slideToggle();
                                }
                                else{
                                    xhr.removeAttr('visibility');
                                    xhr.css('visibility', 'visible');
                                    xhr.slideUp();

                                    el.removeAttr('visibility');
                                    el.css('visibility', 'visible');
                                    el.slideToggle();
                                }
                            });


                            var el = $("#offline");
                            el.css('visibility', 'hidden');
                            el.slideUp();


                            var xhr = $("#xhr_course");
                            xhr.css('visibility', 'hidden');
                            xhr.slideToggle();

                            $(".xhr_course").click(function (e) {
                                if ($('.xhr_course').is(':checked')) {
                                    $('#xhrModal').modal('show');
                                    $('#xhr_link').parent().parent().parent().removeClass('hidden');
                                }
                                else
                                    $('#xhr_link').parent().parent().parent().addClass('hidden');
                            });

                            /* end render alert xhr */

                            $('.selectpicker').selectpicker({
                                size: 7
                            });

                        });

                    </script>
                </div>
            </div>
        </div>


        <script src="http://code.jquery.com/jquery-2.0.2.min.js"></script>
@endsection





