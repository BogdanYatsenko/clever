@extends('inside.index')

@section('title', 'Подбор курса')

@section('modal')
@endsection


@section('content')


    <!-- Latest compiled and minified JavaScript -->
    <style>

        html {
            overflow-y: scroll;
        }

        .container-fluid{
            padding: 0 !important;
        }
        body {
            background-image: url(https://subtlepatterns.com/patterns/kindajean.png);
        }



    </style>
    <style>

        #logo {
            color: #666;
            width:100%;
        }
        #logo h1 {
            font-size: 60px;
            text-shadow: 1px 2px 3px #999;
            font-family: Roboto, sans-serif;
            font-weight: 700;
            letter-spacing: -1px;
        }
        #logo p{
            padding-bottom: 20px;
        }


        #form-buscar >.form-group >.input-group > .form-control {
            height: 40px;
        }
        #form-buscar >.form-group >.input-group > .input-group-btn > .btn{
            height: 40px;
            font-size: 16px;
            font-weight: 300;


        }
        #form-buscar >.form-group >.input-group > .input-group-btn > .btn .glyphicon{
            margin-right:12px;
        }


        #form-buscar >.form-group >.input-group > .form-control {
            font-size: 16px;
            font-weight: 300;

        }

        #form-buscar >.form-group >.input-group > .form-control:focus {
            border-color: #33A444;
            outline: 0;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 1px rgba(0, 109, 0, 0.8);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 1px rgba(0, 109, 0, 0.8);
        }

        .buttons {
            background: rgba(255, 255, 255, .9) !important;
            opacity: 0;
            -webkit-transition: opacity .9s ease-in;
            -ms-transition: opacity .9s ease-in;
            -moz-transition: opacity .9s ease-in;
            -o-transition: opacity .9s ease-in;
            transition: opacity .9s ease-in;
        }
    </style>



    <div class="container" style = "padding: 0 15px 0 15px;" name ="top">
        <br>
        <div class = "col-md-12" style = "padding: 0">
            <div class="col-md-8  col-sm-12 col-xs-12 ">
                <div class="row">
                    <form role="form" id="form-buscar">
                        <div class="form-group">
                            Ключевые слова или название курса:
                            <input id = "searchword" class="form-control" type="text" placeholder="Поиск среди {{$objects_count}} объектов"

                                   @if (isset($searchword))
                                   @if ($searchword)
                                   value = "{{$searchword}}";
                                    @endif
                                    @endif

                            >
                        </div>
                    </form>


                </div>
            </div>
            <div class="col-md-4  col-sm-12 col-xs-12">
                <div class="form-group">
                    Объекты:
                    <select id = "object-select" class="selectpicker form-control">
                        <option data-id = "0" @if ($objects == 0) selected @endif> Все </option>
                        <option data-id = "1" @if ($objects == 1) selected @endif> Онлайн курсы </option>
                        <option data-id = "2" @if ($objects == 2) selected @endif> Оффлайн курсы </option>
                        <option data-id = "3" @if ($objects == 3) selected @endif> Мероприятия </option>

                    </select>
                </div>
            </div>


            <script>
                $(document).ready(function(){
                    var order_id = 0, source_id = 0,  searchword = 0, price_from = 0, price_to = 'max', only_free = 0, object_type = 0;


                    $('#searchword').on('keyup', function (e) {
                        searchword = $(this).val();
                        if (searchword == '') searchword = 0;
                    });

                    $('#object-select').on('change', function () {
                        $('#search_button').click();
                    });


                    $('#order-select').on('change', function (e) {
                        order_id = $(this).find("option:selected").data('id');
                        $('#search_button').click();
                    });

                    $('#source-select').on('change', function (e) {
                        source_id = $(this).find("option:selected").data('id');
                        $('#search_button').click();
                    });

                    $('#price_from').on('keyup', function (e) {
                        price_from = $(this).val();
                    });

                    $('#price_from').on('keypress', function (e) {
                        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                            return false;
                        }
                    });

                    $('#price_to').on('keyup', function (e) {
                        price_to = $(this).val();
                    });

                    $('#price_to').on('keypress', function (e) {
                        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                            return false;
                        }
                    });

                    $(".only_free").change(function() {

                        if(this.checked) {
                            only_free = 1;
                            $("#price_to").prop('disabled', true);
                            $("#price_from").prop('disabled', true);
                        } else {
                            only_free = 0;
                            $("#price_to").prop('disabled', false);
                            $("#price_from").prop('disabled', false);
                        }

                        $('#search_button').click();
                    });

                    $('#search_button').on('click', function (e) {

                        $('.show-more-courses').show();
                        $('.show-more-courses').data('next-page', 2);

                        order_id = $("#order-select").find("option:selected").data('id');
                        source_id = $("#source-select").find("option:selected").data('id');
                        object_type = $("#object-select").find("option:selected").data('id');

                        if ($('#searchword').val() != '') searchword = $('#searchword').val();
                        if (!$('#price_to').val()) price_to = 'max'; else price_to = $('#price_to').val();
                        if (!$('#price_from').val()) price_from = '0'; else price_from = $('#price_from').val();

                        var way;

                        if (only_free == 0)
                            way = '/catalog/object_type_'+object_type+'/source_'+source_id+'/'+searchword+'/from_'+price_from+'/to_'+price_to+'/order_'+order_id+'/page_1';
                        else
                            way = '/catalog/object_type_'+object_type+'/source_'+source_id+'/'+searchword+'/from_'+0+'/to_0'+'/order_'+order_id+'/page_1';

                        history.pushState(null, null, way);

                        $.get( way, function(  ) {

                        }).done(function(data){
                            console.log(data)
                            if ($("#search_result").html() != data)
                                $("#search_result").html(data.content_courses);
                            if($("#searchword").length ==0)
                                $('#panel').html(data.content)
                        })


                    });

                    $('.show-more-courses').on('click', function(e){

                        var nextPage = $(this).data('next-page');
                        order_id = $("#order-select").find("option:selected").data('id');
                        source_id = $("#source-select").find("option:selected").data('id');
                        object_type = $("#object-select").find("option:selected").data('id');

                        if ($('#searchword').val() != '') searchword = $('#searchword').val();

                        if (!$('#price_to').val()) price_to = 'max'; else price_to = $('#price_to').val();
                        if (!$('#price_from').val()) price_from = '0'; else price_from = $('#price_from').val();

                        var way;

                        if (only_free == 0)
                            way = '/catalog/object_type_'+object_type+'/source_'+source_id+'/'+searchword+'/from_'+price_from+'/to_'+price_to+'/order_'+order_id+'/page_'+nextPage;
                        else
                            way = '/catalog/object_type_'+object_type+'/source_'+source_id+'/'+searchword+'/from_'+0+'/to_0'+'/order_'+order_id+'/page_'+nextPage;

                        history.pushState(null, null, way);

                        $.get( way, function(  ) {

                        }).done(function(data){
                                nextPage++;
                                $("#search_result").empty();
                                $("#search_result").append(data.content_courses);


                            if (nextPage >= {{$objects_count / 15}}){
                                $('.show-more-courses').hide();
                            }

                            $('.show-more-courses').data('next-page', nextPage + 1);
                        })

                    });

                });



            </script>


        </div>
        <div class="row">


            <div class="col-md-2 col-xs-12">
                <div class="form-group">
                    Цена от:
                    <input

                            @php
                                if ($price_from != 0)
                                echo 'value = "'.$price_from.'"';
                            @endphp

                            id = "price_from" class="form-control" placeholder="{{$min_price}}">

                    </select> </div> </div>

            <div class="col-md-2 col-xs-12">
                <div class="form-group">
                    Цена до:

                    <input

                            @php
                                if ($price_to != 'max')
                                    if ($price_to != 0)
                                    echo 'value = "'.$price_to.'"';
                            @endphp

                            id = "price_to" class="form-control"  data-live-search="true"  placeholder="{{$max_price}}">

                    </input> </div> </div>

            <div class="col-md-2 col-xs-12">
                <div class="form-group">
                    Сортировать:
                    <select id = "order-select" class="selectpicker form-control">
                        <option data-id = "0" @if ($order_by == 0) selected @endif> По цене ( по возрастанию ) </option>
                        <option data-id = "1" @if ($order_by == 1) selected @endif> По цене ( по убыванию ) </option>
                        <option data-id = "2" @if ($order_by == 2) selected @endif> Сначала новые </option>
                        <option data-id = "3" @if ($order_by == 3) selected @endif> Сначала старые </option>
                    </select>
                </div>
            </div>

            <div class="col-md-2 col-xs-12">
                <div class="form-group">
                    Источник:
                    <select id = "source-select" class="selectpicker form-control">
                        <option data-id = "0" @if ($selected_source == 0) selected @endif> Все </option>
                        <option data-id = "1" @if ($selected_source == 1) selected @endif> Биржа Clever-e </option>

                        {{ $i = 2, $selected = $selected_source}}
                        @foreach($sources as $source)
                            <option data-id = "{{$i}}" @if ($selected == $i) selected @endif> {{ $source->source }} </option>
                            {{$i = $i +1}}
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="col-md-4 col-xs-12">
                Поиск: <br>
                <div class="input-group">

                    <input id = "search_button" type="button" class="form-control btn btn-success" value="Подобрать курс">

                    <span class="input-group-addon">

        <input class = "only_free" type="checkbox" aria-label="...">
                    Бесплатные
      </span>
                </div><!-- /input-group -->

            </div>
        </div>



        <script>
            $( "#form-buscar" ).submit(function( event ) {
                //  alert( "Handler for .submit() called." );
                $('#search_button').click();
                event.preventDefault();
            });

            $( "#price_from" ).on('keyup', function (e) {
                if (e.keyCode == 13) {
                    $('#search_button').click();
                }
            });

            $( "#price_to" ).on('keyup', function (e) {
                if (e.keyCode == 13) {
                    $('#search_button').click();
                }
            });
        </script>




        <div id = "search_result">
            @yield('content_courses')
        </div>


        <style>
            .show-more-courses{
                width: 100%;
                height: 40px;
                margin-bottom: 20px;
            }
        </style>



        @if ($page <= ($courses_var_count/5) )

            <a href = "#top"><div class = "btn btn-default show-more-courses" data-next-page = '{{$page + 1}}'}>Показать больше объектов</div></a>
            @else
            <a href = "#top"><div class = "btn btn-default show-more-courses" style = "display: none;" data-next-page = '{{$page + 1}}'}>Показать больше объектов</div></a>
            @endif


    </div>

@endsection







