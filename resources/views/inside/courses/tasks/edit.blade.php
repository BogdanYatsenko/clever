@extends('inside.index')

@section('title', 'Редактирование задач')

@section('modal')

@endsection

@section('content')



    <div class = "container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6 add-marginb-30">
                <button action="/constructor/id{{$course_id}}" type="button" class="btn-default btn btn-left goto_edit_course" ><b><-- Вернуться в конструктор</b></button>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 add-marginb-30 delete-cont ">
                <div class = "save_task btn btn-success btn-right" data-link ="/task/edit/cid_{{$course_id}}/id{{$task_id}}/save">Сохранить изменения</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form class = "task">
                <div class="row">
                    <div class="col-md-12">
                        <input id = "task_name" name = "task_name" style = "width: 100%; resize: none;"placeholder="Название задания (заголовок)" value="{{$task->name}}">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        Содержание работы:
                    </div>
                    <div class="col-md-12">
                        <textarea id = "task_text" name = "task_text"  style = "height: 300px; width: 100%; resize: none;" >{{$task->task_text}}</textarea>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-3">
                        Срок сдачи:
                    </div>
                    <div class="col-md-3">
                        <input id = "task_time" name = "task_time" style = "width: 100%; resize: none;" @if($task->date_of != null) value="{{$task->date_of}}" @endif>
                    </div>
                    <div class="col-md-5" style="text-align: right;">
                        Оценка:
                    </div>
                    <div class="col-md-1">
                        <input id = "is_rating" name = "is_rating" style = "width: 100%; resize: none;"type="checkbox" @if($task->is_rating == 1)checked @endif>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-10">
                                Принимать просроченое:
                            </div>
                            <div class="col-md-1">
                                <input id = "task_off_time" name = "task_off_time"  style = "width: 100%; resize: none;"type="checkbox" @if($task->is_date_of == 1)checked @endif>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                Материалы далее доступны:
                            </div>
                            <div class="col-md-1">
                                <input id = "task_access" name = "task_access" style = "width: 100%; resize: none;"type="checkbox" @if($task->is_material_look == 1)checked @endif>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <textarea id = "task_comment" name = "task_comment" style = "height: 100px; width: 100%; resize: none;" placeholder="Комментарии" >@if($task->comment != ''){{$task->comment}} @endif</textarea>
                    </div>
                </div>
                </form>
            </div>

        </div>

    </div>

    <script>
        $(document).ready(function () {

            CKEDITOR.replace('task_text',{customConfig: '/lib/ckeditor/light.js'});
            CKEDITOR.config.resize_enabled = false;
            CKEDITOR.config.height = window.innerHeight - 216 * 2.1;
            CKEDITOR.config.width = '100%';
        });

    </script>

@endsection