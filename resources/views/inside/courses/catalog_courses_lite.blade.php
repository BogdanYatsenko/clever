
<div class="container">

        @forelse($courses as $course)

            <div class="col-md-4 col-sm-6">

                <div class="block span3 to_course" style="margin-bottom: 20px;">

                    <div class="product">


                        @if (!is_null($course->photo_id))
                            <div class = "user-no-avatar-sm" style = "height: 190px; object-fit: contain; background-color: #fff;">
                                <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="{{Storage::url('course_avatars/')}}{{ $course->id  }}.{{$course->photo->type}}">
                            </div>
                        @else
                            <div class = "user-no-avatar-sm" style = "height: 190px; object-fit: contain; background-color: #fff;">
                                <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="/img/course/no_avatar.png">
                            </div>
                        @endif


                        <!--
                        <div class="buttons" style="cursor: pointer;">

                            Автор курса: <a href="/id{{ $course->user->id}}">{{$course->user->first}}</a>


                            Дата последнего обновления:
                            {{$course->updated_at}}


                        </div>-->

                    </div>
                    <a href ="/course/card/id{{$course->id}}" class="to_card">
                    <div class="info" style="height: 290px;">
                        <h4>{{$course->name}}</h4>
                        <span class="description" style = "font-size: 10pt;">
                             @php
                              if(strlen($course->name) < 130) {
                                if (strlen($course->discription) > 120){
                                 echo mb_substr($course->discription,0,120, 'UTF-8').'...';
                                 } else echo $course->discription;
                             } else echo '...';
                             @endphp

                         </span>

                    </div>

                    </a>
                    <div class="details">
                        <span class="time" style="font-size: 18pt;"><span class="price">
                              @if ($course->price > 0)
                                    {{$course->price}} р.
                                    </span></span>
                        @if ($course->user_id != Auth::id())
                            <span class="rating pull-right buy-course">
                                        <a class="btn btn-info pull-right" href="#"><i class="icon-shopping-cart" data-id="{{$course->id}}"></i>Купить</a>
                                    </span>
                        @endif

                        @else
                            Бесплатно
                            @if ($course->user_id != Auth::id())
                                <span class="rating pull-right buy-course">
                                        <a class="btn btn-info pull-right" href="/course/buy/id{{$course->id}}" data-id="{{$course->id}}"><i class="icon-shopping-cart"></i>Изучать</a>
                                    </span>
                                @endif
                                @endif
                                </span></span>
                    </div>
                </div>
            </div>
        @empty
            Курсов нет

        @endforelse

            @if ($courses_var_count <= 15)
                <script>
                    $('.show-more-courses').hide();
                </script>
            @endif

    </div>
