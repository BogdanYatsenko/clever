@extends('inside.index')

@section('title', 'Предпосмотр материалов')

@section('modal')

@endsection

@section('content')

    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({
        tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
      });
    </script>

    <div class="container has-preview-api">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">
                        <button action="/constructor/id{{$course_id}}" type="button" class="btn-default btn btn-left goto_edit_course" ><b><-- Вернуться в конструктор</b></button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 block book-head-container">
                        <div class="col-md-9 col-sm-9 col-xs-12 left-container">
                            <div class="row">
                                <div class="col-md-10 col-sm-9 col-xs-12 ">
                                    <div class="row">
                                        <div class="col-md-12  col-sm-12 col-xs-12 course-name">
                                            Книга: {{str_limit($book->name, 150, '...')}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 right-container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-3  btn-container editor-container">
                                    <a href="/book/edit/cid_{{$course_id}}/id{{$book->id}}">
                                        <div class="edit_bar_element_edit pointer" data-toggle="popover"
                                             data-trigger="hover" data-placement="bottom"
                                             data-content="Включить радактирование">
                                            <span class=" 	glyphicon glyphicon-text-size"></span>
                                        </div>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row margin-row">

            <div class="col-md-3 col-sm-12 col-xs-12 menu-lesson block">
                <div class="row">
                    <nav class="navbar navbar-default">
                        <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#lesson_menu">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="lesson_menu">
                            <div class="row">
                                <div class="lesson_menu-container" data-course_id="{{$book->id}}">
                                    <div class="lesson_menu_text ">
                                        Содержание курса
                                    </div>
                                </div>
                            </div>
                            <div class="accordion">
                                @foreach ($book->lessons as $lesson)
                                    @if ($lesson->parent_id == 0)
                                        <div class="lesson-group">
                                            <div class="lesson-container-preview" data-priority="{{$lesson->priority}}" data-has="{{$has_sublesson = 0 }}">
                                                @foreach ($book->lessons as $lesson_sub)
                                                    @if ($lesson_sub->parent_id == $lesson->id)
                                                        <div data-has="{{$has_sublesson = 1}}"></div>
                                                        @break
                                                    @endif
                                                @endforeach
                                                @if ($has_sublesson == 0)
                                                    <div class="col-md-1 col-sm-1 col-xs-1 nopadding lesson-icon">
                                                    </div>
                                                @else
                                                    <div class="col-md-1 col-sm-1 col-xs-1 nopadding lesson-sub-icon">
                                                    </div>
                                                @endif
                                                <a href="#">
                                                    <div class="col-md-11 col-sm-11  col-xs-11 nopadding">
                                                        <h3 data-lesson_id="{{$lesson->id}}" data-target="#{{$lesson->id}}"
                                                            data-title="{{$lesson->name}}" data-priority="{{$lesson->priority}}">
                                                            {{$lesson->name}}
                                                        </h3>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="accordion-sub">
                                                @foreach ($book->lessons as $lesson_sub)
                                                    @if ($lesson_sub->parent_id == $lesson->id)
                                                        <div class="lesson-container-preview" data-priority="{{$lesson_sub->priority}}">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 nopadding ">
                                                            </div>
                                                            <div class="col-md-1 col-sm-1 col-xs-1 nopadding lesson-icon">
                                                            </div>
                                                            <a href="#">
                                                                <div class="col-md-10 col-sm-10 col-xs-10 nopadding">
                                                                    <h3 data-lesson_id="{{$lesson_sub->id}}" data-target="#{{$lesson_sub->id}}"
                                                                        data-title="{{$lesson_sub->name}}" data-priority="{{$lesson_sub->priority}}">
                                                                        {{$lesson_sub->name}}
                                                                    </h3>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>

                        </div>
                    </nav>
                </div>
            </div>

            <div class="col-md-9 col-sm-12 col-xs-12 lesson-content-preview" data-user_id="{{Auth::id()}}">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 block">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 ">
                                <div class="input-name input-name-preview" data-lesson_id = "">
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                            <div class="textarea inputValue" autofocus wrap></div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script>
        jQuery(document).ready(function () {
            $('.lesson-content-preview').addClass('hide_block');
            $('[data-toggle="popover"]').popover({
                container: '#panel'
            });

//            $("body").on("click", ".lesson-container-preview h3", function (e) {
//                var elem = $(this);
//                var lesson_id = elem.data("lesson_id");
//                var course_id = $('.lesson_menu-container').data('course_id');
//                var _token = $("input[name='_token']").val();
//                $.post("/course/preview/id" + course_id + "/getlesson", {
//                    _token: _token,
//                    lesson_id: lesson_id
//                }, function (response) {
//                    $('.lesson-content').removeClass('hide_block');
//                    $('.lesson-content .input-name-preview').empty();
//                    $('.textarea').empty();
//                    $('.lesson-content .input-name-preview').append(response.name);
//                    $('.lesson-content .input-name-preview').data('lesson_id', lesson_id);
//                    console.log(response.name);
//                    $('.textarea').append(response.text);
//                });
//                e.preventDefault();
//            });

            if(window.innerWidth >990) {
                $('.menu-lesson').css('height', window.innerHeight - 174 * 1.32);
                $('.menu-lesson').css('max-height', window.innerHeight - 174 * 1.32);
                $('.accordion').css('max-height', window.innerHeight - 184 * 1.48);
                $('.textarea').css('height', window.innerHeight - 194 * 1.505);
                $('.textarea').css('max-height', window.innerHeight - 194 * 1.505);
                $('.textarea').css('overflow', 'auto');
            }



        });
    </script>

@endsection







