@extends('inside.index')

@section('title', 'Редактирование материалов')

@section('modal')
    <!-- delete lesson -->
    <div class="modal fade" id="deleteConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Удалить раздел?</h4>
                </div>
                <div class="modal-body">
                    <h5>Удалить раздел: </h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-right" id="deleteNo" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove-circle"></span> Отмена
                    </button>
                    <button type="button" class="btn btn-primary btn-left" id="deleteOk" data-remove_id="" data-lesson_id="" data-dismiss="modal">
                        <span class="glyphicon glyphicon-trash"></span> Удалить
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end delete lesson -->

    <!-- delete sublesson -->
    <div class="modal fade" id="deleteConfirmationSub" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Удалить главу?</h4>
                </div>
                <div class="modal-body">
                    <h5>Удалить главу: </h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-right" id="deleteNo" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove-circle"></span> Отмена
                    </button>
                    <button type="button" class="btn btn-primary btn-left" id="deleteSubOk" data-remove_id="" data-lesson_id="" data-dismiss="modal">
                        <span class="glyphicon glyphicon-trash"></span> Удалить
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end delete sublesson -->

    <!-- delete course -->
    <div class="modal fade" id="deleteConfirmationCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Удалить курс?</h4>
                </div>
                <div class="modal-body">
                    <h5>Удалить курс: </h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-right" id="deleteNo" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove-circle"></span> Отмена
                    </button>
                    <button type="button" class="btn btn-primary btn-left" id="deleteBookOk" data-book_id="" data-course_id=""  data-dismiss="modal">
                        <span class="glyphicon glyphicon-trash"></span> Удалить
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end delete course -->

@endsection

@section('content')

    <div class="container has-editor-api">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6 add-marginb-30">
                        <button action="/constructor/id{{$course_id}}" type="button" class="btn-default btn btn-left goto_edit_course" ><b><-- Вернуться в конструктор</b></button>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 add-marginb-30 delete-cont ">
                        <button data-toggle="modal" data-target="#deleteConfirmationCourse" type="button" class="btn-danger btn btn-right delete_book" data-course_id="{{$course_id}}" data-remove_id="{{$book->id}}" data-name="{{$book->name}}"><b>Удалить книгу</b></button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 block book-head-container">
                        <div class="col-md-11 col-sm-11 col-xs-12 left-container">
                            <div class="row">
                                <div class="col-md-10 col-sm-9 col-xs-12 ">
                                    <div class="row">
                                        <div class="col-md-12  col-sm-12 col-xs-12 course-name book-name">
                                            Книга: {{str_limit($book->name, 150, '...')}}
                                        </div>
                                        <div class="col-md-12  col-sm-12 col-xs-12 course-name book-name-input hidden">
                                            <div class="row">
                                                <div class="col-md-1  col-sm-1 col-xs-12">
                                                    Книга:
                                                </div>
                                                <div class="col-md-9  col-sm-9 col-xs-12">
                                                    <input class="" title="Название раздела" value="{{$book->name}}"placeholder="Укажите название раздела" type="text">
                                                </div>
                                                <div class="col-md-2  col-sm-2 col-xs-12 delete-cont">
                                                    <button action="/book/edit/id{{$book->id}}/rename" type="button" class="btn-default btn rename-book-btn" data-id="{{Auth::id()}}" ><b>Сохранить</b></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-12 right-container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-3 btn-container rename-book close-book">

                                    <div class="edit_bar_element_edit pointer" data-toggle="popover"
                                         data-trigger="hover" data-placement="bottom"
                                         data-content="Изменить название">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </div>

                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-3 btn-container close-container close-book">
                                    <a href="/book/preview/cid_{{$course_id}}/id{{$book->id}}">
                                        <div class="edit_bar_element_close pointer" data-toggle="popover"
                                             data-trigger="hover" data-placement="bottom"
                                             data-content="Отключить радактирование">
                                            <span class=" 	glyphicon glyphicon-text-size"></span>

                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row margin-row">

            <div class="col-md-3 col-sm-12 col-xs-12 menu-lesson block">
                <div class="row">
                    <nav class="navbar navbar-default">
                        <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#lesson_menu">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="lesson_menu">
                            <div class="row">
                                <a href="#">
                                    <div class="add-lesson-container" data-book_id="{{$book->id}}">
                                        <div class="add_lesson_btn pointer">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </div>
                                        <div class="add_lesson_text pointer">
                                            Добавить раздел
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="accordion">
                                @foreach ($book->lessons as $lesson)
                                    @if ($lesson->parent_id == 0)
                                        <div class="lesson-group">
                                            <div class="lesson-container" data-priority="{{$lesson->priority}}" data-has="{{$has_sublesson = 0 }}">

                                                @foreach ($book->lessons as $lesson_sub)
                                                    @if ($lesson_sub->parent_id == $lesson->id)
                                                        <div data-has="{{$has_sublesson = 1}}"></div>
                                                        @break
                                                    @endif
                                                @endforeach
                                                @if ($has_sublesson == 0)
                                                    <div class="col-md-1 col-sm-1 col-xs-1 nopadding lesson-icon">
                                                    </div>
                                                @else
                                                    <div class="col-md-1 col-sm-1 col-xs-1 nopadding lesson-sub-icon">
                                                    </div>
                                                @endif
                                                <a href="#">
                                                    <div class="col-md-7 col-sm-7  col-xs-7 nopadding">
                                                        <h3 data-lesson_id="{{$lesson->id}}" data-target="#{{$lesson->id}}"
                                                            data-title="{{$lesson->name}}" data-priority="{{$lesson->priority}}">
                                                            {{$lesson->name}}
                                                        </h3>
                                                    </div>
                                                </a>
                                                <a href="#" data-toggle="modal" data-target="#deleteConfirmation" data-remove_id="{{$lesson->priority}}">
                                                    <div class="col-md-1 col-sm-1 col-xs-1  del-lesson nopadding " data-toggle="popover"
                                                         data-trigger="hover" data-placement="bottom"
                                                         data-content="Удалить раздел"  >
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </div>
                                                </a>
                                                <!--
                                                <a href="#">
                                                    <div class="col-md-1 col-sm-1 col-xs-1 add-new-test  nopadding" data-toggle="popover"
                                                         data-trigger="hover" data-placement="bottom"
                                                         data-content="Добавить тест">
                                                        <span class="glyphicon 	glyphicon glyphicon-check"></span>
                                                    </div>
                                                </a>
                                                -->
                                                <a href="#">
                                                    <div class="col-md-1 col-sm-1 col-xs-1 add-new-sublesson  nopadding" data-toggle="popover"
                                                         data-trigger="hover" data-placement="bottom"
                                                         data-content="Добавить главу">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </div>
                                                </a>

                                            </div>
                                            <div class="accordion-sub">
                                                @foreach ($book->lessons as $lesson_sub)
                                                    @if ($lesson_sub->parent_id == $lesson->id)
                                                        <div class="lesson-container" data-priority="{{$lesson_sub->priority}}">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 nopadding ">
                                                            </div>
                                                            <div class="col-md-1 col-sm-1 col-xs-1 nopadding lesson-icon">
                                                            </div>
                                                            <a href="#">
                                                                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                                                    <h3 data-lesson_id="{{$lesson_sub->id}}" data-target="#{{$lesson_sub->id}}"
                                                                        data-title="{{$lesson_sub->name}}" data-priority="{{$lesson_sub->priority}}">
                                                                        {{$lesson_sub->name}}
                                                                    </h3>
                                                                </div>
                                                            </a>
                                                            <a href="#" data-toggle="modal" data-target="#deleteConfirmationSub" data-remove_id_sub="{{$lesson_sub->priority}}">
                                                                <div class="col-md-1 col-sm-1 col-xs-1  del-sublesson nopadding " data-toggle="popover"
                                                                     data-trigger="hover" data-placement="bottom"
                                                                     data-content="Удалить главу">
                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                </div>
                                                            </a>
                                                            <!--
                                                            <a href="#">
                                                                <div class="col-md-1 col-sm-1 col-xs-1 add-new-subtest  nopadding" data-toggle="popover"
                                                                     data-trigger="hover" data-placement="bottom"
                                                                     data-content="Добавить тест">
                                                                    <span class="glyphicon 	glyphicon glyphicon-check"></span>
                                                                </div>
                                                            </a>
                                                            -->
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>

                        </div>
                    </nav>
                </div>
            </div>

            <div class="col-md-9 col-sm-12 col-xs-12 lesson-content" data-user_id="{{Auth::id()}}">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 block">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 ">
                                <input class="input-name" title="Название раздела"
                                       placeholder="Укажите название раздела" type="text" data-lesson_id = "">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 ">
                                <textarea class="textarea inputValue" autofocus wrap id="mytextarea"> </textarea>
                            </div>
                            <div class="save-triggerhidden" data-save-trigger="0"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {

            $('.lesson-content').addClass('hide_block');
            $('[data-toggle="popover"]').popover({
                container: '#panel'
            });
            upload_course_avatar();

            CKEDITOR.replace('mytextarea');
            CKEDITOR.config.resize_enabled = false;
            CKEDITOR.config.height = window.innerHeight-216*2.045;
            CKEDITOR.config.width = '100%';

            /* editor logick */
            $('.input-name').on('input keyup', function (e) {
                CKEDITOR.instances.mytextarea.getCommand('savecourse').setState(CKEDITOR.TRISTATE_OFF);
                $("#page-content-wrapper").data("lesson_name", $(this).val());
            });
            CKEDITOR.instances.mytextarea.on('change', function () {
                if ($('.save-trigger').data("save-trigger") != 0) {
                    CKEDITOR.instances.mytextarea.getCommand('savecourse').setState(CKEDITOR.TRISTATE_OFF);
                } else if ($('.save-trigger').data("save-trigger") == 0) {
                    $('.save-trigger').data("save-trigger", 1);
                }
            });


            /* end editor logick */

            if(window.innerWidth >990) {
                $('.menu-lesson').css('height', window.innerHeight - 204 * 1.32);
                $('.menu-lesson').css('max-height', window.innerHeight - 204 * 1.32);
                $('.accordion').css('max-height', window.innerHeight - 214 * 1.48);
            }


            $(".edit_bar_element_moderate").on('click', function(){
                $.get( "/course/edit/id{{$book->id}}/moderate", function(  ) {

                }).done(function(data){
                    if (data == 0) alert ("Курс отправлен на модерацию");
                    if (data == 2) alert ("Курс ожидает модерации");
                    if (data == 1) alert ("Курс уже модерирован");
                });
            });
        });
    </script>

@endsection








