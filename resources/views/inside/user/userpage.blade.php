@extends('inside.index')

@section('title', ''.$user->first.'  '.$user->third.'  '.$user->second)


@section('modal')
    <div class="modal fade" id="addPhone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Привязать телефон</h4>
                </div>
                <div class="modal-body">
                    <div class="input-group" id="new_phone">
                    Номер телефона:
                        <input placeholder="+7 (___) ___-__-__"  id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" >

                    </div>
                    <div class="input-group" id="confirm_phone" >
                        Код подтверждения
                        <input placeholder="хххх"  id="password" type="text" class="form-control" name="password" >

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-right" id="getPhoneNo" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove-circle"></span> Отмена
                    </button>
                    <button type="button" class="btn btn-primary btn-left" id="addConfirm" data-course_id="">
                        <span class="glyphicon glyphicon-plus"></span> Привязать
                    </button>
                    <button type="button" class="btn btn-primary btn-left" id="addPhoneOk" data-course_id="">
                        <span class="glyphicon glyphicon-ok"></span> Подтвердить
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
        $("#confirm_phone").hide();
        $("#addPhoneOk").hide();
        var maxLen = 17;
        var lastLogin ="";
        $(document).on("keyup", "#email", function(e) {
            var login = $('#email').val();
            var key = e.keyCode || e.charCode;
            var check = login.replace(/[0-9]/g, '').replace(/[\(\)\-\+]/g, "").replace(/[_\s]/g, '');
            if (login.length > 18 || check != ""){
                $("#email").val(lastLogin);
            }else {
                console.log(check == "");
                console.log(login.length);
                if (key == 8 || key == 46) {
                    if (check == "" && (login.length == 12 || login.length == 15))
                        login = login.substr(0, login.length - 1);
                    else if (check == "" && login.length == 8)
                        login = login.substr(0, login.length - 2);
                    else if (login.length <= 4 && check == "")
                        login = "";
                    maxLen = login.length;
                } else if (check == "" && login.length < 17) {
                    if (login.length == 1) {
                        var temp = "+7 (";
                        if (login[0] == '7' || login[0] == '+' || login[0] == '8') {
                            login = temp;
                        } else {
                            login = temp.concat(login);
                        }
                    } else if (login.length == 7) {
                        var temp = ") ";
                        login = login.concat(temp);
                    } else if (login.length == 12 || login.length == 15) {
                        var temp = "-";
                        login = login.concat(temp);
                    }
                } else {

                }
                $("#email").val(login);
                lastLogin = login;
                if (login.length > maxLen)
                    maxLen = login.length;
            }
        });
            $(document).on('click','#addConfirm', function () {
                $("#addConfirm").prop( "disabled", true );
                $('#email').prop( "disabled", true );
                var login = $('#email').val();
                var check = login.replace(/[0-9]/g, '').replace(/[\(\)\-\+]/g, "").replace(/[_\s]/g, '');
                login = login.replace(/\D/g, '');
                if (check != "") {
                    alert("Не верный формат телефона");
                    $("#addConfirm").prop("disabled", false);
                    $('#email').prop("disabled", false);
                } else {
                    $("#new_phone").hide();
                    $("#addConfirm").hide();
                    $("#confirm_phone").show();
                    $("#addPhoneOk").show();
                    $.post("id"+{{Auth::id()}}+"/getconfirm", {email: login})
                        .done(function (response) {
                        });
                }
            });
            $(document).on('click','#addPhoneOk', function () {
                var password = $('#password').val();
                var login = $('#email').val();
                login = login.replace(/\D/g, '');
                var check = password.replace(/[0-9]/g, '').replace(/[\(\)\-\+]/g, "").replace(/[_\s]/g, '');
                if (password.length != 4 || check != "") {
                    alert("Код подтвержедния состоит из 4-х цифр");
                } else {
                    $.post("id"+{{Auth::id()}}+"/applyconfirm", {email: login, password: password})
                        .done(function (response) {
                            if(response.success) {
                                alert("Номер успешно привязан!");
                                $('#addPhone').modal('toggle');
                            }else if (response == 0){
                                $('#password').val("Неверный код подтверждения");
                            }
                        });
                }
            });
        });

    </script>
@endsection


@section('content')
    <div class="container">
        <div class="row margin-row ">
            <div class="col-md-3 col-sm-4 ">
                <div class="row bordered">


                    <div class="col-md-12 col-sm-12 col-xs-12 user-photo">

                        @if (!Auth::guest())
                            @if ($user->id == Auth::id())
                                <form enctype="multipart/form-data" id="user-avatar-upload-form" action="/id{{$user->id}}" method="POST">
                                    <input type="file" name="avatar" id="upload-avatar">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @endif
                        @endif

                        <span class="helper"></span>

                        @if (!is_null($user->photo_id))
                            @foreach($photos as $photo)
                                @if($photo->id == $user->photo_id)
                                <img class="user-avatar" src="{{Storage::url('avatars/')}}{{$user->id}}.{{$photo->type}}">
                                @endif
                            @endforeach
                        @else
                            <img class="user-avatar" src="/img/no_avatar.png">
                        @endif


                        @if (!Auth::guest())
                            @if ($user->id == Auth::id())
                                    <a class="user-avatar-upload" href="/id{{$user->id}}" type="file" name="avatar">
                                        <div class="middle">
                                            <div class="text">Обновить</div>
                                        </div>
                                    </a>
                                </form>
                            @endif
                        @endif

                    </div>
                    @if (!Auth::guest())
                        @if ($user->id == Auth::id() )
                            {{--&& $user->phone == ""--}}
                            <div class="col-md-12 col-sm-12 col-xs-12 box">
                                <a href="#" data-toggle="modal" data-target="#addPhone"  class="btn-success btn btn-left btn-block"><b>Привязять телефон</b></a>
                            </div>
                            @if(isset($is_teacher))
                                @if(count($is_teacher) == 0)

                                    <div class="col-md-12 col-sm-12 col-xs-12 box">
                                        <button id="becameTeacher" data-toggle="modal" class="btn-success btn btn-left btn-block">Стать преподавателем</button>
                                    </div>
                                @else
                                    @foreach ($is_teacher as $teacher)
                                        @if($teacher->is_moderated == 0)
                                            <div class="col-md-12 col-sm-12 col-xs-12 box" style="text-align: center">
                                                <b>Статус заявки: на рассмотрении</b>
                                            </div>
                                        @elseif($teacher->is_moderated == 1)
                                            <div class="col-md-12 col-sm-12 col-xs-12 box" style="text-align: center">
                                                <b>Вы преподаватель. Ваш id: {{$teacher->id_teacher}}</b>

                                            </div>

                                        @elseif($teacher->is_moderated == 2)
                                            <div class="col-md-12 col-sm-12 col-xs-12 box" style="text-align: center">
                                                <b>Модерация не пройдена. Обратитесь в поддержку или </b>
                                                <button id="becameTeacher" data-toggle="modal" class="btn-success btn btn-left btn-block">пройдите модерацию снова</button>
                                            </div>

                                        @endif
                                    @endforeach
                                @endif
                                @else
                                <div class="col-md-12 col-sm-12 col-xs-12 box">
                                    <button id="becameTeacher" data-toggle="modal" class="btn-success btn btn-left btn-block">Стать преподавателем</button>
                                </div>
                            @endif



                        @endif
                    @endif


                    <script type="text/javascript">
                        $("#becameTeacher").on('click', function () {
                            var url = document.location.href;
                            var ind_of_id = url.indexOf("id");
                            var id = url.slice(ind_of_id + 2, url.length);

                            window.location.href = "/teacher/became_"+id;
                        })


                    </script>


                @if (!Auth::guest())
                        @if ($user->id != Auth::id())
                            <div class="col-md-12 col-sm-12 col-xs-12  box  follow-btn" data-id="{{$user->id}}" data-auth="{{Auth::id()}}"data-name="{{Auth::user()->first}}" data-img="{{Auth::user()->photo_id}}">
                                <div class="col-md-12 ">
                                    @if($subscribes->count() > 0)
                                        @foreach ( $subscribes as $subscribe)
                                            @if($subscribe->id == Auth::id())
                                                <button action="/id{{$user->id}}/unfollow" type="button" class="btn-default btn btn-left btn-block unfollow"><b>Отписаться</b></button>
                                                @break
                                            @endif
                                            @if($subscribe == end($subscribes)  )
                                                <button action="/id{{$user->id}}/follow" type="button" class="btn-success btn btn-left btn-block follow" ><b>Подписаться</b></button>
                                            @endif
                                        @endforeach
                                    @else
                                        <button action="/id{{$user->id}}/follow" type="button" class="btn-success btn btn-left btn-block follow"><b>Подписаться</b></button>
                                    @endif
                                </div>
                            </div>
                        @endif
                    @endif

                    @if ($user->subscribe_to->count() > 0)
                        <div class="col-md-12 col-sm-12 col-xs-12  box followbox">
                            <div class="col-md-12">
                                <h2>Подписки
                                    <span class="fcount">{{$user->subscribe_to->count()}}</span>
                                </h2>
                            </div>
                            <div class="col-md-12">

                                <ul>
                                    @foreach ( $subscribe_tos as $subscribe)
                                        <li>
                                            <a href="/id{{ $subscribe->id }}">
                                                @if (!is_null($subscribe->photo_id))
                                                    <div class="user-avatar-sm">
                                                        @foreach($photos as $photo)@if($photo->id == $subscribe->photo_id)
                                                        <img class="user-avatar-pic" src="{{Storage::url('avatars/small/')}}{{ $subscribe->id  }}.{{$photo->type}}">
                                                        @endif @endforeach
                                                    </div>
                                                @else
                                                    <div class="user-no-avatar-sm">
                                                        <img class="user-avatar-pic" src="/img/no_avatar_small.png">
                                                    </div>
                                                @endif
                                                <p>{{$subscribe->first }}</p>
                                            </a>
                                        </li>

                                    @endforeach

                                </ul>

                            </div>
                        </div>
                    @endif
                    @if ($user->subscribe->count() > 0)
                        <div class="col-md-12 col-sm-12 col-xs-12  box followbox">
                            <div class="col-md-12">
                                <h2>Подписчики
                                    <span class="fcount">{{$user->subscribe->count()}}</span>
                                </h2>
                            </div>
                            <div class="col-md-12">
                                <ul>
                                    @foreach ( $subscribes as $subscribe)
                                        <li>
                                            <a href="/id{{ $subscribe->id }}">
                                                @if (!is_null($subscribe->photo_id))
                                                    <div class = "user-avatar-sm">
                                                        @foreach($photos as $photo)@if($photo->id == $subscribe->photo_id)
                                                        <img class="user-avatar-pic" src="{{Storage::url('avatars/small/')}}{{ $subscribe->id  }}.{{$photo->type}}">
                                                            @endif @endforeach
                                                    </div>
                                                @else
                                                    <div class = "user-no-avatar-sm">
                                                        <img class="user-avatar-pic" src="/img/no_avatar_small.png">
                                                    </div>
                                                @endif
                                                <p>{{$subscribe->first }}</p>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-md-9 col-sm-8 user-info-container">
                <div class="row">
                    <div class="col-md-12  col-sm-12 col-xs-12 ">
                        <div class="col-md-12 col-sm-12 col-xs-12 user-name" id="user_name">
                            {{$user->first}} {{$user->third}} {{$user->second}}
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 big-box">
                            {{--<div class="col-md-6 col-sm-12 col-xs-12">--}}
                                {{--<div class=" row">--}}
                                    {{--<div class="col-md-6 col-sm-4 col-xs-6">--}}
                                        {{--Место учебы--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6 col-sm-8 col-xs-6">--}}
                                        {{--@if ($user->edu_place_id != NULL)--}}
                                            {{--{{$user->edu_place_id}}--}}
                                        {{--@else--}}
                                            {{--Не указано--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class=" row">--}}
                                    {{--<div class="col-md-6 col-sm-4 col-xs-6">--}}
                                        {{--Специальность--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6 col-sm-8 col-xs-6">--}}
                                        {{--@if ($user->edu_grade_id != NULL)--}}
                                            {{--{{$user->edu_grade_id}}--}}
                                        {{--@else--}}
                                            {{--Не указана--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class=" row">
                                    <div class="col-md-6 col-sm-4 col-xs-6">
                                        Место работы
                                    </div>
                                    <div class="col-md-6 col-sm-8 col-xs-6">
                                        @if ($user->work_place_id != NULL)
                                            {{$user->work_place_id}}
                                        @else
                                            Не указано
                                        @endif
                                    </div>
                                </div>
                                <div class=" row">
                                    <div class="col-md-6 col-sm-4 col-xs-6">
                                        Должность
                                    </div>
                                    <div class="col-md-6 col-sm-8 col-xs-6">
                                        @if ($user->job_id != NULL)
                                            {{$user->job_id}}
                                        @else
                                            Не указана
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="collapse-user-info" data-toggle="collapse" data-target="#full-info">
                                    Развернуть
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div id="full-info" class="collapse full-info-padding-10">
                                    <div class=" row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Телефон
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->phone != NULL)
                                                        {{$user->phone}}
                                                    @else
                                                        Нет номера
                                                    @endif
                                                </div>
                                            </div>
                                            @if (!Auth::guest())
                                                @if ($user->id == Auth::id())
                                                    <div class=" row">
                                                        <div class="col-md-6 col-sm-4 col-xs-6">
                                                            E-mail
                                                        </div>
                                                        <div class="col-md-6 col-sm-8 col-xs-6" id="email">
                                                            {{$user->login}}
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Skype
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->skype != NULL)
                                                        {{$user->skype}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    VK
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->vk != NULL)
                                                        {{$user->vk}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Facebook
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->fb != NULL)
                                                        {{$user->fb}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Сайт
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->site != NULL)
                                                        {{$user->site}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" row">
                                        <div class="col-md-6  col-sm-12 full-info-padding-10">
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Дата рождения
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->birthday != NULL)
                                                        {{$user->birthday}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Пол
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->sex != NULL)
                                                        @if ($user->sex == 1)
                                                            Мужской
                                                        @else
                                                            Женский
                                                        @endif
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Место проживания
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->country_id == NULL)
                                                        Не указан
                                                    @elseif ($user->country_id != NULL and $user->region_id == NULL)
                                                        {{$user->country->name}}
                                                    @elseif ($user->country_id != NULL and $user->region_id != NULL and $user->city_id == NULL)
                                                        {{$user->region->name}}
                                                    @else
                                                        {{$user->city->name}}
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                        </div>
                                    </div>
                                    <!--
                                    <div class=" row">
                                        <div class="col-md-12 full-info-padding-10">
                                            <div class=" row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    О себе
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-12  col-sm-12 col-xs-12">
                                                    13
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-12 col-sm-12 col-xs-12 big-box user-course">
                            <ul class="nav nav-pills">
                                <li class="active student">
                                    <a data-toggle="tab" href="#stud">Обучаюсь</a>
                                </li>

                                @if(count($company_admin) != 0)
                                    <li class="company_admin">
                                        <a data-toggle="tab" href="#company_admin">Администрирую</a>
                                    </li>
                                @else
                                    <li class="teacher">
                                        <a data-toggle="tab" href="#teach">Преподаю</a>
                                    </li>
                                    <li class="teacher_creator">
                                        <a data-toggle="tab" href="#teach_creator">Создал</a>
                                    </li>
                                @endif
                            </ul>
                            {{--Вкладка обучаюсь--}}
                            <div class="tab-content clearfix" id="course_container">
                                <div id="stud" class="tab-pane fade in active">

                                    @foreach ( $students as $student)
                                        <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                            <div class="block span3">
                                                <div class="product">
                                                    @if (!is_null($student->course->photo_id ))
                                                        @foreach($photos as $photo)
                                                            @if($photo->id == $student->course->photo_id)
                                                                <img class = "" src="{{Storage::url('course_avatars/')}}{{ $student->course_id }}.{{$photo->type}}" >
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <img src="/img/course/no_avatar.png">
                                                    @endif

                                                    <div class="buttons pointer" >
                                                    </div>
                                                </div>

                                                <div class="info h160">
                                                    <h4>Курс: {{$student->course->name}}</h4>
                                                </div>
                                                <div class ="btn-container h130">


                                                    <button type="button" name="" class="btn btn-primary btn-block go_material" action="/course/id{{ $student->course_id }}/materials">
                                                        <span class="glyphicon glyphicon-pencil"></span> Перейти к курсу
                                                    </button>

                                                    <button type="button" name="" class="btn btn-success btn-block go_my_results" action="/course/id{{ $student->course_id }}/my_results">
                                                        <span class="glyphicon glyphicon-list-alt"></span> Мой прогресс
                                                    </button>

                                                    <button type="button"  class="btn btn-danger btn-block delete_open_dialog_online" data-course_id = "{{$student->course_id}}">
                                                        <span class="glyphicon glyphicon-trash"></span> Удалить курс
                                                    </button>


                                                </div>

                                            </div>
                                        </div>
                                    @endforeach


                                    {{--Оффлайн курсы--}}
                                    @foreach($students_offline as $stud_off)
                                            <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                                <div class="block span3">
                                                    <div class="product">
                                                        @if (!is_null($stud_off->offline_course->photo_id ))
                                                            @foreach($photos as $photo)
                                                                @if($photo->id == $stud_off->offline_course->photo_id)
                                                                    <img class = "" src="{{Storage::url('course_avatars/')}}{{ $stud_off->id_offlinecourse }}.{{$photo->type}}" >
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            <img src="/img/course/no_avatar.png">
                                                        @endif

                                                        <div class="buttons pointer" >
                                                        </div>
                                                    </div>

                                                    <div class="info h160">
                                                        <h4>Курс: {{$stud_off->offline_course->name}}</h4>
                                                    </div>
                                                    <div class ="btn-container h130">

                                                        <button type="button" name="" class="btn btn-primary btn-block go_material" action="/offline/card/id{{ $stud_off->id_offlinecourse }}">
                                                            <span class="glyphicon glyphicon-pencil"></span> Карточка курса
                                                        </button>

                                                        <button type="button"  class="btn btn-danger btn-block delete_open_dialog_offline" data-offline_id = "{{$stud_off->id_offlinecourse}}">
                                                            <span class="glyphicon glyphicon-trash"></span> Удалить курс
                                                        </button>

                                                    </div>

                                                </div>
                                            </div>
                                    @endforeach


                                    {{--Мероприятия--}}
                                        @foreach($students_meetings as $stud_meet)
                                            <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                                <div class="block span3">
                                                    <div class="product">
                                                        @if (!is_null($stud_meet->meetings->photo_id ))
                                                            @foreach($photos as $photo)
                                                                @if($photo->id == $student->course->photo_id)
                                                                    <img class = "" src="{{Storage::url('course_avatars/')}}{{ $stud_meet->id_meeting }}.{{$photo->type}}" >
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            <img src="/img/course/no_avatar.png">
                                                        @endif

                                                        <div class="buttons pointer" >
                                                        </div>
                                                    </div>

                                                    <div class="info h160">
                                                        <h4>{{$stud_meet->meetings->mit_type->name}}: {{$stud_meet->meetings->name}}</h4>
                                                    </div>
                                                    <div class ="btn-container h130">

                                                    <button type="button" name="" class="btn btn-primary btn-block go_material" action="/meeting/card/id{{ $stud_meet->id_meeting }}">
                                                        <span class="glyphicon glyphicon-pencil"></span> Карточка курса
                                                    </button>

                                                    @if($stud_meet->meetings->mit_type->id == 3)
                                                        <button type="button" name="" class="btn btn-primary btn-block go_webinar_room" action="/webinar/@php echo base64_encode($stud_meet->meetings->id_meeting) @endphp/viewer">
                                                            <span class="glyphicon glyphicon-pencil"></span> Переход в комнату
                                                        </button>
                                                    @endif

                                                    <button type="button"  class="btn btn-danger btn-block delete_open_dialog_meeting" data-meeting_id = "{{$stud_meet->id_meeting}}">
                                                        <span class="glyphicon glyphicon-trash"></span> Удалить курс
                                                    </button>

                                                    </div>

                                                </div>
                                            </div>
                                        @endforeach



                                        <script>
                                            // Удаление он-лайн курса из списка
                                            $(function(){
                                                $(".delete_open_dialog_online").on("click", function(){
                                                    var deleteok = confirm("Удалить курс из списка изучаемых?");
                                                    var course_id = $(this).data('course_id');
                                                    if (deleteok == true)
                                                    {var $link = "/course/id"+course_id+"/delete";
                                                        $.get($link).done(function () {
                                                        alert('Курс успешно удалён');
                                                        window.location.reload();
                                                    })}
                                                });

                                                $(".delete_open_dialog_offline").on("click", function(){
                                                    var deleteok = confirm("Удалить курс из списка изучаемых?");
                                                    var offline_id = $(this).data('offline_id');
                                                    if (deleteok == true)
                                                    {var $link = "/offline_course/id"+offline_id+"/delete";
                                                        $.get($link).done(function () {
                                                            alert('Курс успешно удалён');
                                                            window.location.reload();
                                                        })}
                                                });

                                                $(".delete_open_dialog_meeting").on("click", function(){
                                                    var deleteok = confirm("Удалить курс из списка изучаемых?");
                                                    var meeting_id = $(this).data('meeting_id');
                                                    if (deleteok == true)
                                                    {var $link = "/meeting/id"+meeting_id+"/delete";
                                                        $.get($link).done(function () {
                                                            alert('Курс успешно удалён');
                                                            window.location.reload();
                                                        })}

                                                });
                                            });

                                            //Удаление оййлайн курса из списка
                                        </script>
                                </div>
                                {{--TEACHER!!!!!!!!!!!!!--}}
                                <div id="teach" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-md-4 add-marginb-15 add-paddinglr-7">
                                            <div class="block span3 create-new-container">
                                                <div class="create-new ">
                                                    <img class="img-responsive center-block" src="/img/add_course_icon.png">
                                                    <b>Создать курс</b>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 add-marginb-15 add-paddinglr-7">
                                            <div class="block span3 create-new-meeting-container">
                                                <div class="create-new ">
                                                    <img class="img-responsive center-block" src="/img/add_course_icon.png">
                                                    <b>Создать мероприятие</b>
                                                </div>
                                            </div>
                                        </div>
                                @if(count($company_admin) == 0)
                                    @if(isset($offline_courses))
                                            {{--Вкладка преподаю вставка оффлайн курсов--}}
                                        @if(!is_null($offline_courses))
                                            @foreach($offline_courses as $teach)
                                                    <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                                        <div class="block span3">
                                                            <div class="product">

                                                                @if (!is_null($teach->offline->photo_id ))
                                                                    @foreach($photos as $photo)
                                                                        @if($photo->id == $teach->photo_id)
                                                                            <img class = "" src="{{Storage::url('course_avatars/')}}{{ $teach->offline->id }}.{{$photo->type}}" >
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    <img src="/img/course/no_avatar.png">
                                                                @endif


                                                                <div class="buttons pointer" >
                                                                </div>
                                                            </div>

                                                            <div class="info h130">
                                                                <h4>{{str_limit($teach->offline->name, 77, '...')}}</h4>
                                                            </div>
                                                            @if($user->id == Auth::id())
                                                                <div class ="btn-container h160">
                                                                        @if ($teach->is_moderated == 2)
                                                                            <button data-toggle="tooltip" data-placement="top" title="Переход к списку студентов по данному курсу" type="button" class="btn btn-block btn-success go_student_results" action="/course/id{{ $teach->id }}/my_students">
                                                                                <span></span> Курс прошёл модерацию
                                                                            </button>
                                                                        @elseif ($teach->offline->is_moderated == 1)
                                                                            <button data-toggle="tooltip" data-placement="top" title="Курс не прошел модерацию" type="button" class="btn btn-info btn-block go_shop" disabled>
                                                                                <span  class=""></span> На рассмотрении
                                                                            </button>
                                                                        @elseif ($teach->offline->is_moderated == 0)
                                                                            <button type="button"  action="/course/edit_offline/id{{$teach->offline->id}}/moderate" class="btn btn-danger btn-block send_to_moderation_offline" data-course_id="{{$teach->offline->id}}">
                                                                                <span class="glyphicon glyphicon-ruble"></span> Отправить на модерацию
                                                                            </button>

                                                                        @endif

                                                                    <button type="button" name="go_course" class="btn btn-primary btn-block go_course" action="/offline_course/id{{$teach->offline->id}}">
                                                                        <span class="glyphicon glyphicon-pencil"></span> Редактор курса
                                                                    </button>
                                                                    <button type="button" name="go_card" class="btn btn-primary btn-block go_card" action="/offline_course/card/id{{$teach->offline->id}}">
                                                                        <span class="glyphicon glyphicon-list-alt"></span> Карточка курса
                                                                    </button>

                                                                </div>
                                                            @endif

                                                        </div>

                                                    </div>
                                            @endforeach
                                            @endif
                                            {{--Вкладка преподаю вставка онлайн курсов--}}
                                            @if(!is_null($online_teach))
                                                @foreach($online_teach as $o_teach)
                                                    <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                                        <div class="block span3">
                                                            <div class="product">
                                                                @if (!is_null($o_teach->course->photo_id ))
                                                                    @foreach($photos as $photo)
                                                                        @if($photo->id == $teach->photo_id)
                                                                            <img class = "" src="{{Storage::url('course_avatars/')}}{{ $o_teach->course->id }}.{{$photo->type}}" >
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    <img src="/img/course/no_avatar.png">
                                                                @endif

                                                                <div class="buttons pointer" >
                                                                </div>
                                                            </div>

                                                            <div class="info h130">
                                                                <h4>{{str_limit($o_teach->course->name, 77, '...')}}</h4>
                                                            </div>
                                                            @if($user->id == Auth::id())
                                                                <div class ="btn-container h160">
                                                                    @if ($o_teach->course->is_moderated == 2)
                                                                        <button data-toggle="tooltip" data-placement="top" title="Переход к списку студентов по данному курсу" type="button" class="btn btn-block btn-success go_student_results" action="/course/id{{ $o_teach->course->id }}/my_students">
                                                                            <span></span> Курс прошёл модерацию
                                                                        </button>
                                                                    @elseif ($o_teach->course->is_moderated == 1)
                                                                        <button data-toggle="tooltip" data-placement="top" title="Курс не прошел модерацию" type="button" class="btn btn-info btn-block go_shop" disabled>
                                                                            <span  class=""></span> На рассмотрении
                                                                        </button>
                                                                    @elseif ($o_teach->course->is_moderated == 0)
                                                                        <button type="button"  action="/course/edit/id{{$o_teach->course->id}}/moderate" class="btn btn-danger btn-block send_to_modration_online" data-course_id="{{$o_teach->course->id}}">
                                                                            <span class="glyphicon glyphicon-ruble"></span> Отправить на модерацию
                                                                        </button>
                                                                    @endif


                                                                    <button type="button" name="go_course" class="btn btn-primary btn-block go_course" action="/constructor/id{{$o_teach->course->id}}">
                                                                        <span class="glyphicon glyphicon-pencil"></span> Редактор курса
                                                                    </button>
                                                                    <button type="button" name="go_card" class="btn btn-primary btn-block go_card" action="/course/card/id{{$o_teach->course->id}}">
                                                                        <span class="glyphicon glyphicon-list-alt"></span> Карточка курса
                                                                    </button>

                                                                </div>
                                                            @endif

                                                        </div>

                                                    </div>
                                                @endforeach
                                            @endif
                                        {{--Вкладка преподаю вставка мероприятий--}}
                                            @if(!is_null($meeting_teach))
                                                @foreach($meeting_teach as $m_teach)
                                                    <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                                        <div class="block span3">
                                                            <div class="product">
                                                                @if (!is_null($m_teach->meeting->photo_id ))
                                                                    @foreach($photos as $photo)
                                                                        @if($photo->id == $teach->photo_id)
                                                                            <img class = "" src="{{Storage::url('course_avatars/')}}{{ $m_teach->meeting->id }}.{{$photo->type}}" >
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    <img src="/img/course/no_avatar.png">
                                                                @endif

                                                                <div class="buttons pointer" >
                                                                </div>
                                                            </div>

                                                            <div class="info h130">
                                                                <h4>{{str_limit($m_teach->meeting->name, 77, '...')}}</h4>
                                                            </div>



                                                            @if($user->id == Auth::id())
                                                                <div class ="btn-container h160">
                                                                    @if ($m_teach->meeting->is_moderated == 1)
                                                                        <button data-toggle="tooltip" data-placement="top" title="Курс не прошел модерацию" type="button" class="btn btn-info btn-block go_shop" disabled>
                                                                            <span  class=""></span> На рассмотрении
                                                                        </button>
                                                                    @elseif ($m_teach->meeting->is_moderated == 2)
                                                                        <button data-toggle="tooltip" data-placement="top" title="Переход к списку студентов по данному курсу" type="button" class="btn btn-block btn-success go_student_results" action="/course/id{{ $m_teach->meeting->id_meeting }}/my_students">
                                                                            <span></span> Мероприятие прошло модерацию
                                                                        </button>
                                                                    @elseif ($m_teach->meeting->is_moderated == 0)
                                                                        <button type="button"  action="/meeting/edit/id{{$m_teach->meeting->id_meeting}}/moderate" class="btn btn-danger btn-block send_to_moderation_offline" data-course_id="{{$m_teach->meeting->id_meeting}}">
                                                                            <span class="glyphicon glyphicon-ruble"></span> Отправить на модерацию
                                                                        </button>
                                                                    @endif

                                                                        <button type="button" name="go_meeting_edit" class="btn btn-primary btn-block go_course" action="/meeting/edit{{$m_teach->meeting->id_meeting}}">
                                                                            <span class="glyphicon glyphicon-pencil"></span> Редактор встречи
                                                                        </button>
                                                                        <button type="button" name="go_meeting_card" class="btn btn-primary btn-block go_card" action="/meeting/card{{$m_teach->meeting->id_meeting}}">
                                                                            <span class="glyphicon glyphicon-list-alt"></span> Карточка встречи
                                                                        </button>

                                                                </div>
                                                            @endif

                                                        </div>

                                                    </div>

                                                @endforeach
                                            @endif

                                            <script>
                                                $('body').on('click', ".go_sell", function(){
                                                    var elem = $(this);
                                                    $.get( $(this).attr('action'), function(  ) {

                                                     }).done(function(data){
                                                        elem.removeClass('btn-info');
                                                        elem.addClass('disabled');
                                                        elem.addClass('btn-info');
                                                        elem.empty();
                                                        elem.append('<span  class="glyphicon glyphicon-ruble"></span> Проходит модерацию');
                                                     if (data == 0) alert ("Курс отправлен на модерацию");
                                                     if (data == 2) alert ("Курс ожидает модерации");
                                                     if (data == 1) alert ("Курс уже модерирован");
                                                     });
                                                });
                                            </script>



                                @endif
                                    </div>
                                </div>
                                {{--Когда человек преподаватель, но просто создаёт объекты без указания учителей.--}}
                                <div id="teach_creator" class="tab-pane fade">
                                    @if ($user->id == Auth::id())
                                        {{--Оффлайн курсы--}}
                                        @foreach($offline_creator as $of_creator)
                                        <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                            <div class="block span3">
                                                <div class="product">
                                                    @if (!is_null($of_creator->photo_id ))
                                                        @foreach($photos as $photo)
                                                            @if($photo->id == $of_creator->photo_id)
                                                                <img class = "" src="{{Storage::url('course_avatars/')}}{{ $of_creator->meeting->id }}.{{$photo->type}}" >
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <img src="/img/course/no_avatar.png">
                                                    @endif

                                                    <div class="buttons pointer" >
                                                    </div>
                                                </div>

                                                <div class="info h130">
                                                    <h4>{{str_limit($of_creator->name, 77, '...')}}</h4>
                                                </div>
                                                @if($user->id == Auth::id())
                                                    <div class ="btn-container h160">
                                                        @if ($of_creator->is_moderated == 1)
                                                            <button data-toggle="tooltip" data-placement="top" title="Курс не прошел модерацию" type="button" class="btn btn-info btn-block go_shop" disabled>
                                                                <span  class=""></span> На рассмотрении
                                                            </button>
                                                        @elseif ($of_creator->is_moderated == 2)
                                                            <button data-toggle="tooltip" data-placement="top" title="Переход к списку студентов по данному курсу" type="button" class="btn btn-block btn-success go_student_results" action="/course/id{{ $of_creator->id }}/my_students">
                                                                <span></span> Курс прошёл модерацию
                                                            </button>
                                                        @elseif ($of_creator->is_moderated == 0)
                                                            <button type="button"  action="/course/edit_offline/id{{$of_creator->id}}/moderate" class="btn btn-danger btn-block send_to_moderation_offline" data-course_id="{{$of_creator->id}}">
                                                                <span class="glyphicon glyphicon-ruble"></span> Отправить на модерацию
                                                            </button>
                                                        @endif

                                                        <button type="button" name="go_course" class="btn btn-primary btn-block go_course" action="/offline_course/id{{$of_creator->id}}">
                                                            <span class="glyphicon glyphicon-pencil"></span> Редактор курса
                                                        </button>
                                                        <button type="button" name="go_card" class="btn btn-primary btn-block go_card" action="/offline_course/card/id{{$of_creator->id}}">
                                                            <span class="glyphicon glyphicon-list-alt"></span> Карточка курса
                                                        </button>

                                                    </div>
                                                @endif

                                            </div>

                                        </div>
                                        @endforeach
                                        {{--конец оффлайн курсов--}}


                                        {{--Онлайн курсы--}}
                                        @foreach($online_creator as $on_creator)
                                            <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                                <div class="block span3">
                                                    <div class="product">
                                                        @if (!is_null($on_creator->photo_id ))
                                                            @foreach($photos as $photo)
                                                                @if($photo->id == $of_creator->photo_id)
                                                                    <img class = "" src="{{Storage::url('course_avatars/')}}{{ $on_creator->meeting->id }}.{{$photo->type}}" >
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            <img src="/img/course/no_avatar.png">
                                                        @endif

                                                        <div class="buttons pointer" >
                                                        </div>
                                                    </div>

                                                    <div class="info h130">
                                                        <h4>{{str_limit($on_creator->name, 77, '...')}}</h4>
                                                    </div>
                                                    @if($user->id == Auth::id())
                                                        <div class ="btn-container h160">
                                                            @if ($on_creator->is_moderated == 1)
                                                                <button data-toggle="tooltip" data-placement="top" title="Курс не прошел модерацию" type="button" class="btn btn-info btn-block go_shop" disabled>
                                                                    <span  class=""></span> На рассмотрении
                                                                </button>
                                                            @elseif ($on_creator->is_moderated == 2)
                                                                <button data-toggle="tooltip" data-placement="top" title="Переход к списку студентов по данному курсу" type="button" class="btn btn-block btn-success go_student_results" action="/course/id{{ $on_creator->id }}/my_students">
                                                                    <span></span> Курс прошёл модерацию
                                                                </button>
                                                            @elseif ($on_creator->is_moderated == 0)
                                                                <button type="button"  action="/course/edit/id{{$on_creator->id}}/moderate" class="btn btn-danger btn-block send_to_moderation_offline" data-course_id="{{$on_creator->id}}">
                                                                    <span class="glyphicon glyphicon-ruble"></span> Отправить на модерацию
                                                                </button>
                                                            @endif

                                                            <button type="button" name="go_course" class="btn btn-primary btn-block go_course" action="/constructor/id{{$on_creator->id}}">
                                                                <span class="glyphicon glyphicon-pencil"></span> Редактор курса
                                                            </button>
                                                            <button type="button" name="go_card" class="btn btn-primary btn-block go_card" action="/course/card/id{{$on_creator->id}}">
                                                                <span class="glyphicon glyphicon-list-alt"></span> Карточка курса
                                                            </button>

                                                        </div>
                                                    @endif

                                                </div>

                                            </div>
                                        @endforeach
                                        {{--конец онлайн курсов--}}

                                        {{--мероприятия--}}
                                        @foreach($meeting_creator as $m_creator)
                                            <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                                <div class="block span3">
                                                    <div class="product">
                                                        @if (!is_null($m_teach->meeting->photo_id ))
                                                            @foreach($photos as $photo)
                                                                @if($photo->id == $teach->photo_id)
                                                                    <img class = "" src="{{Storage::url('course_avatars/')}}{{ $m_creator->id }}.{{$photo->type}}" >
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            <img src="/img/course/no_avatar.png">
                                                        @endif

                                                        <div class="buttons pointer" >
                                                        </div>
                                                    </div>

                                                    <div class="info h130">
                                                        <h4>{{str_limit($m_creator->name, 77, '...')}}</h4>
                                                    </div>
                                                    @if($user->id == Auth::id())
                                                        <div class ="btn-container h160">
                                                            @if ($m_creator->is_moderated == 1)
                                                                <button data-toggle="tooltip" data-placement="top" title="Курс не прошел модерацию" type="button" class="btn btn-info btn-block go_shop" disabled>
                                                                    <span  class=""></span> На рассмотрении
                                                                </button>
                                                            @elseif ($m_creator->is_moderated == 2)
                                                                <button data-toggle="tooltip" data-placement="top" title="Переход к списку студентов по данному курсу" type="button" class="btn btn-block btn-success go_student_results" action="/course/id{{ $m_creator->id_meeting }}/my_students">
                                                                    <span></span> Мероприятие прошло модерацию
                                                                </button>
                                                            @elseif ($m_teach->meeting->is_moderated == 0)
                                                                <button type="button"  action="/meeting/edit/id{{$m_creator->id_meeting}}/moderate" class="btn btn-danger btn-block send_to_moderation_offline" data-course_id="{{$m_creator->id_meeting}}">
                                                                    <span class="glyphicon glyphicon-ruble"></span> Отправить на модерацию
                                                                </button>
                                                            @endif

                                                            <button type="button" name="go_meeting_edit" class="btn btn-primary btn-block go_course" action="/meeting/edit{{$m_creator->id_meeting}}">
                                                                <span class="glyphicon glyphicon-pencil"></span> Редактор встречи
                                                            </button>
                                                            <button type="button" name="go_meeting_card" class="btn btn-primary btn-block go_card" action="/meeting/card{{$m_creator->id_meeting}}">
                                                                <span class="glyphicon glyphicon-list-alt"></span> Карточка встречи
                                                            </button>

                                                        </div>
                                                    @endif

                                                </div>

                                            </div>
                                        @endforeach
                                        {{--конец мероприятий--}}
                                    @endif
                                </div>


                                {{--Вкладка администрирую--}}
                                @else
                                    <div id="company_admin" class="tab-pane fade">
                                    @if ($user->id == Auth::id())
                                        <div class="col-md-4 add-marginb-15 add-paddinglr-7">
                                            <div class="block span3 create-new-container">
                                                <div class="create-new ">
                                                    <img class="img-responsive center-block" src="/img/add_course_icon.png">
                                                    <b>Создать курс</b>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 add-marginb-15 add-paddinglr-7">
                                            <div class="block span3 create-new-meeting-container">
                                                <div class="create-new ">
                                                    <img class="img-responsive center-block" src="/img/add_course_icon.png">
                                                    <b>Создать мероприятие</b>
                                                </div>
                                            </div>
                                        </div>

                                        @if(!is_null($company_admin_online))
                                        @foreach($company_admin_online as $course)
                                            <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                                <div class="block span3">
                                                    <div class="product">
                                                        @if (!is_null($course->photo_id ))
                                                            @foreach($photos as $photo)
                                                                @if($photo->id == $course->photo_id)
                                                                    <img class = "" src="{{Storage::url('course_avatars/')}}{{ $course->id }}.{{$course->type}}" >
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            <img src="/img/course/no_avatar.png">
                                                        @endif

                                                        <div class="buttons pointer" >
                                                        </div>
                                                    </div>

                                                    <div class="info h130">
                                                        <h4>{{str_limit($course->name, 77, '...')}}</h4>
                                                    </div>
                                                    @if($user->id == Auth::id())
                                                        <div class ="btn-container h160">
                                                            @if(!is_null($course->teacher))
                                                                @if ($course->is_moderated == 0)
                                                                    <button type="button" name="go_sell" action="/course/edit/id{{$course->id}}/moderate" class="btn btn-danger btn-block go_sell" data-course_id="{{$course->id}}">
                                                                        <span class="glyphicon glyphicon-ruble"></span> Отправить на модерацию
                                                                    </button>
                                                                @elseif ($course->is_moderated == 1)
                                                                    <button data-toggle="tooltip" data-placement="top" title="Курс не прошел модерацию" type="button" class="btn btn-info btn-block go_shop" data-course_id="{{$course->id}}" disabled>
                                                                        <span  class=""></span> На рассмотрении
                                                                    </button>
                                                                @else
                                                                    <button data-toggle="tooltip" data-placement="top" title="Переход к списку студентов по данному курсу" type="button" class="btn btn-block btn-success go_student_results" action="/course/id{{ $course->id }}/my_students">
                                                                        <span></span> Курс прошёл модерацию
                                                                    </button>
                                                                @endif

                                                                @foreach($course->teacher as $teachers)
                                                                    @if($teachers->is_moderated_by_teacher == 0)
                                                                        <button class="btn btn-block btn-danger" disabled>{{$teachers->teacher->user->first}} {{$teachers->teacher->user->second}} не принял</button>

                                                                    @else
                                                                        <button class="btn btn-block btn-success" disabled>{{$teachers->teacher->user->first}} {{$teachers->teacher->user->second}} принял</button>
                                                                    @endif
                                                                @endforeach

                                                            @endif

                                                            <button type="button" name="go_course" class="btn btn-primary btn-block go_course" action="/constructor/id{{$course->id}}">
                                                                <span class="glyphicon glyphicon-pencil"></span> Редактор курса
                                                            </button>
                                                            <button type="button" name="go_card" class="btn btn-primary btn-block go_card" action="/course/card/id{{$course->id}}">
                                                                <span class="glyphicon glyphicon-list-alt"></span> Карточка курса
                                                            </button>

                                                        </div>
                                                    @endif

                                                </div>

                                            </div>
                                        @endforeach
                                        @endif

                                        @if(!is_null($company_admin_offline))
                                            @foreach($company_admin_offline as $course)
                                                <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                                    <div class="block span3">
                                                        <div class="product">
                                                            @if (!is_null($course->photo_id ))
                                                                @foreach($photos as $photo)
                                                                    @if($photo->id == $course->photo_id)
                                                                        <img class = "" src="{{Storage::url('course_avatars/')}}{{ $course->id }}.{{$photo->type}}" >
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                <img src="/img/course/no_avatar.png">
                                                            @endif

                                                            <div class="buttons pointer" >
                                                            </div>
                                                        </div>

                                                        <div class="info h130">
                                                            <h4>{{str_limit($course->name, 77, '...')}}</h4>
                                                        </div>
                                                        @if($user->id == Auth::id())
                                                            <div class ="btn-container h160">
                                                                <div style=" max-height: 200px; overflow: auto;">
                                                                @if($course->is_moderated == 0)
                                                                    <button type="button" name="go_sell" action="/course/edit/id{{$course->id}}/moderate" class="btn btn-danger btn-block go_sell" data-course_id="{{$course->id}}">
                                                                        <span class="glyphicon glyphicon-ruble"></span> Отправить на модерацию
                                                                    </button>
                                                                @elseif ($course->is_moderated == 1)
                                                                    <button data-toggle="tooltip" data-placement="top" type="button" class="btn btn-info btn-block go_shop" data-course_id="{{$course->id}}" disabled>
                                                                        <span></span> На рассмотрении
                                                                    </button>

                                                                @elseif ($course->is_moderated == 2)
                                                                    <button data-toggle="tooltip" data-placement="top" title="Переход к списку студентов по данному курсу" type="button" class="btn btn-block btn-success go_student_results" action="/course/id{{ $course->id }}/my_students">
                                                                        <span></span> Курс прошёл модерацию
                                                                    </button>
                                                                @endif

                                                                @foreach($course->teacher as $teachers)
                                                                    @if($teachers->is_moderated_by_teacher == 0)
                                                                            <button class="btn btn-block btn-danger" disabled>{{$teachers->teacher->user->first}} {{$teachers->teacher->user->second}} не принял</button>

                                                                        @else
                                                                            <button class="btn btn-block btn-success" disabled>{{$teachers->teacher->user->first}} {{$teachers->teacher->user->second}} принял</button>
                                                                    @endif
                                                                @endforeach


                                                                <button type="button" name="go_offline_course" class="btn btn-primary btn-block go_course" action="/offline_course/id{{$course->id}}">
                                                                    <span class="glyphicon glyphicon-pencil"></span> Редактор курса
                                                                </button>
                                                                <button type="button" name="go_offline_card" class="btn btn-primary btn-block go_card" action="/offline_course/card/id{{$course->id}}">
                                                                    <span class="glyphicon glyphicon-list-alt"></span> Карточка курса
                                                                </button>
                                                                </div>
                                                            </div>
                                                        @endif

                                                    </div>

                                                </div>
                                            @endforeach
                                        @endif

                                        @if(!is_null($company_admin_meeting))
                                            @foreach($company_admin_meeting as $meeting)
                                                <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                                    <div class="block span3">
                                                        <div class="product">
                                                            @if (!is_null($meeting->photo_id ))
                                                                @foreach($photos as $photo)
                                                                    @if($photo->id == $meeting->photo_id)
                                                                        <img class = "" src="{{Storage::url('course_avatars/')}}{{ $meeting->id }}.{{$meeting->type}}" >
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                <img src="/img/course/no_avatar.png">
                                                            @endif

                                                            <div class="buttons pointer" >
                                                            </div>
                                                        </div>

                                                        <div class="info h130">
                                                            <h4>{{str_limit($meeting->name, 77, '...')}}</h4>
                                                        </div>
                                                        @if($user->id == Auth::id())
                                                            <div class ="btn-container h160">
                                                                @if(!is_null($meeting->teacher))
                                                                    @if ($meeting->is_moderated == 0)
                                                                        <button  action="/meeting/edit/id{{$meeting->id_meeting}}/moderate" class="btn btn-danger btn-block meeting_send_to_moderation" data-meeting_id="{{$meeting->id_meeting}}">
                                                                            <span class="glyphicon glyphicon-ruble"></span> Отправить на модерацию
                                                                        </button>
                                                                    @elseif ($meeting->is_moderated == 1)
                                                                        <button data-toggle="tooltip" data-placement="top" title="Курс не прошел модерацию" type="button" class="btn btn-info btn-block go_shop" data-meeting_id="{{$meeting->id}}" disabled>
                                                                            <span  class=""></span> На рассмотрении
                                                                        </button>
                                                                    @else
                                                                        <button data-toggle="tooltip" data-placement="top" title="Переход к списку студентов по данному курсу" type="button" class="btn btn-block btn-success go_student_results" action="/course/id{{ $meeting->id_meeting }}/my_students">
                                                                            <span></span> Курс прошёл модерацию
                                                                        </button>
                                                                    @endif

                                                                    @foreach($meeting->teacher as $teachers)
                                                                        @if($teachers->is_moderated_by_teacher == 0)
                                                                            <button class="btn btn-block btn-danger" disabled>{{$teachers->teacher->user->first}} {{$teachers->teacher->user->second}} не принял</button>

                                                                        @else
                                                                            <button class="btn btn-block btn-success" disabled>{{$teachers->teacher->user->first}} {{$teachers->teacher->user->second}} принял</button>
                                                                        @endif
                                                                    @endforeach

                                                                @endif



                                                                <button type="button" name="go_meeting_edit" class="btn btn-primary btn-block go_course" action="/meeting/edit{{$meeting->id_meeting}}">
                                                                        <span class="glyphicon glyphicon-pencil"></span> Редактор встречи
                                                                </button>
                                                                <button type="button" name="go_meeting_card" class="btn btn-primary btn-block go_card" action="/meeting/card{{$meeting->id_meeting}}">
                                                                    <span class="glyphicon glyphicon-list-alt"></span> Карточка встречи
                                                                </button>

                                                            </div>
                                                        @endif

                                                    </div>

                                                </div>
                                            @endforeach
                                        @endif
                                    @endif
                                </div>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
                {{--уведомления о новых курсах и мероприятиях--}}
                @if($user->id == Auth::id())

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12">
                        @if(count($is_teacher) != 0)
                            @if(count($count_notification)!= 0)
                                <button id="new_notification" class="btn-success btn btn-block">{{$count_notification}}</button>

                                <div id="notification_panel">

                                        <h3>Вам поступили следующие предложения</h3>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Кто просит стать преподавателем</th>
                                                <th>Тип мероприятия</th>
                                                <th>Название события</th>
                                                <th>Дата начала</th>
                                                <th>Согласие</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                                @foreach($notification_from_online as $notific_online)
                                                    <tr>
                                                        <td><a href="{{$notific_online->u_id}}">{{$notific_online->first}} {{$notific_online->second}}</a></td>
                                                        <td>Он-лайн курс</td>
                                                        <td><a href="{{$notific_online->c_id}}">{{$notific_online->name}}</a></td>
                                                        <td>Уточнить у автора</td>
                                                        <td>
                                                            <button id="online_teacher_accept" class = "btn btn-success" data-c_id = "{{$notific_online->c_id}}">Да</button>
                                                            <button id="online_teacher_reject" class = "btn btn-danger" data-c_id = "{{$notific_online->c_id}}">Нет</button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                @foreach($notification_from_offline as $notific_offline)
                                                    <tr>
                                                        <td><a href="{{$notific_offline->u_id}}">{{$notific_offline->first}} {{$notific_offline->second}}</a></td>
                                                        <td data-id=1>Оффлайн-курс</td>
                                                        <td><a href="{{$notific_offline->o_id}}">{{$notific_offline->name}}</a></td>
                                                        <td>@if(!is_null($notific_offline->start_date)){{$notific_offline->start_date}}@else не указано @endif</td>
                                                        <td>
                                                            <button id="offline_teacher_accept" class = "btn btn-success" data-o_id = "{{$notific_offline->o_id}}">Да</button>
                                                            <button id="offline_teacher_reject" class = "btn btn-danger" data-o_id = "{{$notific_offline->o_id}}">Нет</button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                @foreach($notification_from_meetings as $notific_meetings)
                                                    <tr>
                                                        <td><a href="{{$notific_meetings->u_id}}">{{$notific_meetings->first}} {{$notific_meetings->second}}</a></td>
                                                        <td data-id=2>{{$notific_meetings->t_name}}</td>
                                                        <td><a href="{{$notific_meetings->m_id}}">{{$notific_meetings->name}}</a></td>
                                                        <td>@if(!is_null($notific_meetings->start_date)){{$notific_meetings->start_date}}@else не указано @endif</td>
                                                        <td>
                                                            <button id="meeting_teacher_accept" class = "btn btn-success" data-m_id = "{{$notific_meetings->m_id}}">Да</button>
                                                            <button id="meeting_teacher_reject" class = "btn btn-danger" data-m_id = "{{$notific_meetings->m_id}}">Нет</button>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            </tbody>
                                        </table>


                                </div>
                            @else
                                <button class="btn-success btn btn-block">Уведомлений нет</button>
                            @endif
                        @endif
                    </div>
                </div>
                @endif
            </div>

        </div>
    </div>

    <!--
        <div class="row margin-row ">
            <div class="col-md-12 ">
                6
            </div>
        </div>
        -->
    </div>



    <script>



        jQuery(document).ready(function(){
            upload_avatar();

            $("#notification_panel").slideUp();




        });

        $(".meeting_send_to_moderation").on('click', function () {
            var link = $(this).attr('action');
            alert(link);

            $.get(link).done(function () {
                alert('Мероприятие отправлено на модерацию');
                window.location.reload();
            })
        });

        $("#new_notification").on('click', function () {
            $("#notification_panel").slideToggle();
        })


    </script>
@endsection
