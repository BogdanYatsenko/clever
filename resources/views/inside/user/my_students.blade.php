@extends('inside.index')

@section('title', 'Мои студенты')

@section('modal')

    <div class="modal fade" id="deleteConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Результаты работы студента</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-right" id="deleteNo" data-dismiss="modal">
                        <span></span> Закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('content')

    <div class="container">
        <div class="row margin-row ">
            <div class="col-md-12 col-sm-12 col-xs-12 user-name">
                Список студентов курса "{{$course->name}}"
            </div>


            <p class="col-md-12 col-sm-12 col-xs-12 ">


                <div id="accordion" role="tablist" aria-multiselectable="true">
                    @foreach($students as $student)
                    <div class="card">
                        <div class="card-header" role="tab" id="studentCard{{$student->user->id}}">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$student->user->id}}" aria-expanded="false" aria-controls="collapseTwo">
                                    <b>{{$student->user->first}} {{$student->user->second}} {{$student->user->third}}</b>
                                </a>
                            </h5>
                        </div>
                        <div id="collapse{{$student->user->id}}" class="collapse" role="tabpanel" aria-labelledby="studentCard{{$student->user->id}}">
                            <div class="card-block">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th class="col-md-6 col-xs-6">Название материала</th>
                                        <th class="col-md-4 col-xs-4">Результаты</th>
                                        <th class="col-md-2 col-xs-2">Оценка</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($materials as $material)

                                        @if($material->type == 4)

                                            <tr>
                                                <td  class="col-md-6 col-xs-6 editor-container">Тест {{$material->name}}</td>
                                                <td  class="col-md-4 col-xs-4"><div class = "btn btn-default get_student_results" data-toggle="modal"  data-target="#deleteConfirmation" data-material_id = "{{$material->id}}" data-student_id = "{{$student->user->id}}">Просмотреть</div></td>
                                                <td  class="col-md-2 col-xs-2">

                                                    <div class="dropdown">
                                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            @foreach($student->mark as $mark)
                                                                @if (isset($mark))
                                                                    @if($material->id == $mark->material_id)
                                                                        @if($mark->mark == 1)
                                                                            Зачтено
                                                                        @endif
                                                                            @if($mark->mark == 2)
                                                                                Не зачтено
                                                                            @endif
                                                                            @if($mark->mark == 3)
                                                                                Удовлетворительно
                                                                            @endif
                                                                            @if($mark->mark == 4)
                                                                                Хорошо
                                                                            @endif
                                                                            @if($mark->mark == 5)
                                                                                Отлично
                                                                            @endif
                                                                    @endif
                                                                @else
                                                                    Выставить оценку
                                                                @endif
                                                            @endforeach


                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" >
                                                            <li> <a class = "li_element" data-mark = '1' data-material_id = '{{$material->id}}' data-student_id = '{{$student->user_id}}'>Зачтено</a></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li> <a class = "li_element" data-mark = '2' data-material_id = '{{$material->id}}' data-student_id = '{{$student->user_id}}'>Не зачтено</a></li>
                                                            <li> <a class = "li_element" data-mark = '3' data-material_id = '{{$material->id}}' data-student_id = '{{$student->user_id}}'>Удовлетворительно</a></li>
                                                            <li> <a class = "li_element" data-mark = '4' data-material_id = '{{$material->id}}' data-student_id = '{{$student->user_id}}'>Хорошо</a></li>
                                                            <li> <a class = "li_element" data-mark = '5' data-material_id = '{{$material->id}}' data-student_id = '{{$student->user_id}}'>Отлично</a></li>
                                                        </ul>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endif

                                        @if($material->type == 5)
                                            <tr>
                                                <td  class="col-md-6 col-xs-6 editor-container">Задание {{$material->name}} </td>
                                                 <td  class="col-md-4 col-xs-4">Просмотреть</td>
                                                <td  class="col-md-2 col-xs-2">

                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item" href="#">Action</a>
                                                            <a class="dropdown-item" href="#">Another action</a>
                                                            <a class="dropdown-item" href="#">Something else here</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a class="dropdown-item" href="#">Separated link</a>
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endif

                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>



        </div>

    </div>
    </div>

<script>
    $(document).ready(function () {

        var _token = $("input[name='_token']").val();

        $(".li_element").on('click', function(){

            var student_id = $(this).data('student_id');
            var material_id = $(this).data('material_id');
            var mark = $(this).data('mark');
            var elem = $(this).parent().parent().parent().find('.dropdown-toggle');
            console.log(elem);
            var li_elem = $(this);

            $.post('/course/set_mark_to_student', {
                _token: _token,
                student_id: student_id,
                material_id: material_id,
                mark: mark
            }).done(function(){
                elem.html(li_elem.text() + "<span class='caret'></span>");
            });
        });

        $('.get_student_results').on('click', function(){

            var student_id = $(this).data('student_id');
            var material_id = $(this).data('material_id');

            $.post('/course/id{{$course->id}}/get_student_results', {
                _token: _token,
                student_id: student_id,
                material_id: material_id

            }).done(function (data) {

                var html =

                '<table class="table table-striped">'+
                    '<thead>'+
                    '<tr>'+
                    '<th class="col-md-6 col-xs-6">Результат</th>'+
                    '<th class="col-md-3 col-xs-3">Затраченное время</th>'+
                    '<th class="col-md-3 col-xs-3">Дата начала теста</th>'+
                    '</tr>'+
                    '</thead>'+
                    '<tbody>'

                    ;
                for(var k in data) {

                    var time_for_test =  data[k].updated_at - data[k].created_at;

                    var d1 = new Date();
                    d1.setTime(Date.parse(data[k].updated_at));

                    var d2 = new Date();
                    d2.setTime(Date.parse(data[k].created_at));

                    d3 = Math.floor((d1 - d2)/60000);
                    d4 = Math.floor(((d1 - d2)%60000)/1000);

                    if ((d3+d4) == 0)
                    html +=
                        '<tr>'+
                        '<td  class="col-md-6 col-xs-6">'+data[k].result+' баллов из 100</td>'+
                        '<td  class="col-md-3 col-xs-3">Тест не был закончен</td>'+
                        '<td  class="col-md-3 col-xs-3">'+data[k].created_at+'</td></tr>';

                    if ((d3+d4) > 0)
                        html +=
                            '<tr>'+
                            '<td  class="col-md-6 col-xs-6">'+data[k].result+' баллов из 100</td>'+
                            '<td  class="col-md-3 col-xs-3">'+d3+' мин.'+d4+' сек.</td>'+
                            '<td  class="col-md-3 col-xs-3">'+data[k].created_at+'</td></tr>';

                    if ((d3+d4) > 0) {
                        var cid = '{{$course->id}}';
                        html += '<tr><td colspan="3" class="col-md-3 col-xs-1" ><button action="/course/id' + cid +'/quiz' + data[k].material_id + '/uid' + data[k].user_id + '" type="button" class="btn-default btn btn-left go_student_results" ><b>Посмотреть ответы</b></button></td><tr>';
                    }
                }
                html += '</tbody></table>';



                $('.modal-body').html(html);
            });
        });





    });
</script>
@endsection