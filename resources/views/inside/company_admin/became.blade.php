@extends('inside.index')

@section('title', 'Администрирование')

@section('content')
    <div class="container">
        <div class="row margin-row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12 user-name">
                    Хотите стать администратором компании?
                </div>
            </div>
        </div>


        <div class="row margin-row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                Являетесь представителем компании? Придумать текст ..........................
                <br/>
                Вы сможете
                <ul>
                    <li>Сделать это</li>
                    <li>Сделать это</li>
                    <li>Сделать это</li>
                </ul>
                Укажите Ваш номер телефона, по которому с Вами свяжется наш сотрудник для проверки необходимой информации.
            </div>

            <form id ="create-admin-form" role="form" method="POST" action="/company_admin/became">
                {{ csrf_field() }}
                <div class="row margin-row">
                    <div class="form-group">
                        <div class="col-md-8 col-sm-12 col-xs-12">
                            <input id="company_admin_telephone" class="form-control" placeholder="Контактый номер" required autofocus type="text">
                        </div>
                    </div>
                </div>

                <br/>
                <div class="row margin-row" id="buttons">
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">
                            <button id="cancel_course" action="/id{{Auth::id()}}" type="button" class="btn-default btn btn-left"><b>Отмена</b></button>
                            <button id="send_became_company_admin" type="submit" class="btn btn-success btn-right"><b>Отправить заявку</b></button>
                        </div>
                    </div>

                </div>
            </form>
            </div>


        </div>
    </div>
@endsection