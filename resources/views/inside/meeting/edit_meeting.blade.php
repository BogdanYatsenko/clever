@extends('inside.index')

@section('title', 'Редактирование курса')

@section('modal')
@endsection

@section('content')
    <div class = "container  has-editor-api">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 block course-head-container">
                        <div class="col-md-11 col-sm-11 col-xs-11 left-container">
                            <div class="row">
                                <div class="col-md-2 col-sm-3 col-xs-12 course-avatar-container">
                                    @foreach($meetings as $meeting)
                                        <form enctype="multipart/form-data" id="course-avatar-upload-form"
                                              action="/offline_course/edit/id{{$meeting->id}}" method="POST">
                                            <input type="file" name="avatar" id="upload-avatar">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="course_id" value="{{$meeting->id}}">
                                            <a class="course-avatar-upload" href="/offline_course/edit/id{{$meeting->id}}" type="file"
                                               name="avatar">
                                                <span class="helper"></span>
                                                @if (!is_null($meeting->photo_id))
                                                    <img class="course-avatar"
                                                         src="{{Storage::url('course_avatars/small/')}}{{ $meeting->id }}.{{$meeting->photo->type}}" style = "max-width: 130px; height: 140px; object-fit: contain;">
                                                @else
                                                    <img class="course-avatar" src="/img/course/no_avatar.png">
                                                @endif
                                                <div class="middle">
                                                    <div class="text">Обновить</div>
                                                </div>
                                            </a>
                                        </form>
                                </div>
                                <div class="col-md-10 col-sm-9 col-xs-12 ">
                                    <div class="row">
                                        <div class="col-md-12  col-sm-12 col-xs-12 course-name">
                                            {{str_limit($meeting->name, 150, '...')}}
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
                    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>


                    <form id ="meeting-update-form" role="form" method="POST" action="/meeting/update/id{{$first_meetings->id_meeting}}">
                        {{ csrf_field() }}
                        <div class="col-md-12  col-sm-12 col-xs-12" style="margin-top: 20px">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Название курса:
                                        <input id="meeting_name" class="form-control" placeholder="{{$meeting->name}}" name="course_name" required autofocus type="text" value="{{$meeting->name}}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Тип мероприятия:
                                        <select id="type_meeting" class="selectpicker form-control" name="meeting_type_name" title="Выберите тип">
                                            <option data-id="0" @if($meeting->id_type == 0) selected @endif> Семинар</option>
                                            <option data-id="1" @if($meeting->id_type == 1) selected @endif> Митап</option>
                                            <option data-id="2" @if($meeting->id_type == 2) selected @endif> Тренинг</option>
                                            <option data-id="3" @if($meeting->id_type == 3) selected @endif> Вэбинар</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Краткое описание:
                                        @if ($meeting->description == "")
                                            <textarea type="text" rows="3" class="form-control" id="discription" placeholder="Данные не указаны" value=""></textarea>
                                        @else
                                            <textarea type="text" rows="3" class="form-control" id="discription" placeholder="{{$meeting->description}}" value="{{$meeting->description}}"></textarea>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Полное описание:
                                        @if ($meeting->full_description == "")
                                            <textarea type="text" rows="3" class="form-control" id="full_description" placeholder="Данные не указаны" value=""></textarea>
                                        @else
                                            <textarea type="text" rows="3" class="form-control" id="full_description" placeholder="{{$meeting->full_description}}" value="{{$meeting->full_description}}"></textarea>
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Стоимость посещения мероприятия:
                                        <div class="input-group">
                                            <input id="meeting_course_price" class="form-control" placeholder="Цена" name="meeting_price" type="number" min="0"
                                                   value="{{$meeting->price}}">
                                            <span class="input-group-addon">
                                                <input class="free" aria-label="..." name="meeting_free"  type="checkbox"
                                                    @if($meeting->price == 0) checked @endif>
                                                    Бесплатный
                                            </span>
                                        </div>
                                        <div class="help-block hidden help-error">

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 0px">
                                            Укажите сложность:
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <input class="dif_check" aria-label="..." name="meeting_difficult_checkbox"  type="checkbox">
                                            Не указывать
                                        </div>
                                        <div id="select_dif">
                                            <select id="meeting_difficulty" class="selectpicker form-control" name="meeting_difficulty_name" placeholder="Сложность" title="Выберите сложность">
                                                <option data-id="1" @if(isset($meeting->difficult_id)) @if($meeting->difficult_id == 1) selected @endif @endif> Низкая </option>
                                                <option data-id="2" @if(isset($meeting->difficult_id)) @if($meeting->difficult_id == 2) selected @endif @endif> Средняя </option>
                                                <option data-id="3" @if(isset($meeting->difficult_id)) @if($meeting->difficult_id == 3) selected @endif @endif> Высокая </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="except_if_webinar">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Количество мест:
                                        @if ($meeting->max_number_students == "")
                                        <input id="meeting_max_number_students" class="form-control" placeholder="Данные не указаны"    type="number">
                                        @else
                                            <input id="meeting_max_number_students" class="form-control" placeholder="Максимальное число слушателей"
                                                   type="number" value="{{$meeting->max_number_students}}">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Место проведения:
                                        @if ($meeting->address == "")
                                        <input id="meeting_address" class="form-control" placeholder="Данные не указаны" type="text">
                                        @else
                                        <input id="meeting_address" class="form-control" placeholder="Введите адрес, где будут проходить занятия"
                                               type="text" value="{{$meeting->address}}">
                                        @endif
                                    </div>
                                </div>
                            </div>


                            @if($meeting->id_type == 1)
                                <div id="space_for_company">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            Компания, которая проводит мероприятие:
                                            @if($meeting->source == "")
                                            <input id="meetup_source" class="form-control" placeholder="Данные не указаны" type="text">
                                            @else
                                                <input id="meetup_source" class="form-control" placeholder="Название компании"
                                                       type="text" value="{{$meeting->source}}">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif



                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Необходимые знания:
                                        @if($meeting->knowledge_requires == "")
                                            <textarea type="text" rows="3" class="form-control" id="meeting_knowledge_requires" placeholder="Данные не указаны"></textarea>
                                        @else
                                            <textarea type="text" rows="3" class="form-control" id="meeting_knowledge_requires"
                                                      placeholder="{{$meeting->knowledge_requires}}"
                                                     ></textarea>
                                        @endif

                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Полученные навыки:
                                        @if($meeting->skills == "")
                                        <textarea type="text" rows="3" class="form-control" id="meeting_skills" placeholder="Данные не указаны"></textarea>
                                        @else
                                            <textarea type="text" rows="3" class="form-control" id="meeting_skills"
                                                      placeholder="{{$meeting->skills}}"
                                                      ></textarea>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Целевая аудитория курса:
                                        @if($meeting->audience == "")
                                        <textarea type="text" rows="3" class="form-control" id="meeting_audience" placeholder="Данные не указаны"></textarea>
                                        @else
                                            <textarea type="text" rows="3" class="form-control" id="meeting_audience"
                                                      placeholder="{{$meeting->audience}}"></textarea>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Цель мероприятия:
                                        @if($meeting->goal == "")
                                        <textarea type="text" rows="3" class="form-control" id="meeting_goal" placeholder="Данные не указаны" value=""></textarea>
                                        @else
                                            <textarea type="text" rows="3" class="form-control" id="meeting_goal"
                                                     >{{$meeting->goal}}</textarea>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            @if(!is_null($teachers))
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    Преподаватели мероприятия:
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">

                                        @foreach($teachers as $teacher)
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    @if(isset($teacher->teacher->user->photo->type))
                                                        <img src='{{Storage::url('avatars/')}}{{$teacher->teacher->user->id}}.{{$teacher->teacher->user->photo->type}}' style='width:35px'/> <b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->teacher->user->first}} {{$teacher->teacher->user->second}}</b>
                                                        <button type="button" class="btn btn-success update-meeting-delete-teacher" data-teacher_id="{{$teacher->teacher->id_teacher}}" data-meeting_id="{{$meeting->id_meeting}}">Удалить</button>
                                                        {{--<input data-content="<img src='{{Storage::url('avatars/')}}{{$teacher->teacher->user->id}}.{{$teacher->teacher->user->photo->type}}' style='width:35px'/><b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->teacher->user->first}} {{$teacher->teacher->user->second}}</b> " data-id="{{$teacher->teacher->id_teacher}}" value="{{$teacher->teacher->user->first}} {{$teacher->teacher->user->second}}">--}}
                                                    @else
                                                        <img src='/img/no_avatar.png' style='width:35px'/> <b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->teacher->user->first}} {{$teacher->teacher->user->second}}</b>
                                                        <button type="button" class="btn btn-success update-meeting-delete-teacher" data-teacher_id="{{$teacher->teacher->id_teacher}}" data-meeting_id="{{$meeting->id_meeting}}">Удалить</button>
                                                        {{--<input data-content="<img src='/img/no_avatar.png' style='width:35px'/><b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->teacher->user->first}} {{$teacher->teacher->user->second}}</b> " data-id="{{$teacher->teacher->id_teacher}}" value="{{$teacher->teacher->user->first}} {{$teacher->teacher->user->second}}">--}}
                                                    @endif


                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endif
                            <br/>


                            <div class="row" id="buttons">
                                <div class="col-md-4 col-sm-4 col-xs-6 add-marginb-30">
                                    <button id="cancel_course" action="/id{{Auth::id()}}" type="button" class="btn-default btn btn-left"><b>Отмена</b></button>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6 add-marginb-30">
                                    <div class="Delete_meeting_btn">

                                        <div class="modal-overlay closed" id="modal-overlay"></div>

                                        <div class="row">
                                            <div class="modal closed" id="modal" aria-hidden="true" aria-labelledby="modalTitle" aria-describedby="modalDescription" role="dialog">
                                                <div class="modal-guts" role="document">
                                                    <h2> Вы действительно хотите удалить мероприятие?</h2>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <button class="btn btn-default" id="close-button">Отмена</button>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <button id="delete_ok_meeting" type="button"  class="btn btn-danger btn-right"><b>Удалить</b></button>
                                                </div>
                                            </div>
                                        </div>
                                        <button id="open-button" type="button"  class="btn btn-danger btn-left"><b>Удалить</b></button>
                                    </div>


                                    <script>
                                        $('body').on('click', '#open-button', function (e) {
                                            var scrollHeight = Math.max(
                                                document.body.scrollHeight, document.documentElement.scrollHeight,
                                                document.body.offsetHeight, document.documentElement.offsetHeight,
                                                document.body.clientHeight, document.documentElement.clientHeight
                                            );

                                            var elem = document.getElementById('modal-overlay');
                                            elem.style.height = scrollHeight.toString()+'px';


                                        });

                                        var modal = document.querySelector("#modal"),
                                            modalOverlay = document.querySelector("#modal-overlay"),
                                            closeButton = document.querySelector("#close-button"),
                                            openButton = document.querySelector("#open-button");

                                        closeButton.addEventListener("click", function() {
                                            modal.classList.toggle("closed");
                                            modalOverlay.classList.toggle("closed");
                                        });

                                        openButton.addEventListener("click", function() {
                                            modal.classList.toggle("closed");
                                            modalOverlay.classList.toggle("closed");
                                        });

                                    </script>
                                    <style>

                                        .modal {
                                            /* Не обязательно  block */
                                            display: block;


                                            width: 600px;
                                            max-width: 100%;

                                            height: 300px;
                                            max-height: 100%;

                                            position: fixed;

                                            z-index: 100;

                                            left: 50%;
                                            top: 50%;

                                            /* Центруем */
                                            transform: translate(-50%, -50%);



                                            background: white;
                                            box-shadow: 0 0 60px 10px rgba(0, 0, 0, 0.9);
                                        }
                                        .closed {
                                            display: none;
                                        }

                                        .modal-overlay {
                                            position: fixed;
                                            top: 0;
                                            left: 0;
                                            width: 100%;
                                            height: 100%;
                                            z-index: 50;

                                            background: rgba(0, 0, 0, 0.6);
                                        }
                                        .modal-guts {

                                            top: 0;
                                            left: 0;
                                            width: 100%;

                                            overflow: auto;
                                            padding: 20px 50px 20px 20px;
                                        }
                                    </style>

                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 add-marginb-30">
                                    <button id="update_meeting" type="submit" class="btn btn-success btn-right"><b>Обновить</b></button>
                                </div>
                            </div>
                        </div>

                </form>
                </div>
            </div>
        </div>






    </div>
    </div>

    <script>





        /* render alert xhr */
        $(document).ready(function () {
            jQuery(function(){
                jQuery('#datetimepicker1').datetimepicker(
                    {
                        format: 'YYYY-MM-DD HH:mm'
                    }
                );
            });
            $("select[name=type_name]").change(function(){
                var el = $("#offline");
                var xhr = $("#xhr_course");
                if($(this).find("option:selected").data('id') == 0)
                {

                    el.css('visibility', 'hidden');
                    el.slideUp();

                    xhr.css('visibility', 'visible');
                    xhr.slideToggle();
                }
                else{
                    xhr.removeAttr('visibility');
                    xhr.css('visibility', 'visible');
                    xhr.slideUp();

                    el.removeAttr('visibility');
                    el.css('visibility', 'visible');
                    el.slideToggle();
                }
            });


            var el = $("#offline");
            el.css('visibility', 'hidden');
            el.slideUp();


            var xhr = $("#xhr_course");
            xhr.css('visibility', 'hidden');
            xhr.slideToggle();

            $(".xhr_course").click(function (e) {
                if ($('.xhr_course').is(':checked')) {
                    $('#xhrModal').modal('show');
                    $('#xhr_link').parent().parent().parent().removeClass('hidden');
                }
                else
                    $('#xhr_link').parent().parent().parent().addClass('hidden');
            });

            /* end render alert xhr */

            $('.selectpicker').selectpicker({
                size: 7
            });

        });

    </script>
    </div>
    </div>
    </div>


    {{--<script src="http://code.jquery.com/jquery-2.0.2.min.js"></script>--}}
@endsection





