@extends('inside.index')

@section('title', 'Создание встречи')

@section('content')
    <div class="container has-create-api">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 user-name">
                        Создание мероприятия
                    </div>
                </div>
            </div>


                    <form id ="create-meeting-form" role="form" method="POST" action="/meeting/create">
                        {{ csrf_field() }}
                    <div class="col-md-12  col-sm-12 col-xs-12 ">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    Тип мероприятия:
                                    <select id="type_meeting" class="selectpicker form-control" name="meeting_type_name" title="Выберите тип">
                                        <option data-id="0"> Семинар</option>
                                        <option data-id="1"> Митап</option>
                                        <option data-id="2"> Тренинг</option>
                                        <option data-id="3"> Вэбинар</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    Название мероприятия:
                                    <input class="form-control" placeholder="Тема мероприятия" name="meeting_name" required autofocus type="text">
                                </div>
                            </div>
                        </div>

                        <div id="meeting">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Краткое описание:
                                        <textarea type="text" rows="3" class="form-control" id="meeting_discription" placeholder="Краткое описание" value=""></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Полное описание:
                                        <textarea type="text" rows="3" class="form-control" id="meeting_full_description" placeholder="Полное описание" value=""></textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Стоимость посещения мероприятия:
                                        <div class="input-group">
                                            <input id="meeting_course_price" class="form-control" placeholder="Цена" name="meeting_price" type="number" min="0">
                                            <span class="input-group-addon">
                                                <input class="free" aria-label="..." name="meeting_free"  type="checkbox">
                                                    Бесплатный
                                            </span>
                                        </div>
                                        <div class="help-block hidden help-error">

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 0px">
                                            Укажите сложность:
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <input class="dif_check" aria-label="..." name="meeting_difficult_checkbox"  type="checkbox">
                                            Не указывать
                                        </div>
                                        <div id="select_dif">
                                            <select id="meeting_difficulty" class="selectpicker form-control" name="meeting_difficulty_name" placeholder="Сложность" title="Выберите сложность">
                                                <option data-id="1"> Низкая </option>
                                                <option data-id="2"> Средняя </option>
                                                <option data-id="3"> Высокая </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Длительность мероприятия:
                                        <input id="meeting_length_hours" class="form-control" placeholder="Длительность в часах"   type="number">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Дата мероприятия:
                                        <div class="input-group" id="meeting_datetimepicker">
                                            <input type="text" class="form-control" data-format="YYYY-MM-DD HH:MM:SS"/>
                                            <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>
                                {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>--}}
                                {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>--}}
                            <div class="row" id="except_if_webinar">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Количество мест:
                                        <input id="meeting_max_number_students" class="form-control" placeholder="Максимальное число слушателей"    type="number">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Место проведения:
                                        <input id="meeting_address" class="form-control" placeholder="Введите адрес, где будут проходить занятия"    type="text">
                                    </div>
                                </div>
                            </div>

                            <div id="space_for_company">

                            </div>


                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Необходимые знания:
                                        <textarea type="text" rows="3" class="form-control" id="meeting_knowledge_requires" placeholder="Введите компетенции, которыми должен обладать слушающий" value=""></textarea>

                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Полученные навыки:
                                        <textarea type="text" rows="3" class="form-control" id="meeting_skills" placeholder="Укажите список навыков, полученных после прохождения семинара" value=""></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Целевая аудитория курса:
                                        <textarea type="text" rows="3" class="form-control" id="meeting_audience" placeholder="Введите описание целевой аудитории" value=""></textarea>

                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        Цель мероприятия:
                                        <textarea type="text" rows="3" class="form-control" id="meeting_goal" placeholder="Проблемы, решённые во время мероприятия" value=""></textarea>

                                    </div>
                                </div>

                            </div>

                                @if(is_null($company_admin))
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px">
                                                    Выберите преподавателя:
                                                </div>
                                                <div class="col-md-9 col-sm-8 col-xs-8">
                                                    <input class="dif_check" aria-label="..." name="teacher_checkbox"  type="checkbox">
                                                    Свой преподаватель
                                                </div>

                                                <select title="Начните писать имя" name="teacher_selector" class="form-control selectpicker" data-live-search = "true" >
                                                    @foreach($teachers as $teacher)
                                                        @if(!isset($teacher->user->photo->id))
                                                            <option data-content="<img src='/img/no_avatar.png' style='width:35px'/><b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->user->first}} {{$teacher->user->second}}</b> " data-id="{{$teacher->id_teacher}}" value="{{$teacher->user->first}} {{$teacher->user->second}}"></option>
                                                        @else
                                                            <option data-id="{{$teacher->id_teacher}}" data-content="<img src='{{Storage::url('avatars/')}}{{$teacher->user->id}}.{{$teacher->user->photo->type}}' style='width:35px'/><b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->user->first}} {{$teacher->user->second}}</b>" value="{{$teacher->user->first}} {{$teacher->user->second}}"></option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12" style="padding-right: 0px" id="create_teacher_button_company_admin">

                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px">
                                                    Выберите преподавателя:
                                                </div>
                                                <div class="col-md-4 col-sm-8 col-xs-8">
                                                    <input class="dif_check" aria-label="..." name="teacher_checkbox"  type="checkbox">
                                                    Свой преподаватель
                                                </div>
                                                <div class="col-md-9 col-sm-12 col-xs-12" style="padding-left: 0px">
                                                    <select title="Найдите преподавателя" name="teacher_selector" class="form-control selectpicker" data-live-search = "true">
                                                        @foreach($teachers as $teacher)
                                                            @if(!isset($teacher->user->photo->id))
                                                                <option data-content="<img src='/img/no_avatar.png' style='width:35px'/><b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->user->first}} {{$teacher->user->second}}</b>" data-id="{{$teacher->id_teacher}}" value="{{$teacher->user->first}} {{$teacher->user->second}}"></option>
                                                            @else
                                                                <option data-id="{{$teacher->id_teacher}}" data-content="<img src='{{Storage::url('avatars/')}}{{$teacher->user->id}}.{{$teacher->user->photo->type}}' style='width:35px'/><b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->user->first}} {{$teacher->user->second}}</b>" value="{{$teacher->user->first}} {{$teacher->user->second}}"></option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12" style="padding-right: 0px" id="create_teacher_button_company_admin">

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <br/>

                            <div class="row" id="buttons">
                                <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">
                                    <button id="cancel_course" action="/id{{Auth::id()}}" type="button" class="btn-default btn btn-left" ><b>Отмена</b></button>
                                    <button id="add_meeting" type="submit" class="btn btn-success btn-right"><b>Создать мероприятие</b></button>
                                    <div id="all_teachers" ></div>
                                </div>
                            </div>
                        </div>



                        {{--<div id="webinar">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-6 col-sm-12 col-xs-12">--}}
                                    {{--<div class="form-group">--}}
                                        {{--Краткое описание:--}}
                                        {{--<textarea type="text" rows="3" class="form-control" id="meeting_discription" placeholder="Краткое описание" value=""></textarea>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6 col-sm-12 col-xs-12">--}}
                                    {{--<div class="form-group">--}}
                                        {{--Полное описание:--}}
                                        {{--<textarea type="text" rows="3" class="form-control" id="meeting_full_description" placeholder="Полное описание" value=""></textarea>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="row">--}}
                                {{--<div class="col-md-6 col-sm-12 col-xs-12">--}}
                                    {{--<div class="form-group">--}}
                                        {{--Длительность мероприятия:--}}
                                        {{--<input id="meeting_length_hours" class="form-control" placeholder="Длительность в часах"   type="number">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6 col-sm-12 col-xs-12">--}}
                                    {{--<div class="form-group">--}}
                                        {{--Дата и время начала:--}}
                                        {{--<div class="input-group" id="meeting_datetimepicker">--}}
                                            {{--<input type="text" class="form-control" data-format="YYYY-MM-DD HH:MM:SS"/>--}}
                                            {{--<span class="input-group-addon">--}}
                                  {{--<span class="glyphicon glyphicon-calendar"></span>--}}
                                {{--</span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                {{--</div>--}}
                            {{--</div>--}}

                        {{--</div>--}}
                    {{--</div>--}}





                    </form>



                        </form>
                    </div>
            <div id="add_teacher_company_admin" >
                <form id ="create-teacher-company_admin-form" role="form" method="POST" action="/company_admin/create_teacher">
                    {{ csrf_field() }}
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    Фамилия:
                                    <input id="second_name_create_teacher" class="form-control" placeholder="Фамилия" required type="text">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    Имя:
                                    <input id="first_name_create_teacher" class="form-control" placeholder="Имя" required  type="text">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    Отчество:
                                    <input id="third_name_create_teacher" class="form-control" placeholder="Отчество"   type="text">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    Email:
                                    <input id="email_create_teacher" class="form-control" placeholder="Emial" required  type="text">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    Пароль:
                                    <input id="password_create_teacher" class="form-control" placeholder="Пароль"  required type="password">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    Подтверждение пароля:
                                    <input id="confirm_password_create_teacher" class="form-control" placeholder="Введите пароль повторно"  required type="password">
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 50px">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <button id="new_teacher_company_admin" type="submit" class="btn btn-success btn-block"><b>Сохранить</b></button>
                            </div>
                            <div id="after-create-teacher-success-text" class="col-md-4 col-sm-12 col-xs-12">

                            </div>
                            <div id="after-create-teacher-success-button" class="col-md-4 col-sm-12 col-xs-12">

                            </div>
                        </div>

                    </div>
                    <div class="row hidden" id="all_created_teachers">

                    </div>
                </form>
            </div>

        </div>
    </div>


    <script>
        jQuery(function(){
            jQuery('#meeting_datetimepicker').datetimepicker(
                {
                    format: 'YYYY-MM-DD HH:mm'
                }
            );
        });

        var meeting = $("#meeting");
        meeting.slideUp();

        var webinar = $("#webinar");
        webinar.slideUp();

        var add_teacher_company_admin = $("#add_teacher_company_admin");
        add_teacher_company_admin.slideUp();

        $('.selectpicker').selectpicker({
            size: 7
        });

        $("input[name=difficult_checkbox]").click( function(){
            var el = $("#select_dif");
            if (this.checked) {
                el.slideUp();
            } else {
                el.slideToggle();
            }
        });

        $("input[name=teacher_checkbox]").click( function () {
            var el = $("#create_teacher_button_company_admin");
            if(this.checked){
                el.html("");
                el.html('<button id="create_teacher_company_admin" type="button" class="btn btn-success btn-block"><b>Создать преподавателя?</b></button>').
                click( function () {
                    var create_teacher_company_admin = $("#add_teacher_company_admin");
                    create_teacher_company_admin.slideToggle();
                })
            }
            else{
                el.html("");
                var create_teacher_company_admin = $("#add_teacher_company_admin");
                create_teacher_company_admin.slideUp();
            }
        });

        $('select[name=teacher_selector]').on('change', function () {
            var el = $("#create_teacher_button_company_admin");
            el.html("");
            el.html('<button id="add_selected_teachers_to_course" type="button" class="btn btn-success btn-block"><b>Добавить преподавателя</b></button>').
            unbind().
            on('click', function () {
                var teacher_id = $('select[name=teacher_selector] option:selected').data('id');
                var name = $('select[name=teacher_selector] option:selected').val();
                $("#all_teachers").append('<div id="'+teacher_id+'" style="display: inline-block"><a id="teacher_from_list" data-id = '+teacher_id+'> '+name+' </a><button id="but_'+teacher_id+'" type="button" onclick="delete_a(this.id)">удалить</button>, </div>');

            })


        });


        function delete_a(id) {
            var str = id.slice(4, id.length);
            $("#"+str).remove();
        }

        $(document).ready(function () {

        });
    </script>
@endsection