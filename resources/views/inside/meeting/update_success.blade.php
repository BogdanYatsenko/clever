@extends('inside.index')

@section('title', 'Обновление мероприятия')

@section('modal')
@endsection


@section('content')


    <div class="container">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 user-name centered">
                        Мероприятие успешно обновлено.
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30 centered">
                    <button id="to_editor" class="btn goto_edit_course" action="/id{{Auth::id()}}" type="button" class="btn"><b>Перейти в личный кабинет</b></button>
                </div>
            </div>
        </div>
    </div>

@endsection







