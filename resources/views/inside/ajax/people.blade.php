
@foreach ($users as $user)
    <div class="profile col-lg-3 col-sm-6 text-center">
        <a href="/id{{ $user->id }}">
            @if (!is_null($user->photo_id))
                @foreach($photos as $photo)@if($photo->id == $user->photo_id)
                <img class="user-avatar" src="{{Storage::url('avatars/')}}{{ $user->id }}.{{$photo->type}}">
                @endif @endforeach
            @else
                <img class="img-circle img-responsive img-center" src="/img/no_avatar.png">
            @endif
        </a>
        <h5 class="h5">
            {{ $user->first }} {{ $user->second }}
            {{-- <small>Job Title</small> --}}
        </h5>
    </div>
@endforeach
