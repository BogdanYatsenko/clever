@extends('inside.index')

@section('title', 'Регистрация на портале')

@section('modal')
@endsection

@section('content')
    <div class="container">
        <div class="row margin-row">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3" id="enter_form">
                <div class="form-title col-md-12 col-sm-12 col-xs-12">
                    <h4>Регистрация на сайте</h4>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 form-content">
                    <div style="margin-top: 30px;">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                            {{ csrf_field() }}

                            <div class="form-group help-block">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                        Вы успешно зарегистрированны. Инстукция по активации выслана на {{$email}}
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection