@extends('inside.index')

@section('title', 'Вход на портал')

@section('modal')
@endsection


@section('content')
<div class="container">
    <div class="row margin-row">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3" id="enter_form">
            <div class="form-title col-md-12 col-sm-12 col-xs-12">
                <h4>Авторизоваться на сайте</h4>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-content">
                <div class="add-margin-30">
                    <form class="form-horizontal" id ="login-form" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="login" type="email" placeholder="E-mail" class="form-control" name="login" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                            <div class="col-md-12">
                                <input id="password" type="password" placeholder="Пароль"  class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group help-block">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input id="checkbox1" type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Запомнить данные
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 ">
                                <button type="submit" class="btn btn-login">
                                    Войти
                                </button>
                            </div>
                        </div>

                        <div class="form-group help-block">
                            <div class="row">
                                <div class="col-md-4 col-sm-5 col-xs-5 col-lg-4">
                                    Нет аккаунта?
                                </div>
                                <div class="col-md-8 col-sm-5 col-xs-5 col-lg-8">
                                    <a class="register" href="{{ url('/register') }}"><!-- {{ url('/register') }} -->
                                        Зарегистрироваться
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group help-block">
                            <div class="row">
                                <div class="col-md-4 col-sm-5 col-xs-5 col-lg-4">
                                    Забыли пароль?
                                </div>
                                <div class="col-md-8 col-sm-7 col-xs-5 col-lg-8">
                                    <a class="reset-pass" href="{{ url('/password/reset') }}">
                                        Восстановить доступ
                                    </a>
                                </div>

                                {{--<?php--}}

                                {{--$link = "https://accounts.google.com/o/oauth2/auth?redirect_uri=http://localhost/PleaseWait&response_type=code&client_id=1047157901598-kcgutceam03fg41v8qu6nk7k2ndit7o7.apps.googleusercontent.com&scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"--}}

                                {{--?>--}}
                                {{--<div class="col-md-8 col-sm-7 col-xs-5 col-lg-8">--}}
                                    {{--<a href="{{$link}}">--}}
                                        {{--Вход через goolge--}}
                                    {{--</a>--}}
                                {{--</div>--}}


                            </div>
                        </div>
                        <a href="/redirect/google">Google</a>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
