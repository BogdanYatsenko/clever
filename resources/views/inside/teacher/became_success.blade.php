@extends('inside.index')

@section('title', 'Стать преподавателем')

@section('content')
    <div class="container">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 user-name centered">
                        Заявка подана на рассмотрение.
                        <br/>
                        Следите за изменением статуса заявки в личном кабинете
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30 centered">
                    <button id="to_editor" class="btn goto_edit_course" action="/id{{$id}}" type="button" class="btn"><b>Вернуться в кабинет</b></button>
                </div>
            </div>
        </div>
    </div>
@endsection