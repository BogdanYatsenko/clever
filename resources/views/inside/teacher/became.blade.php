@extends('inside.index')

@section('title', 'Стать преподавателем')

@section('content')

    <div class="container">
        <div class="row margin-row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12 user-name">
                    Хотите стать преподавателем?
                </div>
            </div>
        </div>
        <form id ="create-teacher" role="form" method="POST" action="/teacher/became">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="form-group">
                        Ваше имя:
                        <input id="name_first" class="form-control" placeholder="{{$user->first}}" type="text" disabled>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="form-group">
                        Ваша фамилия:
                        <input id="name_second" class="form-control" placeholder="{{$user->second}}" type="text" disabled>
                    </div>
                </div>
                @if(isset($user->third))
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="form-group">
                            Ваше отчество:
                            <input id="name_third" class="form-control" placeholder="{{$user->third}}" type="text" disabled>
                        </div>
                    </div>
                @endif

            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        {{--Вставить сюда CKEditor?--}}
                        Ваши заслуги (в разработке):
                        <textarea type="text" rows="3" class="form-control" id="" placeholder="Наличие наград и достижений" value="" disabled></textarea>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        Ваш опыт:
                        <textarea name="exp_desc" type="text" rows="3" class="form-control" id="" placeholder="Стаж и места работы" value=""></textarea>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        Организация:
                        <input name="organization" class="form-control" placeholder="Организация, которую Вы представляете" type="text">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        Видео презентация:
                        <input name="video_link" class="form-control" placeholder="Ссылка на видео" type="text">
                    </div>
                </div>
            </div>
            <div class="row" id="buttons">
                <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">
                    <button id="cancel_course" action="/id{{Auth::id()}}" type="button" class="btn-default btn btn-left" ><b>Отмена</b></button>
                    <button id="create_teacher" type="submit" class="btn btn-success btn-right"><b>Стать преподавателем</b></button>
                </div>
            </div>
        </form>

    </div>

@endsection