<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">


  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

      <title>{{ $title }} {{ config('app.name', 'Control.info') }}</title>

      <!-- Bootstrap Core CSS -->
      <!-- <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}"> -->
      <link rel="stylesheet" href="{{ asset('css/app.css') }}">
      <!-- Custom styles for this template -->
      <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="{{ asset('css/home/index.css') }}">
      <link rel="stylesheet" href="{{ asset('css/home/carousel.css') }}">
</head>

<body id="page-top">
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only"></span> Меню <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll logo" href="#page-top"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">Поиск</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/catalog">Курсы</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#feedback">Отзывы</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#partner">Партнёры</a>
                    </li>
                     <li role="separator" class="divider"></li>
                    <li>
                    <a class="page-scroll" href="/login">
                        <i class="fa fa-sign-in" aria-hidden="true"></i> Войти
                    </a>
                </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container header-background">
            <div class="intro-text">
                <div class="intro-heading">Первая образовательная биржа</div>
                <div class="intro-lead-in">Твой навигатор в мире дополнительного образования</div>
                <div class="intro-lead-items">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                Католог всех продуктов и услуг дополнительного образования
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                Дистанционные и очные занятия, курсы, мастер-классы, семинары
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-md-11">
                                Находи, смотри отзывы, получай рукомендации наших экспертов, выбирай и становись лучше
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-md-11">
                                Получай свежую информаци о знаниях, поедлись своим мнением по образовательным услугам
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/register" class="page-scroll btn btn-xl">Зарегистрироваться</a>
            </div>
        </div>
    </header>
    <!-- Graph search section -->

    <style type="text/css">
        /*graphical search*/

        .search-div{
            height: 600px;
            width: 100%;
        }


        /*end graphical search*/
    </style>

    <div class="row">
        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
            <div id="parent-search" class="search-div">
                <canvas id="viewport"></canvas>
            </div>
        {{--</div>--}}




        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
        <script src="{{ asset('js/arbor-v0.92/lib/arbor.js') }}"></script>
        <script src="{{ asset('js/arbor-v0.92/demos/halfviz/src/renderer.js') }}"></script>
        <script src="{{ asset('js/arbor-v0.92/demos/_/graphics.js') }}"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.pack.js"></script>

        <script>
            var canvas = document.getElementById("viewport");
            var parent = document.getElementById("parent-search");
            canvas.style.width = '100%';
            canvas.style.height = '600px';
            canvas.width = parent.offsetWidth;
            canvas.height = parent.offsetHeight;
            var particleSystem;

            (function ($) {
                // работает
                var Renderer = function (canvas) {
                    var canvas = $(canvas).get(0);
                    var ctx = canvas.getContext("2d");
                    // var particleSystem;
                    var that = {
                        init: function (system) {
                            particleSystem = system;
                            particleSystem.screenSize(canvas.width, canvas.height);
                            particleSystem.screenPadding(150);
                        },
                        redraw: function () {
                            ctx.fillStyle = "#f2fcfb";
                            ctx.fillRect(0, 0, canvas.width, canvas.height);
                            particleSystem.eachEdge(function (edge, pt1, pt2) {
                                ctx.strokeStyle = edge.data.linkcolor;
                                ctx.lineWidth = 2;
                                ctx.beginPath();
                                ctx.moveTo(pt1.x, pt1.y);
                                ctx.lineTo(pt2.x, pt2.y);
                                ctx.stroke();
                            });

                            particleSystem.eachNode(function (node, pt) {
                                var gfx = arbor.Graphics(canvas);
                                var w = 100;
                                var text = node.data.name.split(' ');

                                gfx.oval(pt.x - w / 2, pt.y - w / 2, w, w - 5, {
                                    fill: node.data.nodecolor,
                                    alpha: node.data.alpha
                                });

                                if (text.length > 1) {
                                    for (var i = 0; i < text.length; i = i + 1) {
                                        //alert(text[i])
                                        gfx.text(text[i], pt.x, pt.y + (i * 14) - 5, {
                                            color: "black",
                                            align: "center",
                                            font: "Arial",
                                            size: 12
                                        })
                                    }
                                }
                                else {
                                    gfx.text(node.data.name, pt.x, pt.y + 7, {
                                        color: "black",
                                        align: "center",
                                        font: "Arial",
                                        size: 12
                                    })
                                }
                            });
                        }
                    };
                    return that;
                };
                // работает

                $(document).ready(function () {
                    window.addEventListener("resize", resize);

                    function resize(event) {
                        //alert(event);
                        canvas.width = parent.offsetWidth;
                        canvas.height = parent.offsetHeight;
                        particleSystem.screenSize(canvas.width, canvas.height);
                        //alert(parent.offsetWidth+' ' +parent.offsetHeight);
                    }


                    var sys = arbor.ParticleSystem(100,0, 0.75);
                    //sys.parameters({gravity:true});
                    sys.renderer = Renderer("#viewport");
                    sys.addNode('Node central', {name: "{{$center[0]->category_name}}", nodecolor: "#02c6ba", id: "{{$center[0]->id}}", id_parent: "{{$center[0]->id_parent}}", link:"/search/se_{{$center[0]->id}}/page_1"});
                    @foreach($child as $petal)
                        @if($petal->category_name == "")
                            sys.addNode('Node central', {name: "Ничего не найдено("});
                            @break
                        @else
                            sys.addNode('Node_{{$loop->index}}', {name: "{{$petal->category_name}}", link:'/search/se_{{$petal->id}}/page_1', id: "{{$petal->id}}", id_parent: "{{$petal->id_parent}}", nodecolor:'#02c6ba'});
                            sys.addEdge('Node central', 'Node_{{$loop->index}}', {linkcolor: "#888888"});
                        @endif
                    @endforeach
                });
            })(this.jQuery);



        </script>


    </div>


    <!-- Search section -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                        <form role="form" id="form-buscar">
                            <div class="form-group">
                                Ключевые слова или название курса:
                                <input id = "searchword" class="form-control" type="text" placeholder="Поиск среди курсов">
                            </div>
                        </form>
                </div>


            </div>
            <div class="row">
            <div class="col-md-2 col-xs-12">
                <div class="form-group">
                    Цена от:
                    <input
                            @php
                                if ($price_from != 0)
                                echo 'value = "'.$price_from.'"';
                            @endphp

                            id = "price_from" class="form-control" placeholder="{{$min_price}}">

                    </select> </div> </div>

                <div class="col-md-2 col-xs-12">
                    <div class="form-group">
                        Цена до:
                        <input
                                @php
                                    if ($price_to != 'max')
                                        if ($price_to != 0)
                                        echo 'value = "'.$price_to.'"';
                                @endphp

                                id = "price_to" class="form-control"  data-live-search="true"  placeholder="{{$max_price}}">
                        </input>
                    </div>
                </div>

                <div class="col-md-2 col-xs-12">
                    <div class="form-group">
                        Сортировать:
                        <select id = "order-select" class="selectpicker form-control">
                            <option data-id = "0" selected> По цене ( по возрастанию ) </option>
                            <option data-id = "1" > По цене ( по убыванию ) </option>
                            <option data-id = "2" > Сначала новые </option>
                            <option data-id = "3" > Сначала старые </option>
                        </select>
                    </div>
                </div>

                <div class="col-md-2 col-xs-12">
                    <div class="form-group">
                        Источник:
                        <select id = "source-select" class="selectpicker form-control">
                            <option data-id = "0"  selected> Все </option>
                            <option data-id = "1" > Биржа Clever-e </option>
                            {{ $i = 2, $selected = $selected_source}}
                            @foreach($sources as $source)
                                <option data-id = "{{$i}}" @if ($selected == $i) selected @endif> {{ $source->source }} </option>
                                {{$i = $i +1}}
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12">
                    Поиск: <br>
                    <div class="input-group">

                        <input id = "search_button" type="button" class="form-control btn btn-success" value="Подобрать курс">

                        <span class="input-group-addon">

                            <input class = "only_free" type="checkbox" aria-label="...">
                                Бесплатные
                            </span>
                    </div><!-- /input-group -->

                </div>
            </div>
            <br/>

            <div class="row text-center">
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <img src="/img/home/student.png" alt="" style="width:128px; height:128px; object-fit:cover;">
                    </span>
                    <h4 class="service-heading">Учиться?</h4>
                    <p class="text-muted">
                        Выбирай курсы, образовательные мероприятия и услуги,
                        учись, оставляй отзывы!
                    </p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <img src="/img/home/hr.png" alt="" style="width:128px; height:128px; object-fit:cover;">
                    </span>
                    <h4 class="service-heading">Продвигать свои образовательные услуги?</h4>
                    <p class="text-muted">
                        Разместите описание Ваших курсов, образовательных продуктов,
                        услуг и мероприятий, расширяй аудиторию.
                    </p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <img src="/img/home/teacher.png" alt="" style="width:128px; height:128px; object-fit:cover;">
                    </span>
                    <h4 class="service-heading">Преподавать свой он-лайн курс?</h4>
                    <p class="text-muted">
                        Размести свои учебные материалы в Клевере. Используй готовый инструментарий
                        для обучения слушателей.
                    </p>
                </div>
            </div>
        </div>
    </section>


    <!-- Contact Feedbacks -->
    <section id="feedback" class="bg-light-green">
        <div class="container">
            <div class="row">
                <div class='col-md-offset-2 col-md-8 text-center'>
                    <h2>О нас говорят</h2>
                    <div class="col-lg-2 col-lg-offset-5 col-md-2 col-md-offset-5 text-split"></div>
                </div>
            </div>
            <div class='row'>
                <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                    <!-- Bottom Carousel Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#quote-carousel" data-slide-to="1"></li>
                        <li data-target="#quote-carousel" data-slide-to="2"></li>
                    </ol>

                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner">
                        <!-- Quote 1 -->
                        <div class="item active">
                            <blockquote>
                                <div class="row">
                                    <div class="col-md-offset-2 col-lg-offset-2 col-md-2 col-lg-2 col-sm-3 text-center">
                                        <img class="img-circle" src="/img/home/feedback_2.jpg" style="object-fit:cover; width: 150px; height:150px;">
                                    </div>
                                    <div class="fb col-sm-9 col-md-7 col-lg-7">
                                        <p>Пользовался платформой Клевер, как преподаватель корпоративного курса.
                                            Образовательный процесс был построен в соответствии с моими требованиями.
                                            Очень понравились гибкие настройки для тестов, что позволило построить обучение
                                            сотрудников более эффективно. Планирую провести следующее обучение, пользуясь сайтом
                                            Клевер.
                                        </p>
                                        <small>Алексей Смахтин</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 2 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-md-offset-2 col-lg-offset-2 col-md-2 col-lg-2 col-sm-3 text-center">
                                        <img class="img-circle" src="/id1000070" style="object-fit:cover; width: 150px; height:150px;">
                                    </div>
                                    <div class="fb col-sm-9 col-md-7 col-lg-7">
                                        <p>
                                            Клевер-один из самых удобных образовательных ресурсов!
                                            Понятный интерфейс упрощает работу как преподавателю, так и обучающимся.
                                            Очень удобно просматривать результаты обучающихся, а форма связи позволяет
                                            вести живое общение, как при личном общении.
                                        </p>
                                        <small>Екатерина Вавилова</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 3 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-md-offset-2 col-lg-offset-2 col-md-2 col-lg-2 col-sm-3 text-center">
                                        <img class="img-circle" src="/img/home/feedback_1.jpg" style="object-fit:cover; width: 150px; height:150px;">
                                    </div>
                                    <div class="fb col-sm-9 col-md-7 col-lg-7">
                                        <p>
                                            Отличный ресурс. Все понятно и просто. Адекватные преподаватели! это радует.
                                            Есть над чем поработать в плане юзабилити и мелких косяков, но это всегда так на новых платформах.
                                            Должны отшлифовать со временем все. Рекомендую.
                                        </p>
                                        <small>Василий Бондарь</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>

                        <!-- Carousel Buttons Next/Prev -->
                        <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                        <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="partner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Наши партнёры</h2>
                    <div class="col-lg-2 col-lg-offset-5 col-md-2 col-md-offset-5 text-split"></div>
                </div>
            </div>
            <div class="row partners">
                <div class="col-lg-4 col-lg-offset-2">
                    <div class="col-lg-3">
                        <img src="/img/home/rpharm.png" alt="">
                    </div>
                    <div class="col-lg-9">
                        <h4>Р-ФАРМ</h4>
                        <p>Российская высокотехнологичная компания.</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="col-md-3">
                        <img src="/img/home/uniyar.png" alt="">
                    </div>
                    <div class="col-md-9">
                        <h4>ЯрГУ им. П.Г. Демидова</h4>
                        <p>Высшие учебное заведение, опорный ВУЗ в Ярославском районе.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Clients Pay -->
    <aside id="pay">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-2 col-sm-6">
                    <a href="#">
                        <img src="img/home/tinkoff.png" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-sm-6">
                    <a href="#">
                        <img src="img/home/visa.png" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-sm-6">
                    <a href="#">
                        <img src="img/home/mastercard.png" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-sm-6">
                    <a href="#">
                        <img src="img/home/maestro.png" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-sm-6">
                    <a href="#">
                        <img src="img/home/mir.png" class="img-responsive img-centered" alt="">
                    </a>
                </div>
            </div>
        </div>
    </aside>
    <footer class='bg-my-gray'>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12 copyright">
                    <div class="footer-logo col-md-12 col-sm-6 col-xs-6 col-sm-offset-3 col-xs-offset-3"></div>
                    <div class="copy col-md-12 col-sm-12 col-xs-12">
                        &copy; 2017 Клевер. Все права защищены.
                    </div>
                </div>
                <div class="col-md-4">
                    <ul  class="list-inline social-btn">
                        <li><a href="https://vk.com/clevere" target="_blank"><i class="fa fa-vk"></i></a></li>
                        <li><a href="https://www.facebook.com/cleverecom" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                    <div class="col-md-12 col-sm-12 col-xs-12" align="center">
                        support@clever-e.com
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="col-md-5 col-md-offse-1 col-xs-6">
                        <ul class="list-unstyled">
                            <li class="header-ul">О Проекте</li>
                            <li><a href="/files/home/about.docx">О компании</a></li>
                            <li><a href="/home/contacts/">Контакты</a></li>
                            <li><a href="/home/offer">Документы</a></li>
                        </ul>
                    </div>
                    <div class="col-md-5 col-xs-6">
                        <ul class="list-unstyled">
                            <li class="header-ul">Сервисы</li>
                            <li><a href="#">Каталог курсов</a></li>
                            <li><a href="#">Преподаватели</a></li>
                        </ul>
                    </div>
                </div>
                <div class="offer col-md-12 col-sm-12 col-xs-12" align="center">
                    Конечным получателем средств является <a href="home/contacts/"> ООО "А-Реал Консалтинг"</a>,
                    оплата на сайте принимается за пользование услугами сайта
                    clever-e.ru в соответствии с договором - <a href="/files/home/ofert.pdf">публичной офертой</a>.
                    К нашей клиентской аудитории относятся физические лица дееспособного
                    возраста. Вывод средств/пополнение из личного кабинета не
                    предусмотрено.
                </div>

            </div>
        </div>
    </footer>

    @include('home.layers.footer')
    <!-- Plugin JavaScript -->
    <script src="{{ asset('js/home/index.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            var order_id = 0, source_id = 0,  searchword = 0, price_from = 0, price_to = 'max', only_free = 0;

            $('#searchword').on('keyup', function (e) {
                searchword = $(this).val();
                if (searchword == '') searchword = 0;
            });

            $('#order-select').on('change', function (e) {
                order_id = $(this).find("option:selected").data('id');

            });

            $('#source-select').on('change', function (e) {
                source_id = $(this).find("option:selected").data('id');

            });

            $('#price_from').on('keyup', function (e) {
                price_from = $(this).val();
            });

            $('#price_from').on('keypress', function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });

            $('#price_to').on('keyup', function (e) {
                price_to = $(this).val();
            });

            $('#price_to').on('keypress', function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });

            $(".only_free").change(function() {

                if(this.checked) {
                    only_free = 1;
                    $("#price_to").prop('disabled', true);
                    $("#price_from").prop('disabled', true);
                } else {
                    only_free = 0;
                    $("#price_to").prop('disabled', false);
                    $("#price_from").prop('disabled', false);
                }

            });

            $('#search_button').on('click', function (e) {


                order_id = $("#order-select").find("option:selected").data('id');
                source_id = $("#source-select").find("option:selected").data('id');


                if ($('#searchword').val() != '') searchword = $('#searchword').val();

                if (!$('#price_to').val()) price_to = 'max'; else price_to = $('#price_to').val();
                if (!$('#price_from').val()) price_from = '0'; else price_from = $('#price_from').val();

                var way;

                if (only_free == 0)
                    way = '/catalog/source_'+source_id+'/'+searchword+'/from_'+price_from+'/to_'+price_to+'/order_'+order_id+'/page_1';
                else
                    way = '/catalog/source_'+source_id+'/'+searchword+'/from_'+0+'/to_0'+'/order_'+order_id+'/page_1';

                history.pushState(null, null, way);

                $.get( way, function(  ) {

                }).done(function(data){
                    console.log(data)
                    if ($("#search_result").html() != data)
                        $("#search_result").html(data.content_courses);
                    if($("#searchword").length ==0)
                        $('#panel').html(data.content)
                });

                window.location.href = way;

            });
        });
    </script>
</body>

</html>
